﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using Microsoft.Practices.Prism.Modularity;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.Regions;

namespace Fabryka.ModuleVisits
{
    [ModuleExport(typeof(ModuleVisits), InitializationMode = InitializationMode.WhenAvailable)]
    public class ModuleVisits : IModule
    {
        private readonly IRegionViewRegistry regionViewRegistry;

        [ImportingConstructor]
        public ModuleVisits(IRegionViewRegistry registry)
        {
            regionViewRegistry = registry;
        }

        public void Initialize()
        {
           regionViewRegistry.RegisterViewWithRegion("SettingsRegion", typeof(Settings.VisitSettingsView));
           regionViewRegistry.RegisterViewWithRegion("SettingsRegion", typeof(Settings.VisitRTFTemplatesView));
           regionViewRegistry.RegisterViewWithRegion("MainMenuButtonsRegion", typeof(Menu.VisitsButtonView));
        }
    }
}
