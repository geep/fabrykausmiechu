﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Fabryka.Common.Services;
using Microsoft.Practices.Prism.ViewModel;
using Fabryka.Business;
using DevExpress.Xpo;
using System.Collections;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using DevExpress.Data.Filtering;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Microsoft.Practices.ServiceLocation;

namespace Fabryka.ModuleVisits.Visits
{
    [Export]
    public class VisitsViewModel : NotificationObject
    {
        private System.Windows.Visibility _ListVisibility = System.Windows.Visibility.Visible;
        private bool _IsNodePopupOpen;
        private RTFTemplateNode _SelectedNode;
        private bool _VisitTextModified;
        private IDataService dataService;
        private bool viewChosen;
        private readonly ISelectedPatientService selectedService;
        private readonly IRegionManager regionManager;
        public InteractionRequest<Notification> TeethPickerRequest { get; private set; }

        private List<string> _ArcList;
        public List<string> ArcList
        {
            get
            {
                return _ArcList;
            }
        }

        private Patient _SelectedPatient;

        public void ChooseProperView()
        {
            if (_SelectedPatient != null && _SelectedPatient.FabrykaData.IsNFZPatient)
            {
                regionManager.RequestNavigate("VisitRegion", "NFZVisitView");
                regionManager.RequestNavigate("VisitTotalRegion", "NFZVisitTotalView");
            }
            else
            {
                regionManager.RequestNavigate("VisitRegion", "NormalVisitView");
                regionManager.RequestNavigate("VisitTotalRegion", "NormalVisitTotalView");
            }
        }

        public Action<string> InsertText { get; set; }

        public XPCollection<RTFTemplateNode> TemplateRoots { get; private set; }
        public RTFTemplateNode SelectedNode
        {
            get
            {
                return _SelectedNode;
            }

            private set
            {
                _SelectedNode = value;
                RaisePropertyChanged(() => SelectedNode);
            }
        }


        public bool IsNodePopupOpen
        {
            get { return _IsNodePopupOpen; }
            set
            {
                _IsNodePopupOpen = value;
                RaisePropertyChanged(() => IsNodePopupOpen);
            }
        }
        


        public Patient SelectedPatient
        {
            get
            {
                return _SelectedPatient;
            }
            set
            {
                _SelectedPatient = value;
                dataService.UpdateVisits();
                RaisePropertyChanged(() => SelectedPatient);
                RaisePropertyChanged(() => Visits);
                ChooseProperView();
                SelectedVisit = null;
            }
        }

        public Visit AnyVisit
        {
            get
            {
                return SelectedVisit ?? (Visits == null ? null : Visits.FirstOrDefault());
            }
        }

        private Visit _SelectedVisit;
        public Visit SelectedVisit {
            get
            {
                return _SelectedVisit;
            }

            set
            {
                if (_SelectedVisit == value)
                    return;

                if (_SelectedVisit != null)
                {
                    _SelectedVisit.Changed -= _SelectedVisit_Changed;
                }

                _SelectedVisit = value;

                

                if (_SelectedVisit != null)
                {
                    _VisitText = _SelectedVisit.Description;
                    _SelectedVisit.Changed += _SelectedVisit_Changed;

                    if (!viewChosen)
                    {
                        viewChosen = true;
                        ChooseProperView();
                    }
                }

                
                RaisePropertyChanged(() => SelectedVisit);
                RaisePropertyChanged(() => AnyVisit);
                RaisePropertyChanged(() => VisitText);
            }
        }

        public bool VisitTextModified
        {
            get
            {
                return _VisitTextModified;
            }
            set
            {
                _VisitTextModified = value;
                RaisePropertyChanged(() => VisitTextModified);
            }
        }

        void _SelectedVisit_Changed(object sender, ObjectChangeEventArgs e)
        {
            if ((e.Reason == ObjectChangeReason.Reset || e.PropertyName == "Description")  && !VisitTextModified)
            {
                _VisitText = _SelectedVisit.Description;
                RaisePropertyChanged(() => VisitText);
                VisitTextModified = false;
            }
        }

        public XPCollection<Visit> Visits
        {
            get
            {
                return SelectedPatient == null ? null : SelectedPatient.Visits;
            }
        }

        public XPCollection<Doctor> Doctors
        {
            get
            {
                return dataService.GetDoctors();
            }
        }

        private string _VisitText;

        public string VisitText
        {
            get
            {
                return _VisitText;
            }
            set
            {
                _VisitText = value;

                if (SelectedVisit != null)
                {
                    SelectedVisit.Description = _VisitText;
                    VisitTextModified = false;
                }

                RaisePropertyChanged(() => VisitText);
            }
        }

        public DelegateCommand PayAll { get; private set; }
        public DelegateCommand<RTFTemplateNode> OpenNode { get; private set; }
        public DelegateCommand ClosePopup { get; private set; }

        public System.Windows.Visibility ListVisibility
        {
            get
            {
                return _ListVisibility;
            }
            private set
            {
                _ListVisibility = value;
                RaisePropertyChanged(() => ListVisibility);
            }
        }

        [ImportingConstructor]
        public VisitsViewModel(ISelectedPatientService _selectedService, IDataService _dataService, IRegionManager _regionManager)
        {
            dataService = _dataService;
            regionManager = _regionManager;
            selectedService = _selectedService;
            selectedService.SelectionChanged += selectedService_SelectionChanged;
            SelectedPatient = selectedService.SelectedPatient;

            _ArcList = Properties.Settings.Default.ArcList.ConvertAll((input) => input.ArcName);

            TemplateRoots = dataService.GetTypedTemplateRoots(RTFTemplateType.Visits);

            TeethPickerRequest = new InteractionRequest<Notification>();

            Properties.Settings.Default.SettingChanging += Default_SettingChanging;
            PayAll = new DelegateCommand(() => { if (SelectedVisit != null) SelectedVisit.Payed = SelectedVisit.Receivable; },
                () => SelectedVisit != null && SelectedVisit.Receivable != 0);
            OpenNode = new DelegateCommand<RTFTemplateNode>(
                (node) =>
                {
                    if (node == null)
                        return;

                    if (node.IsLeaf)
                    {
                        IsNodePopupOpen = false;
                        ListVisibility = System.Windows.Visibility.Visible;

                        string teeth = String.Empty;
                        var text = node.Template;
                        if (node.ShowTeethPicker)
                        {
                            TeethPickerRequest.Raise(new Notification() { Content = this, Title = "Wybór zębów" });
                            teeth = ServiceLocator.Current.GetInstance<ITeethPickerService>().GetResult();
                            text = text.Replace("&", teeth);
                        }
                        InsertText(text);

                        return;
                    }

                    SelectedNode = node;
                    IsNodePopupOpen = true;
                    ListVisibility = System.Windows.Visibility.Hidden;
                }
            );

            ClosePopup = new DelegateCommand(() => { IsNodePopupOpen = false; ListVisibility = System.Windows.Visibility.Visible; });
        }

        void Default_SettingChanging(object sender, System.Configuration.SettingChangingEventArgs e)
        {
            if (e.SettingName == "ArcList")
            {
                _ArcList = ((Fabryka.ModuleVisits.Settings.ArcList)e.NewValue).ConvertAll((input) => input.ArcName);
                RaisePropertyChanged(() => ArcList);
            }
        }

        void selectedService_SelectionChanged(object sender, EventArgs e)
        {
            SelectedPatient = selectedService.SelectedPatient;
        }
    }
}
