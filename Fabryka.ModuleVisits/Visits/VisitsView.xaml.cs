﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.Regions;
using DevExpress.Xpf.RichEdit;

namespace Fabryka.ModuleVisits.Visits
{
    /// <summary>
    /// Interaction logic for VisitsView.xaml
    /// </summary>
   [Export("VisitsView", typeof(VisitsView))]
    public partial class VisitsView : UserControl, INavigationAware
    {
        public VisitsView()
        {
#pragma warning disable 0618
            DevExpress.Xpf.Core.DXGridDataController.DisableThreadingProblemsDetection = true;
#pragma warning restore 0618
            InitializeComponent();
            //visitEdit.Views.SimpleView.Padding = new System.Windows.Forms.Padding(0);
        }

        [Import]
        public VisitsViewModel ViewModel
        {
            get
            {
                return DataContext as VisitsViewModel;
            }
            set
            {
                DataContext = value;
                value.InsertText = (text) =>
                    {
                        this.visitEdit.BeginUpdate();
                        this.visitEdit.Document.InsertRtfText(visitEdit.Document.CaretPosition, text);
                        this.visitEdit.EndUpdate();
                        this.visitEdit.GetBindingExpression(RichEditControl.ContentProperty).UpdateSource();
                        this.visitEdit.Focus();
                        
                    };

                value.PropertyChanged += value_PropertyChanged;
            }
        }

        void value_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "VisitTextModified")
            {
                var res = ((VisitsViewModel)DataContext).VisitTextModified;
                if (visitEdit.Modified != res)
                    visitEdit.Modified = res;
            }
        }

        private void richEditControl1_PreviewMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
        	//e.Handled = true;
			//scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - e.Delta); 
        }

        private void visitEdit_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            if (visitEdit.IsKeyboardFocused)
                e.Handled = true;
        }

        private void visitEdit_ModifiedChanged(object sender, EventArgs e)
        {
            ((VisitsViewModel)DataContext).VisitTextModified = visitEdit.Modified;
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            ViewModel.ChooseProperView();
            ViewModel.ClosePopup.Execute();
        }
    }
}
