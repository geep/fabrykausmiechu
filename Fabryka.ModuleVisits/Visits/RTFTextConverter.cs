﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Documents;
using System.IO;

namespace Fabryka.ModuleVisits.Visits
{
    [ValueConversion(typeof(string), typeof(string))]
    class RTFTextConverter : IValueConverter
    {
        private static string CleanUp(string value)
        {
            value = value.Replace(Environment.NewLine, " ");
            value = value.Replace('\n', ' ');
            value = value.Replace("  ", " ");
            value = value.Trim();
            return value;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return String.Empty;
            MemoryStream ms = new MemoryStream(UTF8Encoding.Default.GetBytes((string) value));
            FlowDocument flow = new FlowDocument();
            TextRange range = new TextRange(flow.ContentStart, flow.ContentEnd);
            range.Load(ms, System.Windows.Forms.DataFormats.Rtf);
            ms.Close();
            return CleanUp(range.Text);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
