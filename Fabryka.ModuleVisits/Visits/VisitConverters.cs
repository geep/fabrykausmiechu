﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;

namespace Fabryka.ModuleVisits.Visits
{
    internal class VisitTotalConverter : MarkupExtension, IMultiValueConverter
    {
        private static VisitTotalConverter _converter;

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return decimal.Parse(values[0].ToString()) - decimal.Parse(values[1].ToString());
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }


        public override object ProvideValue(IServiceProvider serviceProvider)

        {
            return _converter ?? (_converter = new VisitTotalConverter());
        }

    }

    internal class VisitColorConverter : MarkupExtension, IValueConverter
    {
        private static VisitColorConverter _converter;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (decimal.Parse((value ?? 0).ToString()) > 0) ? Brushes.IndianRed : Brushes.LimeGreen;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return _converter ?? (_converter = new VisitColorConverter());
        }
    }

}
