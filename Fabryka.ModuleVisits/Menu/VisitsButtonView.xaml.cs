﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.Regions;

namespace Fabryka.ModuleVisits.Menu
{
    /// <summary>
    /// Interaction logic for PhotosButton.xaml
    /// </summary>
    [Export]
    [ViewSortHint("020")]
    public partial class VisitsButtonView : UserControl
    {
        public VisitsButtonView()
        {
            InitializeComponent();
        }

        [Import]
        public VisitsButtonViewModel ViewModel { set { DataContext = value; } }

    }
}
