﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.Composition;
using Fabryka.CommonWPF.Menu;

namespace Fabryka.ModuleVisits.Menu
{
    [Export]
    public class VisitsButtonViewModel : MainMenuButtonViewModel
    {
        public VisitsButtonViewModel()
        {
            UriName = "VisitsView";
            SubMenuUriName = "VisitsSubMenuView";
        }
    }
}
