﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;

namespace Fabryka.ModuleVisits.Menu
{
    /// <summary>
    /// Interaction logic for PatientSubMenuView.xaml
    /// </summary>
    [Export("VisitsSubMenuView", typeof(VisitsSubMenuView))]
    public partial class VisitsSubMenuView : UserControl
    {
        public VisitsSubMenuView()
        {
            InitializeComponent();
        }

        [Import]
        public VisitsSubMenuViewModel ViewModel
        {
            set
            {
                DataContext = value;
            }
        }
    }
}
