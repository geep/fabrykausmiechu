﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Fabryka.Common.Services;
using Microsoft.Practices.Prism.Commands;
using Fabryka.ModuleVisits.Visits;
using Fabryka.Business;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Fabryka.ModuleVisits.Report;
using DevExpress.XtraReports.UI;

namespace Fabryka.ModuleVisits.Menu
{
    [Export]
    public class VisitsSubMenuViewModel
    {
        private readonly ISelectedPatientService selectedService;

        public DelegateCommand CreateVisit { get; private set; }
        public DelegateCommand RemoveVisit { get; private set; }
        public DelegateCommand PrintVisits { get; private set; }

        public InteractionRequest<Confirmation> ConfirmationRequest { get; private set; }

        [ImportingConstructor]
        public VisitsSubMenuViewModel(ISelectedPatientService _selectedService, IDataService _dataService, VisitsViewModel visitsViewModel)
        {
            selectedService = _selectedService;

            ConfirmationRequest = new InteractionRequest<Confirmation>();


            PrintVisits = new DelegateCommand(() => PrintVisitsReport(), () => selectedService.SelectedPatient != null);
            CreateVisit = new DelegateCommand(() =>
            {
                Visit v = _dataService.CreateVisit(selectedService.SelectedPatient);
                visitsViewModel.SelectedVisit = v;
            }, () => selectedService.SelectedPatient != null);
            RemoveVisit = new DelegateCommand(() =>
                {
                    ConfirmationRequest.Raise(new Confirmation() { Content = "Czy na pewno chcesz skasować tę wizytę?", Title = "Kasowanie wizyty" },
                        (cb) => { if (cb.Confirmed) _dataService.RemoveVisit(visitsViewModel.SelectedVisit); });
                },
                () => selectedService.SelectedPatient != null && visitsViewModel.SelectedVisit != null);

            selectedService.SelectionChanged += (s, e) =>
                {
                    CreateVisit.RaiseCanExecuteChanged();
                    RemoveVisit.RaiseCanExecuteChanged();
                    PrintVisits.RaiseCanExecuteChanged();
                };

            visitsViewModel.PropertyChanged += (s, e) =>
                {
                    if (e.PropertyName == "SelectedVisit")
                        RemoveVisit.RaiseCanExecuteChanged();
                };
        }

        private void PrintVisitsReport()
        {
            if (selectedService.SelectedPatient == null)
                return;

            VisitsReport report =
                                    new VisitsReport();
            report.PatientID.Value = selectedService.SelectedPatient.Oid;
            ReportPrintTool printTool = new ReportPrintTool(report);
            printTool.ShowPreviewDialog();
        }


    }
}
