﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.ViewModel;
using System.Collections;
using System.Windows.Documents;
using Microsoft.Practices.Prism.Commands;

namespace Fabryka.ModuleVisits.Settings
{
    [Export]
    public class VisitSettingsViewModel : NotificationObject
    {
        public string ViewName
        {
            get
            {
                return "Wizyty";
            }
        }

        // Fields...
        public IEnumerable<ArcDescription> ArcList
        {
            get
            {
                return Properties.Settings.Default.ArcList;
            }
        }

        public DelegateCommand SaveCommand { get; private set; }
        public DelegateCommand CancelCommand { get; private set; }

        public VisitSettingsViewModel()
        {
            Properties.Settings.Default.PropertyChanged += Default_PropertyChanged;
            SaveCommand = new DelegateCommand(
                () =>
                {
                    Properties.Settings.Default.NotifySettingsChanging();
                    Properties.Settings.Default.Save();
                }
            );
            CancelCommand = new DelegateCommand(
                () =>
                { 
                    Properties.Settings.Default.Reload(); 
                    RaisePropertyChanged(() => ArcList); 
                });
        }

        void Default_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ArcList")
            {
                RaisePropertyChanged(() => ArcList);
            }
        }
    }
}
