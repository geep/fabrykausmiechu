﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.ViewModel;
using Fabryka.Common.Services;
using DevExpress.Xpo;
using Fabryka.Business;
using Microsoft.Practices.Prism.Commands;

namespace Fabryka.ModuleVisits.Settings
{
    [Export]
    public class VisitRTFTemplatesViewModel : NotificationObject
    {

        public string ViewName
        {
            get
            {
                return "Wzorce - wizyty";
            }
        }

        private IDataService dataService;

        public DelegateCommand<RTFTemplateNode> AddGroup { get; set; }
        public DelegateCommand<RTFTemplateNode> AddTemplate { get; set; }
        public DelegateCommand<RTFTemplateNode> RemoveNode { get; set; }


        public XPCollection<RTFTemplateNode> TemplateNodes
        {
            get
            {
                return dataService.GetVisitTemplates();
            }
        }

        [ImportingConstructor]
        public VisitRTFTemplatesViewModel(IDataService _dataService)
        {
            dataService = _dataService;

            AddGroup = new DelegateCommand<RTFTemplateNode>(
                (node) => TemplateNodes.Add(new RTFTemplateNode(TemplateNodes.Session) { Name = "Nazwa grupy", IsLeaf = false, TemplateType = RTFTemplateType.Visits, Parent = node }),
                (node) => node == null || !node.IsLeaf
                );

            AddTemplate = new DelegateCommand<RTFTemplateNode>(
                (node) => TemplateNodes.Add(new RTFTemplateNode(TemplateNodes.Session) { Name = "Nazwa wzorca", IsLeaf = true, TemplateType = RTFTemplateType.Visits, Parent = node}),
                (node) => node == null || !node.IsLeaf
                );

            RemoveNode = new DelegateCommand<RTFTemplateNode>(
                (node) => { if (node != null) node.Delete(); }, (node) => node != null);
            

        }
    }
}
