﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Fabryka.ModuleVisits.Settings
{

    public class ArcDescription
    {
        [UserScopedSetting()]
        [SettingsSerializeAs(System.Configuration.SettingsSerializeAs.Xml)]
        public string ArcName { get; set; }

        public override string ToString()
        {
            return ArcName;
        }
    }

    public class ArcList : List<ArcDescription>
    {
    }
}
