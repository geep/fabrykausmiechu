﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;

namespace Fabryka.ModuleVisits.Settings
{
    /// <summary>
    /// Interaction logic for VisitSettingsView.xaml
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class VisitSettingsView : UserControl
    {
        public VisitSettingsView()
        {
            InitializeComponent();
        }

        [Import]
        public VisitSettingsViewModel ViewModel
        {
            set
            {
                DataContext = value;
            }
        }

    }
}
