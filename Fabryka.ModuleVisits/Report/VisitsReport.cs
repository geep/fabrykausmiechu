﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Fabryka.Common.Services;
using Microsoft.Practices.ServiceLocation;

namespace Fabryka.ModuleVisits.Report
{
    public partial class VisitsReport : DevExpress.XtraReports.UI.XtraReport
    {
        public VisitsReport()
        {
            InitializeComponent();
        }

        private void patientCollection_ResolveSession(object sender, DevExpress.Xpo.ResolveSessionEventArgs e)
        {
            var dataService = ServiceLocator.Current.GetInstance<IDataService>();
            e.Session = dataService.GetCurrentSession();
        }

    }
}
