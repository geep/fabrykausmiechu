﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Fabryka.ModuleVisits.Visits;
using System.ComponentModel.Composition;

namespace Fabryka.ModuleVisits.NormalVisit
{
    /// <summary>
    /// Interaction logic for NormalVisitTotal.xaml
    /// </summary>
    [Export("NormalVisitTotalView", typeof(NormalVisitTotalView))]
    public partial class NormalVisitTotalView : UserControl
    {
        public NormalVisitTotalView()
        {
            InitializeComponent();
        }

        [Import]
        public VisitsViewModel ViewModel
        {
            set
            {
                DataContext = value;
            }
        }
    }
}
