﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;

namespace Fabryka.ModuleVisits.NFZVisit
{
    /// <summary>
    /// Interaction logic for NFZVisit.xaml
    /// </summary>
    [Export("NFZVisitView", typeof(NFZVisitView))]
    public partial class NFZVisitView : UserControl
    {
        public NFZVisitView()
        {
            InitializeComponent();
        }

        [Import]
        public NFZVisitViewModel ViewModel
        {
            set
            {
            	DataContext = value;
            }
        }
    }
}
