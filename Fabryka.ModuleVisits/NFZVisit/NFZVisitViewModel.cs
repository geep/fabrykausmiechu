using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.ViewModel;
using Fabryka.ModuleVisits.Visits;
using System.ComponentModel.Composition;
using Fabryka.Business;
using DevExpress.Xpo;
using Fabryka.Common.Services;
using Microsoft.Practices.Prism.Commands;

namespace Fabryka.ModuleVisits.NFZVisit
{
    [Export]
    public class NFZVisitViewModel : NotificationObject
    {
        private Fabryka.Business.NFZVisit _AnyNFZVisit;
        private Fabryka.Business.NFZVisit _SelectedNFZVisit;
        private NFZProcedure _SelectedProcedure;
        private IDataService dataService;
        private Visit _SelectedVisit;
        private VisitsViewModel visitsVm;

        public object FocusedRow { get; set; }

        public Visit SelectedVisit
        {
            get { return _SelectedVisit; }
            set { 
                _SelectedVisit = value;
                if (_SelectedVisit != null)
                {
                    if (!_SelectedVisit.Patient.FabrykaData.IsNFZPatient)
                    {
                        _SelectedVisit = null;
                        SelectedNFZVisit = null;
                    }
                    else
                    {
                        SelectedNFZVisit = dataService.GetNFZVisit(_SelectedVisit);
                    }
                }
                RaisePropertyChanged(() => SelectedVisit); 
            }
        }


        public Fabryka.Business.NFZVisit SelectedNFZVisit
        {
            get { return _SelectedNFZVisit; }
            set 
            { 
                _SelectedNFZVisit = value;
                RaisePropertyChanged(() => SelectedNFZVisit);
                AddProcedure.RaiseCanExecuteChanged();
                RemoveProcedure.RaiseCanExecuteChanged();
            }
        }


        public Fabryka.Business.NFZVisit AnyNFZVisit
        {
            get { return _AnyNFZVisit; }
            set 
            { 
                _AnyNFZVisit = value;
                RaisePropertyChanged(() => AnyNFZVisit);
            }
        }


        public NFZProcedure SelectedProcedure
        {
            get { return _SelectedProcedure; }
            set { 
                _SelectedProcedure = value;
                AddProcedure.RaiseCanExecuteChanged();
            }
        }

        public XPCollection<NFZProcedure> NFZProcedures
        {
            get
            {
                return dataService.GetNFZProcedures();
            }
        }

        public DelegateCommand AddProcedure { get; set; }
        public DelegateCommand RemoveProcedure { get; set; }

        [ImportingConstructor]
        public NFZVisitViewModel(VisitsViewModel _visitsVm, IDataService _dataService)
        {
            AddProcedure = new DelegateCommand(
                () =>
                {
                    dataService.BeginTransaction();
                    SelectedNFZVisit.AddProcedure(SelectedProcedure);
                    dataService.EndTransaction();
                },
                () => SelectedProcedure != null && SelectedNFZVisit != null
            );

            RemoveProcedure = new DelegateCommand(
                () =>
                {
                    if (FocusedRow == null)
                        return;
                    dataService.BeginTransaction();
                    SelectedNFZVisit.RemoveProcedure(FocusedRow as NFZCompletedProcedure);
                    dataService.EndTransaction();
                },
                () => SelectedNFZVisit != null
            );

            dataService = _dataService;
            visitsVm = _visitsVm;
            visitsVm.PropertyChanged += visitsVm_PropertyChanged;
            SelectedVisit = visitsVm.SelectedVisit;

            if (visitsVm.AnyVisit != null && visitsVm.AnyVisit.Patient.FabrykaData.IsNFZPatient)
                AnyNFZVisit = dataService.GetNFZVisit(visitsVm.AnyVisit);
        }

        void visitsVm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedVisit")
            {
                SelectedVisit = visitsVm.SelectedVisit;
            }
            else if (e.PropertyName == "AnyVisit")
            {
                if (visitsVm.AnyVisit != null && visitsVm.AnyVisit.Patient.FabrykaData.IsNFZPatient)
                    AnyNFZVisit = dataService.GetNFZVisit(visitsVm.AnyVisit);
            }
        }
    }
}
