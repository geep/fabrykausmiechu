﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;

namespace Fabryka.ModuleVisits.NFZVisit
{
    /// <summary>
    /// Interaction logic for NFZVisitTotalView.xaml
    /// </summary>
    [Export("NFZVisitTotalView", typeof(NFZVisitTotalView))]
    public partial class NFZVisitTotalView : UserControl
    {
        public NFZVisitTotalView()
        {
            InitializeComponent();
        }

        [Import]
        public NFZVisitViewModel ViewModel
        {
            set
            {
                DataContext = value;
            }
        }
    }
}
