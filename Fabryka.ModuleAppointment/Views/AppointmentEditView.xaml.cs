﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DevExpress.XtraScheduler;
using DevExpress.Xpf.Scheduler;
using Fabryka.ModuleAppointment.ViewModels;
using Fabryka.Common.Services;
using System.ComponentModel.Composition;
using System.Windows.Data;
using System.Windows.Threading;
using System.Windows.Media;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Core.Native;
using DevExpress.Xpf.Editors.Helpers;

namespace Fabryka.ModuleAppointment.Views
{
    /// <summary>
    /// Interaction logic for AppointmentEditView.xaml
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class AppointmentEditView : UserControl
    {
        public AppointmentEditView()
        {
            InitializeComponent(); 
        }

        private readonly IDataService dataService;
        private readonly ISelectedPatientService selectedPatientService;

        [ImportingConstructor]
        public AppointmentEditView(IDataService _dataService, ISelectedPatientService _selectedPatientService)
        {
            dataService = _dataService;
            selectedPatientService = _selectedPatientService;
            InitializeComponent();
            //TODO: Zgłoś to?
            firstName.Loaded += ((s, e) => firstName.Focus());
            
        }

        public AppointmentEditViewModel ViewModel
        {
            set
            {
                DataContext = value;
            }

            get
            {
                return (AppointmentEditViewModel) DataContext;
            }
        }

        // TODO: Go to viewModel
        private void Ok_button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // Save all changes of the editing appointment.   
            BaseEdit editor = phoneEdit;
            if (editor != null)
            {
                editor.DoValidate();
                if (BaseEditHelper.GetIsValueChanged(editor))
                {
                    BindingExpression bindingExpression = editor.GetBindingExpression(BaseEdit.EditValueProperty);
                    bindingExpression.UpdateSource();
                }
            }
            ViewModel.ApplyChanges();
            SchedulerFormBehavior.Close(this, true);
        }

        private void Cancel_button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SchedulerFormBehavior.Close(this, false);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SchedulerFormBehavior.SetTitle(this, "Edycja wizyty");
            //this.resourceEdit1.SelectedItem = this.resourceEdit1.SchedulerControl.SelectedResource;
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.DeleteAppointment();
            SchedulerFormBehavior.Close(this, true);
        }

        internal void Configure(DevExpress.Xpf.Scheduler.SchedulerControl control, Appointment apt)
        {
            if (control == null || apt == null)
                throw new ArgumentNullException("control");
            if (control == null || apt == null)
                throw new ArgumentNullException("apt");

            this.ViewModel = new AppointmentEditViewModel(selectedPatientService, control, apt, dataService);
        }

        private void ColorTextBlock(object sender)
        {
            var block = sender as TextBlock;

            if (block == null)
                return;

            block.TextEffects.Clear();

            if (lookUpEdit1.AutoSearchText == null)
                return;

            var value = lookUpEdit1.AutoSearchText;
            if (String.IsNullOrWhiteSpace(value))
                return;

            int start = block.Text.IndexOf(value, StringComparison.CurrentCultureIgnoreCase);
            if (start == -1)
                return;

            block.TextEffects.Add(new TextEffect()
            {
                Foreground = Brushes.DodgerBlue,
                PositionStart = start,
                PositionCount = value.Length,
            });
        }

        private void TextBlock_Loaded(object sender, RoutedEventArgs e)
        {
            ColorTextBlock(sender);
        }

        private void TextBlock_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            ColorTextBlock(sender);
        }

        private void lookUpEdit1_GotFocus(object sender, RoutedEventArgs e)
        {
            var ed = lookUpEdit1;
            Action act = () =>
            {
                if (ed.SelectionLength != ed.Text.Length)
                    ed.SelectAll();
            };

            Dispatcher.BeginInvoke(act, DispatcherPriority.Render, null);
        }

        private void lookUpEdit1_LostFocus(object sender, RoutedEventArgs e)
        {
            var ed = lookUpEdit1;
            Action act = () =>
            {
                ed.SelectionLength = 0;
            };

            Dispatcher.BeginInvoke(act, DispatcherPriority.Render, null);
        }
    }
}