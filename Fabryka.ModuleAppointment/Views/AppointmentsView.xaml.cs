﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.ComponentModel.Composition;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Localization;
using Fabryka.ModuleAppointment.ViewModels;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Scheduler.UI;
using System.Windows;
using Microsoft.Practices.Prism.Regions;
using Fabryka.CommonWPF.Extensions;
using Fabryka.Common.Services;
using Microsoft.Practices.ServiceLocation;

namespace Fabryka.ModuleAppointment.Views
{
    /// <summary>
    /// Interaction logic for AppointmentsView.xaml
    /// </summary>
    [Export("AppointmentsView",typeof(AppointmentsView))]
    public partial class AppointmentsView : UserControl, IRegionMemberLifetime, INavigationAware
    {
        public AppointmentsViewModel ViewModel
        {
            get
            {
                return DataContext as AppointmentsViewModel;
            }
            set
            {
                DataContext = value;
            }
        }

        private IDataService dataService;

        [ImportingConstructor]
        public AppointmentsView(AppointmentsViewModel viewModel, IDataService _dataService)
        {
            dataService = _dataService;
            DevExpress.Xpf.Editors.EditorLocalizer.Active = new ModuleEditorLocalizer();
            DevExpress.Xpf.Scheduler.SchedulerControlLocalizer.Active = new ModuleSchedulerControlLocalizer();
            SchedulerLocalizer.Active = new ModuleSchedulerLocalizer();
            InitializeComponent();
            ViewModel = viewModel;
            viewModel.Control = schedulerControl;
            SetWorkDays();
            SetResourceBars();
        }

        private void SetWorkDays()
        {
            schedulerControl.WorkDays.BeginUpdate();
            schedulerControl.WorkDays.Clear();
            //schedulerControl.WorkDays.AddHoliday( new DateTime(2012, 7, 16), "Bo tak");
            schedulerControl.WorkDays.Add(WeekDays.Monday | WeekDays.Tuesday | WeekDays.Thursday | WeekDays.Friday | WeekDays.Saturday);
            schedulerControl.WorkDays.EndUpdate();
        }

        private void SetResourceBars()
        {
            PopupMenu menu = new PopupMenu();
            foreach (Fabryka.Business.AppointmentResource resource in ViewModel.Resources)
            {
                string name = String.Format("resource_{0}", resource.Oid);
                var item = new BarButtonItem() 
                { 
                    Name = name, 
                    Content = resource.Label,
                    Command = ViewModel.PrintSelectedCommand,
                    CommandParameter = resource.Oid
                };
                barManager.Items.Add(item);

                var link = new BarButtonItemLink() { BarItemName = name };
                menu.ItemLinks.Add(link);           	
            }            

            printSelected.PopupControl = menu;

            var template = FindResource("AppointmentTypeSquare") as DataTemplate;

            foreach (Fabryka.Business.AppointmentType type in ViewModel.Types)
            {
                string name = String.Format("type_{0}", type.Oid);
                var item = new BarButtonItem()
                {
                    Name = name,
                    Content = type.Label,
                    Command = ViewModel.ChangeAppointmentTypeCommand,
                    CommandParameter = type,
                    DataContext = type,
                    GlyphTemplate = template
                };
                barManager.Items.Add(item);
            }

        }

        private void schedulerControl_EditAppointmentFormShowing(object sender, DevExpress.Xpf.Scheduler.EditAppointmentFormEventArgs e)
        {
            var editView = ServiceLocator.Current.GetInstance<AppointmentEditView>();
            editView.Configure(schedulerControl, e.Appointment);
            e.Form = editView;
        }

        void schedulerStorage_AppointmentsChanged(object sender, PersistentObjectsEventArgs e)
        {
            foreach (Appointment apt in e.Objects)
            {
                Fabryka.Business.Appointment a = schedulerControl.Storage.GetObjectRow(apt) as Fabryka.Business.Appointment;
                if (a != null)
                {
                    a.Save();
                }
            }
            ViewModel.UpdateNextPrevious();
            schedulerControl.ActiveView.LayoutChanged();
        }

        //TODO: Put in common helper
        private static string IfEmptyString(string orig, string rep)
        {
            return (String.IsNullOrWhiteSpace(orig) ? rep : orig);
        }

        private void schedulerControl_AppointmentViewInfoCustomizing(object sender, DevExpress.Xpf.Scheduler.AppointmentViewInfoCustomizingEventArgs e)
        {
            var viewInfo = e.ViewInfo;

            Fabryka.Business.Appointment a = schedulerControl.Storage.GetObjectRow(viewInfo.Appointment) as Fabryka.Business.Appointment;
            if (a != null)
            {
                viewInfo.Subject = String.Format("{1}, {0}",
                    IfEmptyString(a.PatientFirstName, "???"), IfEmptyString(a.PatientLastName, "???"));
                viewInfo.Location = String.Format("- {0}", a.Type.Label);
                var labelColor = a.Type.Color.AsMediaColor();
                viewInfo.Label = new DevExpress.Xpf.Scheduler.AppointmentLabel(labelColor, a.Type.Label);
                viewInfo.Status = new DevExpress.Xpf.Scheduler.AppointmentStatus(AppointmentStatusType.Busy, System.Windows.Media.Colors.CornflowerBlue, "Praca");
                var secondColor = labelColor;
                double opacity = 0.8;
                switch (a.State)
                {
                    case Fabryka.Business.AppointmentState.Occured:
                        secondColor = System.Windows.Media.Colors.White;
                        opacity = 0.6;
                        break;
                    case Fabryka.Business.AppointmentState.NotArrived:
                        secondColor = System.Windows.Media.Colors.IndianRed;
                        opacity = 0.7;
                        break;
                    case Fabryka.Business.AppointmentState.Unknown:
                    default:
                        break;
                }

                viewInfo.CustomViewInfo = new { Opacity = opacity, SecondColor = secondColor, NewPatient = a.IsNewPatient };
            }
        }

        private void dateNavigator_SelectedDatesChanged(object sender, EventArgs e)
        {
            ViewModel.SelectedDates = dateNavigator.SelectedDates;
        }

        private void schedulerControl_AppointmentDrop(object sender, AppointmentDragEventArgs e)
        {
            if (e.SourceAppointment.AllDay != e.EditedAppointment.AllDay)
            {
                e.Allow = false;
                e.Handled = true;
            }
        }

        private void schedulerControl_PopupMenuShowing(object sender, DevExpress.Xpf.Scheduler.SchedulerMenuEventArgs e)
        {
            if (e.Menu.Name == SchedulerMenuItemName.AppointmentMenu)
            {
                if (schedulerControl.SelectedAppointments.Count != 1)
                {
                    typeSubMenu.IsEnabled = false;
                    return;
                }

                typeSubMenu.IsEnabled = true;

                bool allDay = schedulerControl.SelectedAppointments[0].AllDay;

                typeSubMenu.ItemLinks.Clear();

                foreach (Fabryka.Business.AppointmentType type in ViewModel.Types.Where(x => x.IsAllDay == allDay))
                {
                    string name = String.Format("type_{0}", type.Oid);
                    typeSubMenu.ItemLinks.Add(new BarButtonItemLink() { BarItemName = name });
                }
                
            }
        }

        private void schedulerControl_InplaceEditorShowing(object sender, DevExpress.Xpf.Scheduler.InplaceEditorEventArgs e)
        {
            var editor = new AppointmentInPlaceEditView(schedulerControl, e.Appointment);
            e.InplaceEditor = editor;
            if (e.Bounds.Width > editor.MinWidth)
                editor.Width = e.Bounds.Width;
            e.Bounds = new Rect(e.Bounds.TopLeft, new Size(editor.Width, editor.Height));
        }


        public bool KeepAlive
        {
            get { return true; }
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            //ViewModel.StopUpdates();
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            ViewModel.StartUpdates();
        }
    }
}
