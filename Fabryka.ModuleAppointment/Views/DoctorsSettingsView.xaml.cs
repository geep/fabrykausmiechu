﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Fabryka.ModuleAppointment.ViewModels;
using System.ComponentModel.Composition;
using DevExpress.Xpf.Editors;

namespace Fabryka.ModuleAppointment.Views
{
    /// <summary>
    /// Interaction logic for DoctorsEditView.xaml
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class DoctorsSettingsView : UserControl
    {
        //[ImportingConstructor]
        public DoctorsSettingsView()
        {
            InitializeComponent();
        }

        [Import]
        public DoctorsSettingsViewModel ViewModel
        {
            get
            {
                return DataContext as DoctorsSettingsViewModel;
            }
            set
            {
                DataContext = value;
            }
        }

        private void tableView1_ShownEditor(object sender, DevExpress.Xpf.Grid.EditorEventArgs e)
        {
            //if (e.Column.Name == "buttonsColumn")
            //{
            //    (e.Editor as ButtonEdit).ShowText = false;
            //} 
        }
    }
}
