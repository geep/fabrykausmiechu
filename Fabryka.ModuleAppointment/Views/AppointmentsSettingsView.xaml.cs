﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using Fabryka.ModuleAppointment.ViewModels;

namespace Fabryka.ModuleAppointment.Views
{
    /// <summary>
    /// Interaction logic for AppointmentsSettingsView.xaml
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class AppointmentsSettingsView : UserControl
    {
        public AppointmentsSettingsView()
        {
            InitializeComponent();
        }

        [Import]
        public AppointmentsSettingsViewModel ViewModel
        {
            get
            {
                return DataContext as AppointmentsSettingsViewModel;
            }
            set
            {
                DataContext = value;
            }
        }
    }
}
