﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Scheduler.UI;
using DevExpress.XtraScheduler;
using DevExpress.Xpf.Scheduler;
using Fabryka.ModuleAppointment.ViewModels;
using Microsoft.Practices.ServiceLocation;
using Fabryka.Common.Services;

namespace Fabryka.ModuleAppointment.Views
{
    /// <summary>
    /// Interaction logic for AppointmentInPlaceEditView.xaml
    /// </summary>
    public partial class AppointmentInPlaceEditView : AppointmentInplaceEditorBase
    {
        private Fabryka.Common.Services.IDataService dataService;

        public AppointmentInPlaceEditView()
        {
            InitializeComponent();
        }
        public AppointmentInPlaceEditView(DevExpress.Xpf.Scheduler.SchedulerControl control, DevExpress.XtraScheduler.Appointment apt)
            : base(control, apt)
        {
            DataContext = new AppointmentInPlaceEditViewModel(apt, IsNewAppointment);
            InitializeComponent();
        }

        public override void Activate()
        {
            base.Activate();
            Dispatcher.BeginInvoke(new Action(() =>
            {
                patientFullName.Focus();
                if (IsNewAppointment)
                {
                    patientFullName.SelectionStart = patientFullName.Text.Length;
                    patientFullName.SelectionLength = 0;
                }
                else
                    patientFullName.SelectAll();
            }));
        }

        protected override void OnCommitChanges()
        {
            if (IsNewAppointment && String.IsNullOrWhiteSpace(patientFullName.Text))
                OnRollbackChanges();
            else
                base.OnCommitChanges();
        }

        public override void ApplyChanges()
        {
            if (dataService == null)
                dataService = ServiceLocator.Current.GetInstance<IDataService>();

            dataService.BeginTransaction();

            try
            {
                (DataContext as AppointmentInPlaceEditViewModel).ApplyChanges();
                base.ApplyChanges();
            }
            finally
            {
                dataService.EndTransaction();
            }
        }
         
    }
}
