﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using System.ComponentModel.Composition;
using DevExpress.Xpf.Grid;

namespace Fabryka.ModuleAppointment
{
    [ModuleExport(typeof(ModuleAppointment), InitializationMode = InitializationMode.WhenAvailable)]
    public class ModuleAppointment : IModule
    {
        private readonly IRegionViewRegistry regionViewRegistry;

        [ImportingConstructor]
        public ModuleAppointment(IRegionViewRegistry registry)
        {
            regionViewRegistry = registry;
        }

        public void Initialize()
        {
            regionViewRegistry.RegisterViewWithRegion("SettingsRegion", typeof(Views.AppointmentsSettingsView));
            regionViewRegistry.RegisterViewWithRegion("SettingsRegion", typeof(Views.DoctorsSettingsView));
            regionViewRegistry.RegisterViewWithRegion("MainMenuButtonsRegion", typeof(Menu.AppointmentButtonView));
            GridControlLocalizer.Active = new ModuleGridLocalizer();
        }
    }
}
