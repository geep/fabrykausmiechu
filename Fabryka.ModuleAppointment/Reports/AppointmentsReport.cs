﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Fabryka.Business;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using Fabryka.Common.Services;

namespace Fabryka.ModuleAppointment.Reports
{
    public partial class AppointmentsReport : DevExpress.XtraReports.UI.XtraReport
    {
        public AppointmentsReport()
        {
            InitializeComponent();
        }

        private void xrLabel2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel label = (XRLabel)sender;
            Appointment app = DetailReport.GetCurrentRow() as Appointment;
            if (app != null && (app.Description == null || app.Description.Length == 0))
            {
                label.Text = string.Empty;
            }
        }

        private void xrShape2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRShape shape = (XRShape)sender;
            Appointment app = DetailReport.GetCurrentRow() as Appointment;
            if (app != null)
            {
                shape.ForeColor = app.Type.Color;
                //shape.FillColor = app.Type.Color;
            }
        }

        bool lastVal = true;

        private void xrLabel6_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            Appointment app = DetailReport.GetCurrentRow() as Appointment;

            if (app == null)
                return;

            var endTime = (DateTime) DetailReport.GetPreviousColumnValue("EndTime");
            var startTime = app.StartTime;

            XRLabel label = (XRLabel)sender;

            if (startTime - endTime > TimeSpan.FromMinutes(5))
            {
                label.Text = String.Format("Przerwa: {0:HH:mm} - {1:HH:mm}", endTime, startTime);
                DetailBand band = (DetailBand)label.Parent;

                if (!lastVal)
                {
                    foreach (XRControl control in band.Controls)
                    {
                        if (control != label)
                        {
                            control.TopF += 27;
                        }
                    }
                    //band.HeightF += 27;
                }
                lastVal = true;
            }
            else
            {
                //label.HeightF = 0;
                label.Text = String.Empty;

                DetailBand band = (DetailBand)label.Parent;

                if (lastVal)
                {
                    foreach (XRControl control in band.Controls)
                    {
                        if (control != label)
                        {
                            control.TopF -= 27;
                        }
                    }
                    band.HeightF -= 27;
                }
                lastVal = false;
            }
        }

        private void resourceCollection_ResolveSession(object sender, DevExpress.Xpo.ResolveSessionEventArgs e)
        {
            var dataService = ServiceLocator.Current.GetInstance<IDataService>();
            e.Session = dataService.GetCurrentSession();
        }
    }
}
