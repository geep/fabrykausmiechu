﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraScheduler.Localization;

namespace Fabryka.ModuleAppointment
{
    public class ModuleEditorLocalizer : DevExpress.Xpf.Editors.EditorLocalizer
    {
        public override string GetLocalizedString(DevExpress.Xpf.Editors.EditorStringId id)
        {
            if (id == DevExpress.Xpf.Editors.EditorStringId.Today)
                return "Dzisiaj";
            if (id == DevExpress.Xpf.Editors.EditorStringId.SelectAll)
                return "(Wszystkie)";
            return base.GetLocalizedString(id);
        }
    }

    public class ModuleGridLocalizer : DevExpress.Xpf.Grid.GridControlLocalizer
    {
        public override string GetLocalizedString(DevExpress.Xpf.Grid.GridControlStringId id)
        {
            if (id == DevExpress.Xpf.Grid.GridControlStringId.GridNewRowText)
                return "Kliknij by dodać nową pozycję";
            return base.GetLocalizedString(id);
        }
    }

    public class ModuleSchedulerControlLocalizer : DevExpress.Xpf.Scheduler.SchedulerControlLocalizer
    {
        public override string GetLocalizedString(DevExpress.Xpf.Scheduler.SchedulerControlStringId id)
        {
            if (id == DevExpress.Xpf.Scheduler.SchedulerControlStringId.ButtonCaption_Cancel)
                return "Anuluj";
            if (id == DevExpress.Xpf.Scheduler.SchedulerControlStringId.Caption_GotoDate)
                return "Przejdź do daty";
            if (id == DevExpress.Xpf.Scheduler.SchedulerControlStringId.Form_Date)
                return "Data:";
            if (id == DevExpress.Xpf.Scheduler.SchedulerControlStringId.Form_ShowIn)
                return "Pokaż w:";
            if (id == DevExpress.Xpf.Scheduler.SchedulerControlStringId.ButtonCaption_Delete)
                return "Usuń";
            if (id == DevExpress.Xpf.Scheduler.SchedulerControlStringId.ButtonCaption_Delete)
                return "Usuń";
            return base.GetLocalizedString(id);
        }
    }

    public class ModuleSchedulerLocalizer : SchedulerLocalizer
    {
        public override string GetLocalizedString(SchedulerStringId id)
        {
            if (id == SchedulerStringId.Appointment_StartContinueText)
                return "Od {0}";
            if (id == SchedulerStringId.Appointment_EndContinueText)
                return "Do {0}";
            if (id == SchedulerStringId.Abbr_Minute || id == SchedulerStringId.Abbr_Minutes)
                return "Min.";
            if (id == SchedulerStringId.MenuCmd_DeleteAppointment)
                return "Usuń";
            if (id == SchedulerStringId.MenuCmd_GotoDate)
                return "Przejdź do daty...";

            if (id == SchedulerStringId.MenuCmd_GotoToday)
                return "Przejdź do dzisiaj";

            if (id == SchedulerStringId.MenuCmd_NewAppointment)
                return "Nowa wizyta";

            if (id == SchedulerStringId.MenuCmd_NewAllDayEvent)
                return "Nowe zdarzenie całodniowe";

            if (id == SchedulerStringId.MenuCmd_OpenAppointment)
                return "Edytuj";

            if (id == SchedulerStringId.MenuCmd_SwitchToDayView)
                return "Dzień";
            if (id == SchedulerStringId.MenuCmd_SwitchToWorkWeekView)
                return "Tydzień pracy";

            if (id == SchedulerStringId.MenuCmd_SwitchViewMenu)
                return "Zmień widok na";

            if (id == SchedulerStringId.MenuCmd_5Minutes)
                return "5 minut";

            if (id == SchedulerStringId.MenuCmd_6Minutes)
                return "6 minut";

            if (id == SchedulerStringId.MenuCmd_10Minutes)
                return "10 minut";

            if (id == SchedulerStringId.MenuCmd_15Minutes)
                return "15 minut";

            if (id == SchedulerStringId.MenuCmd_20Minutes)
                return "20 minut";

            if (id == SchedulerStringId.MenuCmd_30Minutes)
                return "30 minut";

            if (id == SchedulerStringId.MenuCmd_60Minutes)
                return "60 minut";

            return base.GetLocalizedString(id);
        }
    }


}
