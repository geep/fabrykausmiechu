﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.ViewModel;
using DevExpress.Xpo;
using Fabryka.Business;
using Fabryka.Common.Services;
using Microsoft.Practices.ServiceLocation;

namespace Fabryka.ModuleAppointment.ViewModels
{
    [Export]
    public class AppointmentsSettingsViewModel : NotificationObject
    {

        public string ViewName
        {
            get
            {
                return "Terminarz";
            }
        }

        private IDataService _dataService;

        private XPCollection<AppointmentType> _Types;
        private XPCollection<AppointmentResource> _Resources;

        public XPCollection<AppointmentType> Types
        {
            get
            {
                return _Types;
            }
            set
            {
                _Types = value;
                RaisePropertyChanged(() => Types);
            }
        }

        public XPCollection<AppointmentResource> Resources
        {
            get
            {
                return _Resources;
            }
            set
            {
                _Resources = value;
                RaisePropertyChanged(() => Resources);
            }
        }

        

        public AppointmentsSettingsViewModel()
        {
            _dataService = ServiceLocator.Current.GetInstance<IDataService>();
            Types = _dataService.GetAppointmentTypes();
            Resources = _dataService.GetAppointmentResources();
        }
    }
}
