﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Xpf.Scheduler.UI;
using System.ComponentModel.Composition;
using System.Globalization;
using Fabryka.Business;
using System.ComponentModel;
using DevExpress.Xpo;
using Fabryka.CommonWPF.Converters;
using Fabryka.Common.Services;


namespace Fabryka.ModuleAppointment.ViewModels
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AppointmentEditViewModel : AppointmentFormController, IDataErrorInfo
    {
        private TimeSpan _defaultTimeSpan;

        private IDataService dataService;
        private ISelectedPatientService patientService;

        private const int Existing = 0;
        private const int Temporary = 1;

        public AppointmentEditViewModel(ISelectedPatientService _patientService, DevExpress.Xpf.Scheduler.SchedulerControl control, DevExpress.XtraScheduler.Appointment apt, IDataService _dataService)
            : base(control, apt)
        {
            dataService = _dataService;
            patientService = _patientService;
            if (IsNewAppointment)
            {
                _defaultTimeSpan = DisplayEnd - DisplayStart;
            }

            if (IsNewAppointment || !(bool)EditedAppointmentCopy.CustomFields["IsNewPatient"])
            {
                SelectedPatientType = Existing;

                if (IsNewAppointment)
                {
                    if (patientService.SelectedPatient != null)
                        SelectedPatient = dataService.GetPatientByOid(patientService.SelectedPatient.Oid, true);
                    else
                        SelectedPatient = null;
                }
            }
            else
            {
                SelectedPatientType = Temporary;
            }

        }

        //TODO: Dodaj obrazki do stateów, zostaw nazwy w view'ach.
        public static IEnumerable<Tuple<AppointmentState, string>> States
        {
            get
            {
                return Enum.GetValues(typeof(AppointmentState))
                    .OfType<AppointmentState>()
                    .Select((st => 
                        Tuple.Create(st, EnumToDescriptionConverter.GetEnumDescription(st))));
            }
        }

        public string TimeEditMask
        {
            get { return CultureInfo.CurrentCulture.DateTimeFormat.LongTimePattern; }
        }

        private Patient _oldSelectedPatient;

        private int _SelectedPatientType;
        public int SelectedPatientType
        {
            get
            {
                return _SelectedPatientType;
            }
            set
            {
                _SelectedPatientType = value;
                if (value == Temporary)
                {
                    _oldSelectedPatient = SelectedPatient;
                    SelectedPatient = null;
                    EditedAppointmentCopy.CustomFields["IsNewPatient"] = true;
                }
                else
                {
                    if (_oldSelectedPatient != null)
                        SelectedPatient = _oldSelectedPatient;
                    EditedAppointmentCopy.CustomFields["IsNewPatient"] = false;
                }
            }
        }

        public Patient SelectedPatient
        {
            get
            {
                return (Patient)EditedAppointmentCopy.CustomFields["Patient"];
            }

            set
            {
                EditedAppointmentCopy.CustomFields["Patient"] = value;
            }
        }

        public string PatientFirstName
        {
            get
            {
                return (string)EditedAppointmentCopy.CustomFields["PatientFirstName"];
            }
            set
            {
                EditedAppointmentCopy.CustomFields["PatientFirstName"] = value.NullTrim();
            }
        }

        public string PatientLastName
        {
            get
            {
                return (string)EditedAppointmentCopy.CustomFields["PatientLastName"];
            }
            set
            {
                EditedAppointmentCopy.CustomFields["PatientLastName"] = value.NullTrim();
            }
        }

        public string PatientCellphoneNumber
        {
            get
            {
                 return (string) EditedAppointmentCopy.CustomFields["PatientCellphoneNumber"];
            }
            set
            {
                EditedAppointmentCopy.CustomFields["PatientCellphoneNumber"] = value.NullTrim();
            }
        }

        public AppointmentState State
        {
            get
            {
                if (EditedAppointmentCopy.CustomFields["State"] == null)
                    return AppointmentState.Unknown;
                return (AppointmentState) EditedAppointmentCopy.CustomFields["State"];
            }

            set
            {
                EditedAppointmentCopy.CustomFields["State"] = value;
            }
        }

        public int StateInt
        {
            get
            {
                return (int)State;
            }
            set
            {
                State = (AppointmentState)value;
            }
        }

        public int TypeId
        {
            get
            {
                if (EditedAppointmentCopy.CustomFields["TypeId"] == null)
                    return dataService.GetDefaultAppointmentType(AllDay).Oid;
                return (int)EditedAppointmentCopy.CustomFields["TypeId"];
            }
            set
            {
                if (EditedAppointmentCopy.CustomFields["TypeId"] != null && (int)EditedAppointmentCopy.CustomFields["TypeId"] == value)
                    return;
                EditedAppointmentCopy.CustomFields["TypeId"] = value;
                if (IsNewAppointment && !AllDay)
                {
                    // TODO: Pamiętaj, użyj tu zmiennej? DefaultTime? Whateva.
                    var duration = (Type == null || Type.Duration.TotalSeconds == 0) ? _defaultTimeSpan : Type.Duration;
                    DisplayEnd = DisplayStart + duration;
                    NotifyPropertyChanged("DisplayEndDate");
                    NotifyPropertyChanged("DisplayEndTime");
                }
            }
        }

        public AppointmentType Type
        {
            get
            {
                return dataService.GetAppointmentTypes().First((type) => type.Oid == (TypeId));
            }
        }

        public IEnumerable<AppointmentType> Types
        {
            get
            {
                return dataService.GetAppointmentTypes().Where((type) => type.IsAllDay == AllDay);
            }
        }

        public XPCollection<Doctor> Doctors
        {
            get
            {
                return dataService.GetDoctors(true);
            }
        }

        public XPCollection<Patient> Patients
        {
            get
            {
                return dataService.GetPatients();
            }
        }

        public Doctor CurrentDoctor
        {
            get
            {
                return EditedAppointmentCopy.CustomFields["Doctor"] as Doctor;
            }

            set
            {
                EditedAppointmentCopy.CustomFields["Doctor"] = value;
            }
        }

        public override bool IsAppointmentChanged()
        {
            if (base.IsAppointmentChanged())
                return true;

            return false;
        }

        public override void ApplyChanges()
        {
            dataService.BeginTransaction();
            try
            {
                base.ApplyChanges();
            }
            finally
            {
                dataService.EndTransaction(UpdateType.Appointments);
            }
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get 
            {
                string result = null;

                //TODO: Dodaj tu coś.
                return result;
            }
        }
        
    }
}
