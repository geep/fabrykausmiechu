﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Commands;
using System.ComponentModel.Composition;
using DevExpress.Xpo;
using Fabryka.Business;
using DevExpress.Xpf.Scheduler;
using Fabryka.ModuleAppointment.Reports;
using DevExpress.XtraReports.UI;
using System.Windows;
using Microsoft.Practices.ServiceLocation;
using Fabryka.Common.Services;
using XtraSch = DevExpress.XtraScheduler;

namespace Fabryka.ModuleAppointment.ViewModels
{
    [Export]
    public class AppointmentsViewModel : NotificationObject
    {
        public DelegateCommand PrintAllCommand { get; set; }
        public DelegateCommand<int?> PrintSelectedCommand { get; set; }
        public DelegateCommand<DevExpress.XtraScheduler.SchedulerViewType?> SetViewTypeCommand { get; set; }
        public DelegateCommand<AppointmentType> ChangeAppointmentTypeCommand { get; set; }
        public DelegateCommand<AppointmentState?> ChangeAppointmentStateCommand { get; set; }
        public DelegateCommand OpenSettingsCommand { get; set; }
        public DelegateCommand SelectAppointedPatient { get; private set; }

        public bool? HasNextAppointment { get; private set;}
        public Appointment NextAppointment { get; private set; }
        public DelegateCommand GoToNextAppointment { get; private set; }

        public bool? HasPreviousVisit { get; private set; }
        public Visit PreviousVisit { get; private set; }
        public DelegateCommand GoToPreviousVisit { get; private set; }

        private XtraSch.TimeInterval lastInterval;

        private SchedulerControl _Control;

        public SchedulerControl Control
        {
            get
            {
                return _Control;
            }
            set
            {
                _Control = value;
                _Control.SelectionChanged += (s, e) => SelectAppointedPatient.RaiseCanExecuteChanged();
                _Control.Storage.FetchAppointments += Storage_FetchAppointments;
            }
        }

        private readonly ISelectedPatientService selectedPatientService;

        private readonly IDataService dataService;

        [ImportingConstructor]
        public AppointmentsViewModel(ISelectedPatientService _selectedPatientService, IDataService _dataService)
        {
            selectedPatientService = _selectedPatientService;
            selectedPatientService.SelectionChanged += selectedPatientService_SelectionChanged;

            dataService = _dataService;

            GoToNextAppointment = new DelegateCommand(
                () => Control.ActiveView.GotoTimeInterval(new DevExpress.XtraScheduler.TimeInterval(NextAppointment.StartTime, NextAppointment.EndTime)),
                () => HasNextAppointment == true);

            GoToPreviousVisit = new DelegateCommand(
                () => Control.ActiveView.GotoTimeInterval(new DevExpress.XtraScheduler.TimeInterval(PreviousVisit.Time, PreviousVisit.Time)),
                () => HasPreviousVisit == true);

            UpdateNextPrevious();

            PrintAllCommand = new DelegateCommand(
                () => PrintReport(-1),
                () => !IsActiveViewWeek );

            PrintSelectedCommand = new DelegateCommand<int?>(
                resId => PrintReport(resId.HasValue ? resId.Value : -1),
                resId => !IsActiveViewWeek);

            SetViewTypeCommand = new DelegateCommand<DevExpress.XtraScheduler.SchedulerViewType?>(
                type => ActiveViewType = type ?? DevExpress.XtraScheduler.SchedulerViewType.Day);

            ChangeAppointmentTypeCommand = new DelegateCommand<AppointmentType>(
                type => ChangeAppointmentType(type));

            ChangeAppointmentStateCommand = new DelegateCommand<AppointmentState?>(
                state => ChangeAppointmentState(state ?? AppointmentState.Unknown));

            SelectAppointedPatient = new DelegateCommand(
                () => ChangeSelectedPatient(),
                () => CanChangeSelectedPatient()
                );

            InitializeStorage();
        }

        void selectedPatientService_SelectionChanged(object sender, EventArgs e)
        {
            UpdateNextPrevious();
        }

        public void UpdateNextPrevious()
        {
            var patient = selectedPatientService.SelectedPatient;
            if (patient == null)
            {
                HasNextAppointment = null;
                HasPreviousVisit = null;
            }
            else
            {
                if (patient.Visits.Count == 0)
                {
                    HasPreviousVisit = false;
                }
                else
                {
                    HasPreviousVisit = true;
                    PreviousVisit = patient.Visits.OrderByDescending((v) => v.Time).First();
                }

                var app = patient.Appointments.OrderBy((a) => a.StartTime).Where((a) => a.StartTime > DateTime.Now).FirstOrDefault();

                if (app == null)
                {
                    HasNextAppointment = false;
                }
                else
                {
                    HasNextAppointment = true;
                    NextAppointment = app;
                }
            }

            RaisePropertyChanged(() => HasPreviousVisit);
            RaisePropertyChanged(() => HasNextAppointment);
            RaisePropertyChanged(() => PreviousVisit);
            RaisePropertyChanged(() => NextAppointment);
            GoToNextAppointment.RaiseCanExecuteChanged();
            GoToPreviousVisit.RaiseCanExecuteChanged();
        }
        private void InitializeStorage()
        {
            _Appointments = dataService.GetAppointments();

            _Appointments.CollectionChanged += _Appointments_CollectionChanged;
            _Appointments.ListChanged += new System.ComponentModel.ListChangedEventHandler(_Appointments_ListChanged);

            _Resources = dataService.GetAppointmentResources();

            _Types = dataService.GetAppointmentTypes();

            if (_Resources.Count == 0)
            {
                PopulateTables();
            }
        }

        void Storage_FetchAppointments(object sender, DevExpress.XtraScheduler.FetchAppointmentsEventArgs e)
        {
            DateTime start = e.Interval.Start;
            DateTime end = e.Interval.End;
            // Specify the time range to pad the queried time interval
            TimeSpan padding = TimeSpan.FromDays(7);

            // Check if the requested interval is outside the lastFetchedInterval
            if (lastInterval == null || start <= lastInterval.Start || end >= lastInterval.End)
            {
                lastInterval = new XtraSch.TimeInterval(start - padding, end + padding);
                _Appointments.Criteria =
                    DevExpress.Data.Filtering.CriteriaOperator
                        .Parse("(StartTime >= (?) AND StartTime < (?)) OR (EndTime >= (?) AND EndTime < (?))",
                            lastInterval.Start, lastInterval.End, lastInterval.Start, lastInterval.End);
            }
        }

        void _Appointments_ListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
        {
            if (e.ListChangedType == System.ComponentModel.ListChangedType.Reset)
                RaisePropertyChanged(() => Appointments);
        }

        private void PopulateTables()
        {
            // TODO: Przenieś to

            dataService.BeginTransaction();

            try
            {

                var r = new AppointmentResource() { Label = "Fotel 1", Color = System.Drawing.Color.RoyalBlue };
                _Resources.Add(r);

                r = new AppointmentResource() { Label = "Fotel 2", Color = System.Drawing.Color.IndianRed };
                _Resources.Add(r);


                var t = new AppointmentType()
                {
                    Label = "Konsultacja",
                    Color = System.Drawing.Color.LightBlue,
                    Position = 1,
                };
                _Types.Add(t);

                t = new AppointmentType()
                {
                    Label = "Plan leczenia",
                    Color = System.Drawing.Color.LightGreen,
                    Position = 2,
                };
                _Types.Add(t);

                t = new AppointmentType()
                {
                    Label = "Aparat stały",
                    Color = System.Drawing.Color.PaleVioletRed,
                    Position = 3,
                    Duration = new TimeSpan(1, 0, 0),
                };
                t.Save();
                _Types.Add(t);

                t = new AppointmentType()
                {
                    Label = "Awaria",
                    Color = System.Drawing.Color.PaleVioletRed,
                    Position = 3,
                    IsAllDay = true,
                    IsDefault = true,
                };

                _Types.Add(t);

                t = new AppointmentType()
                {
                    Label = "Brak",
                    Color = System.Drawing.Color.White,
                    Position = 0,
                    IsDefault = true,
                };

                _Types.Add(t);
            }
            finally
            {
                dataService.EndTransaction();
            }
        }


        private void ChangeAppointmentType(AppointmentType type)
        {
            
            DevExpress.XtraScheduler.AppointmentBaseCollection appointments = Control.SelectedAppointments;

            dataService.BeginTransaction();

            try
            {

                for (int i = 0; i < appointments.Count; i++)
                {
                    Appointment a = Control.Storage.GetObjectRow(appointments[i]) as Appointment;
                    a.Type = type;
                }
            }
            finally
            {
                dataService.EndTransaction(UpdateType.Appointments);
            }
        }

        private void ChangeAppointmentState(AppointmentState state)
        {
            DevExpress.XtraScheduler.AppointmentBaseCollection appointments = Control.SelectedAppointments;

            dataService.BeginTransaction();

            try
            {
                for (int i = 0; i < appointments.Count; i++)
                {
                    Appointment a = Control.Storage.GetObjectRow(appointments[i]) as Appointment;
                    a.State = state;
                }
            }
            finally
            {
                dataService.EndTransaction(UpdateType.Appointments);
            }
        }

        private void ChangeSelectedPatient()
        {
            DevExpress.XtraScheduler.AppointmentBaseCollection appointments = Control.SelectedAppointments;

            if (appointments.Count != 1)
                return;

            var appointment = Control.Storage.GetObjectRow(appointments[0]) as Appointment;

            if (appointment.IsNewPatient == true || appointment.Patient == null)
                return;

            selectedPatientService.SelectedPatient = dataService.GetPatientByOid(appointment.Patient.Oid);
        }

        private bool CanChangeSelectedPatient()
        {
            DevExpress.XtraScheduler.AppointmentBaseCollection appointments = Control.SelectedAppointments;

            if (appointments.Count != 1)
                return false;

            var appointment = Control.Storage.GetObjectRow(appointments[0]) as Appointment;

            if (appointment.IsNewPatient == true || appointment.Patient == null)
                return false;

            return true;
        }

        private XPCollection<Appointment> _Appointments;
        public XPCollection<Appointment> Appointments
        {
            get
            {
                return _Appointments;
            }
        }

        private XPCollection<AppointmentResource> _Resources;
        public XPCollection<AppointmentResource> Resources
        {
            get
            {
                return _Resources;
            }
        }

        private XPCollection<AppointmentType> _Types;
        public XPCollection<AppointmentType> Types
        {
            get
            {
                return _Types;
            }
        }

        private DevExpress.XtraScheduler.SchedulerViewType _ActiveViewType;
        public DevExpress.XtraScheduler.SchedulerViewType ActiveViewType
        {
            get { return _ActiveViewType; }
            set
            {
                if (_ActiveViewType == value)
                    return;
                _ActiveViewType = value;
                RaisePropertyChanged(() => ActiveViewType);
                RaisePropertyChanged(() => IsActiveViewWeek);
                PrintAllCommand.RaiseCanExecuteChanged();
                PrintSelectedCommand.RaiseCanExecuteChanged();
            }
        }

        private DevExpress.XtraScheduler.TimeOfDayInterval _WorkHours = new DevExpress.XtraScheduler.TimeOfDayInterval(TimeSpan.FromHours(7), TimeSpan.FromHours(20));
        public DevExpress.XtraScheduler.TimeOfDayInterval WorkHours
        {
            get { return _WorkHours; }
            set 
            { 
                _WorkHours = value;
                RaisePropertyChanged(() => WorkHours);
            }
        }

        public bool IsActiveViewWeek
        {
            get
            {
                return ActiveViewType == DevExpress.XtraScheduler.SchedulerViewType.WorkWeek;
            }

            set
            {
                if (IsActiveViewWeek == value)
                    return;

                if (value)
                    ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.WorkWeek;
                else
                    ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Day;
            }
        }


        private IList<DateTime> _SelectedDates;
        public IList<DateTime> SelectedDates
        {
            get { return _SelectedDates; }
            set { _SelectedDates = value; }
        }

        void _Appointments_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            if (e.CollectionChangedType != XPCollectionChangedType.AfterRemove)
            {
                return;
            }
            dataService.BeginTransaction();

            try
            {
                if (e.CollectionChangedType == XPCollectionChangedType.AfterRemove)
                {
                    Appointment apt = e.ChangedObject as Appointment;
                    if (apt != null)
                    {
                        apt.Delete();
                        apt.Save();
                    }
                }
            }
            finally
            {
                dataService.EndTransaction(UpdateType.Appointments);
            }

            UpdateNextPrevious();
        }

        private void PrintReport(int resId)
        {
            AppointmentsReport report =
                                    new AppointmentsReport();
            report.SelectedDate.Value = SelectedDates.First();
            report.SelectedDate.Visible = false;
            report.EndDate.Value = SelectedDates.First().AddDays(1);
            report.EndDate.Visible = false;
            report.SelectedResourceId.Value = resId;
            ReportPrintTool printTool = new ReportPrintTool(report);
            printTool.ShowPreviewDialog();
        }

        internal void StartUpdates()
        {
            dataService.StartAppointmentUpdates();
        }

        internal void StopUpdates()
        {
            dataService.StopAppointmentUpdates();
        }
    }
}
