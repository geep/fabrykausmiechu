﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using Microsoft.Practices.Prism.ViewModel;
using Fabryka.Business;
using Fabryka.Common.Services;
using Microsoft.Practices.ServiceLocation;

namespace Fabryka.ModuleAppointment.ViewModels
{
    public class AppointmentInPlaceEditViewModel : NotificationObject
    {
        private bool _isNew;

        private IDataService dataService;

        public AppointmentInPlaceEditViewModel(DevExpress.XtraScheduler.Appointment apt, bool isNew)
        {
            _isNew = isNew;

            dataService = ServiceLocator.Current.GetInstance<IDataService>();

            Appointment = apt;
            if (Appointment.CustomFields["PatientFirstName"] == null)
            {
                FullName = Appointment.CustomFields["PatientLastName"] as string;
            }
            else
            {
                FullName = String.Format("{0}, {1}", Appointment.CustomFields["PatientLastName"] ?? String.Empty,
                    Appointment.CustomFields["PatientFirstName"]);
            }
            if (Appointment.CustomFields["TypeId"] == null)
                Type = dataService.GetDefaultAppointmentType(Appointment.AllDay);
            else Type = dataService.GetAppointmentTypes().First((type) => type.Oid == (int)Appointment.CustomFields["TypeId"]);
        }

        private Business.AppointmentType _Type;
        private string _FullName;

        public string FullName
        {
            get
            {
                return _FullName;
            }
            set
            {
                _FullName = value;
                RaisePropertyChanged(() => FullName);
            }
        }

        public DevExpress.XtraScheduler.Appointment Appointment { get; set; }

        public Business.AppointmentType Type
        {
            get
            {
                return _Type;
            }
            set
            {
                _Type = value;
                RaisePropertyChanged(() => Type);
            }
        }

        public IEnumerable<Business.AppointmentType> Types
        {
            get
            {
                //XPCollection<Business.AppointmentType>  = new XPCollection<Business.AppointmentType>(new DevExpress.Data.Filtering.BinaryOperator("IsAllDay", Appointment.AllDay),
                //                    new SortProperty("Position", DevExpress.Xpo.DB.SortingDirection.Ascending)); ;
                return dataService.GetAppointmentTypes().Where((type) => type.IsAllDay == Appointment.AllDay);
            }
        }

        public void ApplyChanges()
        {
            Appointment.CustomFields["TypeId"] = Type.Oid;

            if (_isNew && Type.Duration != null && Type.Duration.TotalSeconds > 0)
            {
                Appointment.Duration = Type.Duration.Duration();
            }

            if (String.IsNullOrWhiteSpace(FullName))
                return;

            if (FullName.Contains(','))
            {
                var names = FullName.Split(new char[] { ',' }, 2, StringSplitOptions.None);
                Appointment.CustomFields["PatientLastName"] = names[0].NullTrim();
                Appointment.CustomFields["PatientFirstName"] = names[1].NullTrim();
            }
            else
            {
                Appointment.CustomFields["PatientLastName"] = FullName.NullTrim();
            }
        }
    }
}
