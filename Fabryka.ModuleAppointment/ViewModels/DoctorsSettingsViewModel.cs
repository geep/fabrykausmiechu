﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.ViewModel;
using DevExpress.Xpo;
using Fabryka.Business;
using System.ComponentModel.Composition;
using Fabryka.Common.Services;
using Microsoft.Practices.ServiceLocation;

namespace Fabryka.ModuleAppointment.ViewModels
{
    [Export]
    public class DoctorsSettingsViewModel : NotificationObject
    {
        private IDataService dataService;

        public string ViewName
        {
            get
            {
                return "Lekarze";
            }
        }

        private XPCollection<Doctor> _Doctors;

        public XPCollection<Doctor> Doctors
        {
            get
            {
                return _Doctors;
            }
            set
            {
                _Doctors = value;
                RaisePropertyChanged(() => Doctors);
            }
        }

        public DoctorsSettingsViewModel()
        {
            dataService = ServiceLocator.Current.GetInstance<IDataService>();
            Doctors = dataService.GetDoctors();
        }

    }
}
