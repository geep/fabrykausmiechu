﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fabryka.ModuleAppointment.ViewModels
{
    public class ViewTypeListItem
    {
        public string Caption { get; set; }
        public DevExpress.XtraScheduler.SchedulerViewType ViewType { get; set; }
    }
}
