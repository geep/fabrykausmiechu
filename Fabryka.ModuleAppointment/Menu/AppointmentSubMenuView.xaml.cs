﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;

namespace Fabryka.ModuleAppointment.Menu
{
    /// <summary>
    /// Interaction logic for PatientSubMenuView.xaml
    /// </summary>
    [Export("AppointmentSubMenuView", typeof(AppointmentSubMenuView))]
    public partial class AppointmentSubMenuView : UserControl
    {
        public AppointmentSubMenuView()
        {
            InitializeComponent();
        }

        [Import]
        public AppointmentSubMenuViewModel ViewModel
        {
            set
            {
                DataContext = value;
            }
        }
    }
}
