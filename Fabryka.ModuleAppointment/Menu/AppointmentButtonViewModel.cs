﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.Composition;
using Fabryka.CommonWPF.Menu;
using Microsoft.Practices.Prism.Regions;

namespace Fabryka.ModuleAppointment.Menu
{
    [Export]
    public class AppointmentButtonViewModel : MainMenuButtonViewModel
    {
        public AppointmentButtonViewModel()
        {
            UriName = "AppointmentsView";
            SubMenuUriName = "AppointmentSubMenuView";
        }
    }
}
