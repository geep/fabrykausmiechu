﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Fabryka.Common.Services;
using Microsoft.Practices.Prism.Commands;
using Fabryka.ModuleAppointment.ViewModels;

namespace Fabryka.ModuleAppointment.Menu
{
    [Export]
    public class AppointmentSubMenuViewModel
    {
        private readonly AppointmentsViewModel appointmentsViewModel;

        public DelegateCommand DayView { get; private set; }
        public DelegateCommand WeekView { get; private set; }
        public DelegateCommand PrintAll { get; private set; }

        [ImportingConstructor]
        public AppointmentSubMenuViewModel(AppointmentsViewModel _appointmentsViewModel)
        {
            appointmentsViewModel = _appointmentsViewModel;

            DayView = new DelegateCommand(() => appointmentsViewModel.IsActiveViewWeek = false, 
                () => appointmentsViewModel.IsActiveViewWeek);
            WeekView = new DelegateCommand(() => appointmentsViewModel.IsActiveViewWeek = true,
                () => !appointmentsViewModel.IsActiveViewWeek);
            appointmentsViewModel.PropertyChanged += (sender, args) =>
                {
                    if (args.PropertyName == "IsActiveViewWeek")
                    {
                        DayView.RaiseCanExecuteChanged();
                        WeekView.RaiseCanExecuteChanged();
                    }
                };
            PrintAll = appointmentsViewModel.PrintAllCommand;
        }
    }
}
