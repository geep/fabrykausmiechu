﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel.Composition;

namespace Fabryka.ModuleNFZ.Report
{
    /// <summary>
    /// Interaction logic for ReportView.xaml
    /// </summary>
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class ReportView : Window
    {
        public ReportView()
        {
            InitializeComponent();
        }


        [Import]
        public ReportViewModel ViewModel
        {
            get
            {
                return DataContext as ReportViewModel;
            }
            set
            {
                DataContext = value;
                value.RunSave = (month) => RunSave(month);
            }
        }

        private void RunSave(string month)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "raport" + month; // Default file name
            dlg.DefaultExt = ".csv"; // Default file extension
            dlg.Filter = "Plik CSV (.csv)|*.csv"; // Filter files by extension 

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results 
            if (result == true)
            {
                // Save document 
                string filename = dlg.FileName;
                tableView1.ExportToCsv(filename);
            }
        }
    }
}
