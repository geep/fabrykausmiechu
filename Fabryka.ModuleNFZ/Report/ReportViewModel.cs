﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.ViewModel;
using System.ComponentModel.Composition;
using Fabryka.Common.Services;
using Microsoft.Practices.ServiceLocation;
using DevExpress.Xpo;
using Fabryka.Business;
using Microsoft.Practices.Prism.Commands;

namespace Fabryka.ModuleNFZ.Report
{
    [Export]
    public class ReportViewModel : NotificationObject
    {
        private bool _Generated;
        private DateTime _SelectedMonth;
        private IDataService dataService;


        public DateTime SelectedMonth
        {
            get { return _SelectedMonth; }
            set
            {
                _SelectedMonth = new DateTime(value.Year, value.Month, 1); ;
                RaisePropertyChanged(() => SelectedMonth);
            }
        }


        public bool Generated
        {
            get { return _Generated; }
            set
            {
                _Generated = value;
                RaisePropertyChanged(() => Generated);
                RaisePropertyChanged(() => SelectedVisits);
                Save.RaiseCanExecuteChanged();
            }
        }


        public IEnumerable<NFZCompletedProcedure> SelectedVisits
        {
            get
            {
                if (!Generated)
                {
                    return null;
                }

                return dataService.GetRangedNFZVisits(SelectedMonth, SelectedMonth.AddMonths(1));
            }
        }

        public Action<string> RunSave { get; set; }
        public DelegateCommand Generate { get; private set; }
        public DelegateCommand Save { get; private set; }

        public ReportViewModel()
        {
            dataService = ServiceLocator.Current.GetInstance<IDataService>();
            var now = DateTime.Now;
            SelectedMonth = new DateTime(now.Year, now.Month, 1);

            Generate = new DelegateCommand(
                () => Generated = true);
            Save = new DelegateCommand(() => RunSave(SelectedMonth.ToString("-MM-yyyy")), () => Generated);
        }
    }
}
