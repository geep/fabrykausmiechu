﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using DevExpress.Xpf.Editors;

namespace Fabryka.ModuleNFZ.Settings
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class NFZSettingsView : UserControl
    {
        //[ImportingConstructor]
        public NFZSettingsView()
        {
            InitializeComponent();
        }

        [Import]
        public NFZSettingsViewModel ViewModel
        {
            get
            {
                return DataContext as NFZSettingsViewModel;
            }
            set
            {
                DataContext = value;
            }
        }
    }
}
