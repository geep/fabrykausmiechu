using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.ViewModel;
using System.ComponentModel.Composition;
using Fabryka.Common.Services;
using Microsoft.Practices.ServiceLocation;
using DevExpress.Xpo;
using Fabryka.Business;
using Microsoft.Practices.Prism.Commands;

namespace Fabryka.ModuleNFZ.Settings
{
    [Export]
    public class NFZSettingsViewModel : NotificationObject
    {
        private IDataService dataService;

        public string ViewName
        {
            get
            {
                return "NFZ";
            }
        }

        public XPCollection<NFZProcedure> NFZProcedures
        {
            get
            {
                return dataService.GetNFZProcedures();
            }
        }

        public DelegateCommand Generate { get; private set; }

        public NFZSettingsViewModel()
        {
            dataService = ServiceLocator.Current.GetInstance<IDataService>();
            Generate = new DelegateCommand(() => ServiceLocator.Current.GetInstance<Report.ReportView>().ShowDialog());
        }


    }
}
