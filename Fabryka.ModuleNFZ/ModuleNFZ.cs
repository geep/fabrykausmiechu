﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using Microsoft.Practices.Prism.Modularity;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.Regions;

namespace Fabryka.ModuleNFZ
{
    [ModuleExport(typeof(ModuleNFZ), InitializationMode = InitializationMode.WhenAvailable)]
    public class ModuleNFZ : IModule
    {
        private readonly IRegionViewRegistry regionViewRegistry;

        [ImportingConstructor]
        public ModuleNFZ(IRegionViewRegistry registry)
        {
            regionViewRegistry = registry;
        }

        public void Initialize()
        {
           regionViewRegistry.RegisterViewWithRegion("SettingsRegion", typeof(Settings.NFZSettingsView));
           //regionViewRegistry.RegisterViewWithRegion("MainMenuButtonsRegion", typeof(Menu.VisitsButtonView));
        }
    }
}
