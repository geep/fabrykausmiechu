﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using DevExpress.Xpo;
using Fabryka.Business;
using Microsoft.Practices.Prism.ViewModel;
using Fabryka.Common.Services;
using Microsoft.Practices.Prism.Commands;

namespace Fabryka.ModuleControls.Search
{
    [Export]
    public class SearchViewModel : NotificationObject
    {
        private Patient _ActualPatient;
        private IDataService dataService;
        private readonly ISelectedPatientService selectedService;

        public XPCollection<Patient> Patients { get; set; }


        public Patient ActualPatient
        {
            get { return _ActualPatient; }
            set 
            {
                if (_ActualPatient == null || value == null)
                {
                    if (_ActualPatient == value)
                        return;
                }
                else
                {
                    if (_ActualPatient.Oid == value.Oid)
                        return;

                }

                _ActualPatient = value;

                if (_ActualPatient != null)
                {
                    SelectedPatient = dataService.GetPatientByOid(_ActualPatient.Oid);
                }
                else
                    SelectedPatient = null;

                RaisePropertyChanged(() => ActualPatient);
            }
        }

        private Patient _SelectedPatient;
        public Patient SelectedPatient
        {
            get
            {
                return _SelectedPatient;
            }
            set
            {
                if (SelectedPatient == null || value == null)
                {
                    if (SelectedPatient == value)
                        return;
                }
                else
                {
                    if (_SelectedPatient.Oid == value.Oid)
                        return;
                }

                _SelectedPatient = value;
                selectedService.SelectionChanged -= selectedService_SelectionChanged;
                selectedService.SelectedPatient = value;
                selectedService.SelectionChanged += selectedService_SelectionChanged;


                RaisePropertyChanged(() => SelectedPatient);
                GoBackCommand.RaiseCanExecuteChanged();
                GoForewardCommand.RaiseCanExecuteChanged();
                
            }
        }

        public DelegateCommand GoBackCommand { get; set; }
        public DelegateCommand GoForewardCommand { get; set; }

        /// <summary>
        /// Initializes a new instance of the SearchViewModel class.
        /// </summary>
        [ImportingConstructor]
        public SearchViewModel(ISelectedPatientService _selectedService, IDataService _dataService)
        {
            dataService = _dataService;

            GoBackCommand = new DelegateCommand(
                () => selectedService.GoBack(),
                () => selectedService.CanGoBack);

            GoForewardCommand = new DelegateCommand(
                () => selectedService.GoForeward(),
                () => selectedService.CanGoForeward);

            Patients = _dataService.GetPatients();

            selectedService = _selectedService;
            selectedService.SelectionChanged += selectedService_SelectionChanged;
            SelectedPatient = selectedService.SelectedPatient;
            if (SelectedPatient != null)
                _ActualPatient = dataService.GetPatientByOid(SelectedPatient.Oid, true);
            else
                _ActualPatient = null;

        }

        void selectedService_SelectionChanged(object sender, EventArgs e)
        {
            SelectedPatient = selectedService.SelectedPatient;
            if (SelectedPatient != null)
                _ActualPatient = dataService.GetPatientByOid(SelectedPatient.Oid, true);
            else
                _ActualPatient = null;
            RaisePropertyChanged(() => ActualPatient);
            GoBackCommand.RaiseCanExecuteChanged();
            GoForewardCommand.RaiseCanExecuteChanged();
        }
    }
}
