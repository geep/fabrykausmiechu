﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.ComponentModel.Composition;
using System.Windows.Markup;
using System.Threading;
using System.Windows.Threading;

namespace Fabryka.ModuleControls.Search
{

    [Export]
    public partial class SearchView : UserControl
    {
        public SearchView()
        {
            Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);
            InitializeComponent();
        }

        [Import]
        public SearchViewModel ViewModel
        {
            set
            {
                DataContext = value;
            }
        }

        private void ColorTextBlock(object sender)
        {
            var block = sender as TextBlock;

            if (block == null)
                return;

            block.TextEffects.Clear();

            if (lookUpEdit1.AutoSearchText == null)
                return;

            var value = lookUpEdit1.AutoSearchText;
            if (String.IsNullOrWhiteSpace(value))
                return;

            int start = block.Text.IndexOf(value, StringComparison.CurrentCultureIgnoreCase);
            if (start == -1)
                return;

            block.TextEffects.Add(new TextEffect()
            {
                Foreground = Brushes.DodgerBlue,
                PositionStart = start,
                PositionCount = value.Length,
            });
        }

        private void TextBlock_Loaded(object sender, RoutedEventArgs e)
        {
            ColorTextBlock(sender);
        }

        private void TextBlock_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            ColorTextBlock(sender);
        }

        private void lookUpEdit1_GotFocus(object sender, RoutedEventArgs e)
        {
            var ed = lookUpEdit1;
            Action act = () =>
            {
                if (ed.SelectionLength != ed.Text.Length)
                    ed.SelectAll();
            };

            Dispatcher.BeginInvoke(act, DispatcherPriority.Render, null);
        }

        private void lookUpEdit1_LostFocus(object sender, RoutedEventArgs e)
        {
            var ed = lookUpEdit1;
            Action act = () =>
            {               
                ed.SelectionLength = 0;
            };

            Dispatcher.BeginInvoke(act, DispatcherPriority.Render, null);
        }
    }
}
