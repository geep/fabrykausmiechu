﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Fabryka.ModuleControls.TeethPicker
{
    public class TeethAlphaConverter: IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Length != 2)
                return null;

            var str = values[0].ToString().Split(',');
            var y = int.Parse(str[0]);
            var x = int.Parse(str[1]);

            bool[,] selection = values[1] as bool[,];

            if (selection == null)
                return null;

            return (selection[y,x] ? 0.5 : 0.0);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

