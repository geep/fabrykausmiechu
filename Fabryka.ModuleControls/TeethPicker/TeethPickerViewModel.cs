﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.ViewModel;
using Fabryka.Common.Services;

namespace Fabryka.ModuleControls.TeethPicker
{
    [Export, PartCreationPolicy(CreationPolicy.Shared), Export(typeof(ITeethPickerService))]
    public class TeethPickerViewModel : NotificationObject, ITeethPickerService
    {
        private string _Result;
        private bool[,] _Selections = new bool[2, 17];

        public bool[,] Selections
        {
            get
            {
                return _Selections;
            }
        }


        public string Result
        {
            get { return GenerateResult(); }
            set
            {
                _Result = value;
            }
        }
        

        public void ChangeSelection(int x, int y)
        {
            _Selections[y, x] = !_Selections[y, x];
            RaisePropertyChanged(() => Selections);
            RaisePropertyChanged(() => Result);
        }

        public string TranslatePosition(int x, int y)
        {
            var result = "";
            if (y == 0)
            {
                if (x >= 8)
                {
                    result += "2";
                }
                else
                {
                    result += "1";
                }
            }
            else
            {
                if (x >= 8)
                {
                    result += "3";
                }
                else
                {
                    result += "4";
                }               
            }

            if (x >= 8)
            {
                x -= 7;
            }
            else
            {
                x = 8 - x;
            }    

            return result + x;
        }

        public string GenerateResult()
        {
            var result = "";
            bool first = true;

            for (int y = 0; y < 2; y++)
            {
                bool start = false;
                int count = 0;
                for (int x = 0; x <= 16; x++)
                {
                    if (_Selections[y, x])
                    {                        
                        if (!start)
                        {
                            result += (first ? "" : ", ") + TranslatePosition(x, y);
                            start = true;
                        }
                        first = false;
                        count++;
                    }
                    else
                    {
                        if (start && count > 1)
                        {
                            result += "-" + TranslatePosition(x-1, y);
                        }
                        start = false;
                        count = 0;
                    }
                }
            }

            return result;
        }

        public void CleanUp()
        {
            for (int y = 0; y < 2; y++)
            {
                for (int x = 0; x <= 16; x++)
                {
                    Selections[y, x] = false;
                }
            }
            RaisePropertyChanged(() => Selections);
        }

        public string GetResult()
        {
            var result = GenerateResult();
            CleanUp();
            return result;
        }
    }
}
