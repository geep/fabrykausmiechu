﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.ComponentModel.Composition;
using System.Windows.Markup;
using System.Threading;
using System.Windows.Threading;
using System.Windows.Input;

namespace Fabryka.ModuleControls.TeethPicker
{
    /// <summary>
    /// Interaction logic for TeethPicker.xaml
    /// </summary>
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class TeethPickerView : UserControl
    {
        private bool mouseDown = false;

        public TeethPickerView()
        {
            InitializeComponent();
        }

        [Import]
        public TeethPickerViewModel ViewModel
        {
            get
            {
                return DataContext as TeethPickerViewModel;
            }
            set
            {
            	DataContext = value;
            }
        }

        private void MarkSelection(object sender)
        {
            int y, x;

            var str = (sender as Border).Tag.ToString().Split(',');
            y = int.Parse(str[0]);
            x = int.Parse(str[1]);

            ViewModel.ChangeSelection(x, y);
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            mouseDown = true;

            MarkSelection(sender);
        }

        private void Border_MouseEnter(object sender, MouseEventArgs e)
        {
            if (!mouseDown)
                return;

            MarkSelection(sender);
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            mouseDown = false;
        }
    }
}
