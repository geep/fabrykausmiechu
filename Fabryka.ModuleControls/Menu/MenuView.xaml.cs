﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Prism.Regions;
using System.ComponentModel.Composition;
using Fabryka.Common.Services;

namespace Fabryka.ModuleControls.Menu
{
    /// <summary>
    /// Interaction logic for MenuView.xaml
    /// </summary>
    [Export]
    public partial class MenuView : UserControl
    {
        public MenuView()
        {
            InitializeComponent();
        }

        [Import]
        public ILoadingService LoadingService { get; set; }

        private void EndOfNavigation(NavigationResult obj)
        {
            LoadingService.IsLoading = false;
        }

        private void SwitchView(string uriString)
        {            
            // Initialize
            var regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();

            // Show Main Region.
            LoadingService.IsLoading = true;
            var moduleARibbonTab = new Uri(uriString, UriKind.Relative);

            Dispatcher.BeginInvoke(new Action(() => regionManager.RequestNavigate("MainRegion", moduleARibbonTab,
                EndOfNavigation)),
                System.Windows.Threading.DispatcherPriority.Background);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            SwitchView("PatientDataView");
         }



        private void button2_Click(object sender, RoutedEventArgs e)
        {
            SwitchView("AppointmentsView");
        }

        private void VisitsButton_Click(object sender, RoutedEventArgs e)
        {
            SwitchView("VisitsView");
        }

        private void AnimatedWidthButton_Copy_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
        }
    }
}
