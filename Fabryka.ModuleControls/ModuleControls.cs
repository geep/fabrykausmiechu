﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using Microsoft.Practices.Prism.Regions;


namespace Fabryka.ModuleControls
{
    [ModuleExport(typeof(ModuleControls), InitializationMode = InitializationMode.WhenAvailable)]
    public class ModuleControls : IModule
    {
        private readonly IRegionViewRegistry regionViewRegistry;

        [ImportingConstructor]
        public ModuleControls(IRegionViewRegistry registry)
        {
            regionViewRegistry = registry;
        }

        public void Initialize()
        {
            regionViewRegistry.RegisterViewWithRegion("MenuRegion", typeof(Menu.MenuView));
            regionViewRegistry.RegisterViewWithRegion("TeethRegion", typeof(TeethPicker.TeethPickerView));
            regionViewRegistry.RegisterViewWithRegion("SearchRegion", typeof(Search.SearchView));

        }        
    }
}
