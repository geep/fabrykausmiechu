﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.ViewModel;
using Hardcodet.Wpf.TaskbarNotification;
using System.Windows;
using Fabryka.SMSServer.MainWindow;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Prism.Commands;
using Fabryka.SMSServer.Services;

namespace Fabryka.SMSServer.Controlers
{
    public class AppControler : NotificationObject
    {
        
        private static AppControler current;
        
        public static AppControler Current
        {
            get
            {
                if (current == null)
                {
                    current = new AppControler();
                    current.Initialize();
                }
                return current;
            }
        }

        private ISMSDataService dataService;
        private ISMSService smsService;
        private TaskbarIcon _TaskbarIcon;

        public TaskbarIcon TaskbarIcon
        {
            get { return _TaskbarIcon; }
            set
            {
                if (_TaskbarIcon != null)
                {
                    _TaskbarIcon.Dispose();
                }
                _TaskbarIcon = value;
            }
        }

        public DelegateCommand TrayOpen { get; private set; }
        public DelegateCommand TrayQuit { get; private set; }

        private AppControler()
        {
        }

        private void Initialize()
        {
            TrayOpen = new DelegateCommand(
                () => ShowMainWindow()
                );
            TrayQuit = new DelegateCommand(
                () => CloseApplication(true)
                );

            dataService = ServiceLocator.Current.GetInstance<ISMSDataService>();
            smsService = ServiceLocator.Current.GetInstance<ISMSService>();
        }

        public void ShowMainWindow()
        {
            Application app = Application.Current;

            if (app.MainWindow == null)
            {
                app.MainWindow = ServiceLocator.Current.GetInstance<MainWindowView>();
                app.MainWindow.Show();
                app.MainWindow.Activate();
                app.MainWindow.Closing += OnMainWindowClosing;
            }
            else
            {
                app.MainWindow.Focus();
                app.MainWindow.Activate();
            }

            if (TaskbarIcon != null) TaskbarIcon.Visibility = Visibility.Collapsed;
        }

        private void OnMainWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ((Window)sender).Closing -= OnMainWindowClosing;

            Application.Current.MainWindow = null;

            CloseApplication(false);
        }

        public void CloseApplication(bool forceShutdown)
        {
            if (!forceShutdown)
            {
                MinimizeToTray();
            }
            else
            {
                StopServices();
                Application.Current.Shutdown();
            }
        }

        public void MinimizeToTray()
        {
            var mainWindow = Application.Current.MainWindow;

            if (mainWindow != null)
            {
                mainWindow.Closing -= OnMainWindowClosing;
                mainWindow.Close();
            }

            if (TaskbarIcon == null)
            {
                var tb = (TaskbarIcon)Application.Current.FindResource("TaskbarIcon");
                tb.DataContext = this;
                TaskbarIcon = tb;
            }

            TaskbarIcon.Visibility = Visibility.Visible;
        }

        public void StartServices()
        {
            dataService.StartService();
            smsService.StartService();
        }

        public void StopServices()
        {
            smsService.StopService();
            dataService.StopService();
        }

        public bool? ShowSettingsWindow()
        {
            var window = ServiceLocator.Current.GetInstance<Settings.SettingsView>();
            return window.ShowDialog();
        }
    }
}
