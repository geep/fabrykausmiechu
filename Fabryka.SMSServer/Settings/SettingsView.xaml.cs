﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel.Composition;

namespace Fabryka.SMSServer.Settings
{
    /// <summary>
    /// Interaction logic for SettingsView.xaml
    /// </summary>
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class SettingsView : Window
    {
        public SettingsView()
        {
            InitializeComponent();
        }

        [Import]
        public SettingsViewModel ViewModel
        {
            set
            {
                DataContext = value;
                value.DialogResult = ((res) => DialogResult = res);
            }
        }

    }
}
