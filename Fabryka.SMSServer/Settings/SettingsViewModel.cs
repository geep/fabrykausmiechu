﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.ViewModel;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.Commands;

namespace Fabryka.SMSServer.Settings
{
    [Export]
    public class SettingsViewModel : NotificationObject
    {
        private string _ReminderCommands;

        public Action<bool?> DialogResult { get; set; }

        public DelegateCommand SaveCommand { get; private set; }
        public DelegateCommand CancelCommand { get; private set; }

        public string ReminderCommands
        {
            get
            {
                if (_ReminderCommands == null)
                {
                    _ReminderCommands = String.Join<string>(",", Properties.Settings.Default.ReminderCommands.Cast<string>());
                }
                return _ReminderCommands;
            }
            set
            {
                if (_ReminderCommands == value)
                    return;
            	_ReminderCommands = value;
                Properties.Settings.Default.ReminderCommands = new System.Collections.Specialized.StringCollection();
                Properties.Settings.Default.ReminderCommands.AddRange(_ReminderCommands.Split(','));
            }
        }

        public SettingsViewModel()
        {
            SaveCommand = new DelegateCommand(
                () =>
                {
                    Properties.Settings.Default.Save();
                    DialogResult(true);
                }
            );
            CancelCommand = new DelegateCommand(
                () =>
                { 
                    Properties.Settings.Default.Reload(); 
                    DialogResult(false);
                });
        }
    }
}
