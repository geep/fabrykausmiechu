﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.ViewModel;
using Fabryka.SMSServer.Services;
using DevExpress.Xpo;
using Microsoft.Practices.Prism.Commands;
using Fabryka.Business;
using Fabryka.SMSServer.Controlers;

namespace Fabryka.SMSServer.MainWindow
{
    [Export]
    public class MainWindowViewModel : NotificationObject
    {
        private ISMSDataService dataService;

        public ISMSService SMSService { get; private set; }
        public XPCollection<SMSStatusLogEntry> StatusLog
        {
            get
            {
                return dataService.GetStatusLog();
            }
        }

        public XPCollection<SMSLogEntry> SMSLog
        {
            get
            {
                return dataService.GetSMSLog();
            }
        }

        public DelegateCommand<DateTime?> StartSending { get; private set; }
        public DelegateCommand StopSending { get; private set; }
        public DelegateCommand ShowSettings { get; private set; }

        [ImportingConstructor]
        public MainWindowViewModel(ISMSService _smsService, ISMSDataService _dataService)
        {
            SMSService = _smsService;
            dataService = _dataService;

            StartSending = new DelegateCommand<DateTime?>(
                (dt) => SMSService.SetStartingDate(dt.Value));
            StopSending = new DelegateCommand(
                () => SMSService.StopSending());
            ShowSettings = new DelegateCommand(
                () => AppControler.Current.ShowSettingsWindow());
        }

        
    }
}
