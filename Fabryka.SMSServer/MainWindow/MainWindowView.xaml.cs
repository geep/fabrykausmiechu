﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel.Composition;

namespace Fabryka.SMSServer.MainWindow
{
    /// <summary>
    /// Interaction logic for MainWindowView.xaml
    /// </summary>
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class MainWindowView : Window
    {
        public MainWindowView()
        {
            InitializeComponent();
            DevExpress.Xpf.Core.DXGridDataController.DisableThreadingProblemsDetection = true;
        }

        [Import]
        public MainWindowViewModel ViewModel
        {
            set
            {
            	DataContext = value;
            }
        }

        private void statusLogGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            statusTable.BestFitColumns();
            smsTable.BestFitColumns();            
        }

    }
}
