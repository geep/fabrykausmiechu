﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using Fabryka.Business;
using DevExpress.Data.Filtering;
using System.ComponentModel.Composition;

namespace Fabryka.SMSServer.Services
{
    [Export(typeof(ISMSDataService)), PartCreationPolicy(CreationPolicy.Shared) ]
    public class SMSDataService : ISMSDataService
    {
        private UnitOfWork _Session;
        private XPCollection<SMSStatusLogEntry> statusLog;
        private XPCollection<SMSLogEntry> smsLog;


        public void StartService()
        {
            XpoDefault.DataLayer = XpoDefault.GetDataLayer(Properties.Settings.Default.DBConnectionString,
                  DevExpress.Xpo.DB.AutoCreateOption.SchemaOnly);

            _Session = new UnitOfWork();
        }

        public void StopService()
        {
            _Session.Disconnect();
        }

        public XPCollection<Appointment> GetAppoinmentsForDate(DateTime date)
        {
            var nextDay = date.AddDays(1);
            CriteriaOperator criteria = 
                CriteriaOperator.Parse("(StartTime >= (?) AND StartTime < (?)) OR (EndTime >= (?) AND EndTime < (?))", date, nextDay);
            XPCollection<Appointment> apps = new XPCollection<Appointment>(_Session, criteria);

            return apps;
        }

        public SMSServiceStatus GetServiceStatus()
        {
            var query = _Session.Query<SMSServiceStatus>();

            var status = (from s in query
                            orderby s.CreatedOn descending 
                          select s).FirstOrDefault();

            return status;
        }

        public void SaveServiceStatus(bool serviceStarted, DateTime startOn)
        {
            SMSServiceStatus status = new SMSServiceStatus(_Session)
            {
                ServiceStarted = serviceStarted,
                StartTime = startOn
            };

            _Session.CommitChanges();  
        }


        public void LogStatus(SMSStatusLogType type, string message)
        {
            SMSStatusLogEntry entry = new SMSStatusLogEntry(_Session)
            {
                Type = type,
                Message = message.Substring(0, Math.Min(message.Length, 250))
            };

            statusLog.Add(entry);
            _Session.CommitChanges();
        }

        public void LogSMS(SMSLogType type, string cellphone, string message, Appointment notified = null)
        {
            SMSLogEntry sms = new SMSLogEntry(_Session)
            {
                Type = type,
                CellphoneNumber = cellphone,
                Message = message.Substring(0, Math.Min(message.Length, 250)),
                NotifiedAppointmentID = (notified == null) ? -1 : notified.Oid
            };

            smsLog.Add(sms);
            _Session.CommitChanges();
        }


        public XPCollection<SMSStatusLogEntry> GetStatusLog()
        {
            if (statusLog == null)
            {
                statusLog = new XPCollection<SMSStatusLogEntry>(_Session, null,
                    new SortProperty("CreatedOn", DevExpress.Xpo.DB.SortingDirection.Descending))
                    {
                        TopReturnedObjects = 100
                    };

            }

            return statusLog;
        }

        public XPCollection<SMSLogEntry> GetSMSLog()
        {
            if (smsLog == null)
            {
                smsLog = new XPCollection<SMSLogEntry>(_Session, null,
                    new SortProperty("CreatedOn", DevExpress.Xpo.DB.SortingDirection.Descending))
                {
                    TopReturnedObjects = 100
                };
            }
            return smsLog;
        }


        public Appointment GetNextAppointmentFor(string cellphone)
        {
            var modCell = "%" + cellphone.Substring(2);
            var query = new XPCollection<Appointment>(_Session,
                CriteriaOperator.Parse("StartTime >= (?) && ((IsNewPatient && (NewPatient.CellphoneNumber LIKE (?))) " 
                        + "|| (not (Patient is null) && Patient.ContactData.CellphoneNumber LIKE (?)))", DateTime.Now, 
                        modCell, modCell),
                new SortProperty("StartTime", DevExpress.Xpo.DB.SortingDirection.Ascending))
                {
                    TopReturnedObjects = 1
                };

            return query.FirstOrDefault();
        }

        public bool AlreadySent(Appointment appointment)
        {
            var query = new XPCollection<SMSLogEntry>(_Session,
                CriteriaOperator.Parse("NotifiedAppointmentID = (?)",appointment.Oid));

            return query.Count > 0;
        }
    }
}
