﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using Fabryka.Business;

namespace Fabryka.SMSServer.Services
{
    public interface ISMSDataService
    {
        void StartService();
        void StopService();

        XPCollection<Appointment> GetAppoinmentsForDate(DateTime date);
        SMSServiceStatus GetServiceStatus();
        void SaveServiceStatus(bool serviceStarted, DateTime startOn);

        void LogStatus(SMSStatusLogType type, string message);
        void LogSMS(SMSLogType type, string cellphone, string message, Appointment notified = null);

        XPCollection<SMSStatusLogEntry> GetStatusLog();
        XPCollection<SMSLogEntry> GetSMSLog();

        Appointment GetNextAppointmentFor(string cellphone);
        bool AlreadySent(Appointment appointment);
    }

}
