﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Microsoft.Practices.Prism.ViewModel;
using System.ComponentModel.Composition;
using System.Timers;
using DevExpress.Xpo;
using Fabryka.Business;
using Fabryka.SMSServer.Services.SMSApi;
using System.ServiceModel;
using Microsoft.Practices.ServiceLocation;
using System.Text.RegularExpressions;
using System.ServiceModel.Web;
using System.Web;
using System.ServiceModel.Activation;
using System.IO;
using System.Globalization;
using System.Threading.Tasks;

namespace Fabryka.SMSServer.Services
{
    [Export(typeof(ISMSService)), PartCreationPolicy(CreationPolicy.Shared)]
    [ServiceBehavior(ConcurrencyMode=ConcurrencyMode.Single, InstanceContextMode=InstanceContextMode.Single)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SMSService : NotificationObject, ISMSService, ISMSRecieveService
    {
        private class Texts
        {
            public const string StatusStopped = "Wysyłanie smsów zatrzymane.";
            public const string StatusStarted = "Wysyłanie smsów aktywne.";
            public const string StatusSending = "Wysyłanie powiadomień do pacjentów.";
            public const string StatusSent = "Wysłano powiadomienia do pacjentów.";
        }

        private enum EndReason
        {
            TooLate,
            NoAppointments,
            AppointmentsSent
        }
        
        private readonly object mutex = new object();
        private Timer mainServiceTimer;
        private bool sendNotifications;
        private DateTime nextNotificationDate;
        private XPCollection<Appointment> appointmentsToSend;
        private Appointment currentAppointment;

        private smsapiClient smsApiService;
        private Client smsClient;
        private ISMSDataService dataService;

        private string _CurrentStatus = Texts.StatusStopped;
        private WebServiceHost serviceHost;
        private byte[] okResponse;

        public string CurrentStatus
        {
            get
            {
                return _CurrentStatus;
            }
            private set
            {
                _CurrentStatus = value;
                RaisePropertyChanged(() => CurrentStatus);
            }
        }

        public double TimeInterval = 30 * 1000; // 30 sec
        public double MaximalDelay = 2 * 60; // 120 minutes;
        public double DaysInAdvance = 1;


        public void StartService()
        {
            lock (mutex)
            {
                dataService = ServiceLocator.Current.GetInstance<ISMSDataService>();

                InitializeSettings();
                InitializeRecieveService();

                if (mainServiceTimer == null)
                {
                    mainServiceTimer = new Timer(TimeInterval);
                    mainServiceTimer.Elapsed += ServiceTick;
                }

                if (!mainServiceTimer.Enabled)
                {
                    mainServiceTimer.Start();
                }
            }
        }



        private void InitializeSettings()
        {
            smsClient = new Client()
            {
                username = Properties.Settings.Default.SMSAPIUser,
                password = Properties.Settings.Default.SMSAPIPassword
            };

            CreateSmsApiService();

            var status = dataService.GetServiceStatus();

            if (status != null)
            {
                sendNotifications = status.ServiceStarted;
                nextNotificationDate = status.StartTime;
                CurrentStatus = (sendNotifications ? Texts.StatusStarted : Texts.StatusStopped);
            }
            else
            {
                sendNotifications = false;
                CurrentStatus = Texts.StatusStopped;
            }
        }

        private void InitializeRecieveService()
        {
            if (serviceHost == null)
            {
                serviceHost = new WebServiceHost(this, new Uri(Properties.Settings.Default.ServiceHost));
                okResponse = ASCIIEncoding.Default.GetBytes("OK");
            }

            serviceHost.Open();
        }

        private void CreateSmsApiService()
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.SendTimeout = TimeSpan.FromMinutes(1);
            binding.OpenTimeout = TimeSpan.FromMinutes(1);
            binding.CloseTimeout = TimeSpan.FromMinutes(1);
            binding.ReceiveTimeout = TimeSpan.FromMinutes(10);

            binding.AllowCookies = false;
            binding.BypassProxyOnLocal = false;
            binding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            binding.MessageEncoding = WSMessageEncoding.Text;
            binding.TextEncoding = System.Text.Encoding.UTF8;
            binding.TransferMode = TransferMode.Buffered;
            binding.UseDefaultWebProxy = true;
            binding.MaxBufferSize = 65536;
            binding.MaxReceivedMessageSize = 65536;
            binding.MaxBufferPoolSize = 524288;

            var rq = new System.Xml.XmlDictionaryReaderQuotas()
            {
                MaxDepth = 32,
                MaxStringContentLength = 8192,
                MaxArrayLength = 16384,
                MaxBytesPerRead = 4096,
                MaxNameTableCharCount = 16384
            };

            binding.ReaderQuotas = rq;


            var sec = new BasicHttpSecurity()
            {
                Transport = new HttpTransportSecurity()
                {
                    ClientCredentialType = HttpClientCredentialType.None,
                    ProxyCredentialType = HttpProxyCredentialType.None
                },
                Message = new BasicHttpMessageSecurity()
                {
                    ClientCredentialType = BasicHttpMessageCredentialType.UserName,
                },
                Mode = BasicHttpSecurityMode.Transport
            };

            binding.Security = sec;

            smsapiClient service = new smsapiClient(binding, new EndpointAddress(Properties.Settings.Default.SMSAPIEndpoint));
            smsApiService = service;
        }

        public void StopService()
        {
            lock (mutex)
            {
                mainServiceTimer.Stop();
                serviceHost.Close();
            }
        }

        public void SetStartingDate(DateTime start)
        {
            lock (mutex)
            {
                nextNotificationDate = start.Date.AddHours((double)Properties.Settings.Default.SendHour);
                sendNotifications = true;
                dataService.SaveServiceStatus(sendNotifications, nextNotificationDate);
                dataService.LogStatus(SMSStatusLogType.Important, String.Format("Następna godzina wysyłania: {0:dd-MM-yyyy HH:mm}.", nextNotificationDate));
                CurrentStatus = Texts.StatusStarted;
            }
        }

        public void StopSending()
        {
            lock (mutex)
            {
                sendNotifications = false;
                dataService.SaveServiceStatus(sendNotifications, nextNotificationDate);
                dataService.LogStatus(SMSStatusLogType.Stopped, "Zatrzymano wysyłanie smsów.");
                CurrentStatus = Texts.StatusStopped;
            }
        }

        void ServiceTick(object sender, ElapsedEventArgs e)
        {
            lock (mutex)
            {
                CheckNotificationStatus();
            }
        }

        private void CheckNotificationStatus()
        {
            if (!sendNotifications)
                return;

            var now = DateTime.Now;
            if (nextNotificationDate > now)
            {
                return;
            }

            if ((now - nextNotificationDate).TotalMinutes > MaximalDelay)
            {
                EndDailySending(EndReason.TooLate);
                return;
            }

            if (appointmentsToSend == null)
            {
                GetAppointmentsToSend();
            }

            if (appointmentsToSend.Count == 0)
            {
                EndDailySending(EndReason.NoAppointments);
                return;
            }

            SendOutNotifications();

            if (appointmentsToSend.Count == 0)
            {
                EndDailySending(EndReason.AppointmentsSent);
                return;
            }
        }

        private void EndDailySending(EndReason reason)
        {
            appointmentsToSend = null;

            string text = "";
            switch (reason)
            {
                case EndReason.AppointmentsSent:
                    text = String.Format("Zakończono wysyłanie powiadomień z dnia {0:dd-MM-yyyy}.", nextNotificationDate.Date);
                    break;
                case EndReason.NoAppointments:
                    text = String.Format("Brak powiadomień do wysłania z dnia {0:dd-MM-yyyy}.", nextNotificationDate.Date);
                    break;
                case EndReason.TooLate:
                    text = String.Format("Za późno na powiadomienia z dnia {0:dd-MM-yyyy}.", nextNotificationDate.Date);
                    break;
            }

            dataService.LogStatus(SMSStatusLogType.SendingDaily, text);

            SetStartingDate(nextNotificationDate.AddDays(1));


            CurrentStatus = Texts.StatusSent;
        }

        private void GetAppointmentsToSend()
        {
            appointmentsToSend = dataService.GetAppoinmentsForDate(nextNotificationDate.Date.AddDays(DaysInAdvance));
        }

        private bool? SendSMS(string cellphone, string message)
        {
            SMS sms = new SMS()
            {
                sender = Properties.Settings.Default.SMSSenderNumber,
                recipient = cellphone,
                eco = 0, // TODO: Dodaj to jako opcję?
                date_send = 0,
                details = true,
                message = message,
                //@params = new string[] { @"parametr 1", @"parametr 2", @"parametr 3", @"parametr 4" },
                idx = "0",
            };

            SendSms_Input sms_input = new SendSms_Input() 
            { 
                client = smsClient, 
                sms = sms 
            };

            SMSResult result = null;

            try
            {
                result = smsApiService.send_sms(sms_input);
            }
            catch (FaultException<SMSApi.Fault> fault)
            {
                dataService.LogStatus(SMSStatusLogType.Error,
                     String.Format("Błąd wysyłania smsa, {3}: {0} - {1}{2}",
                     cellphone, message.Substring(0, Math.Min(15, message.Length)), (message.Length <= 15) ? "" : "...",
                     fault.Message));

                var code = 0;
                int.TryParse(fault.Code.Name, out code);

                if (code == 13)
                {
                     return null;
                }
                else if (code == 202 || code == 202)
                {
                    return false;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                dataService.LogStatus(SMSStatusLogType.Error,
                    String.Format("Błąd wysyłania smsa: {0} - {1}{2}, {3}",
                    cellphone, message.Substring(0, Math.Min(15, message.Length)), (message.Length <= 15) ? "" : "...",
                    e));
                return false;
            }

            return true;
        }

        private void SendOutNotifications()
        {
            dataService.LogStatus(SMSStatusLogType.SendingDaily,
                String.Format("Wysyłanie powiadomień z dnia {0:dd-MM-yyyy}.", nextNotificationDate.Date));
            CurrentStatus = Texts.StatusSending;

            List<Appointment> apps = appointmentsToSend.ToList();

            foreach (var appointment in apps)
            {
                if (NotifyAboutAppointment(appointment))
                {
                    appointmentsToSend.Remove(appointment);
                }
            }
        }

        private bool CleanUpPhoneNumber(string oldNumber, out string newNumber)
        {
            newNumber = oldNumber;

            if (newNumber == null)
                return false;

            newNumber = newNumber.Replace("-", "");
            newNumber = newNumber.Replace("+", "");
            newNumber = newNumber.Replace("(", "");
            newNumber = newNumber.Replace(")", "");
            newNumber = newNumber.Replace(" ", "");

            if (newNumber.Length != 9 && newNumber.Length != 11)
                return false;

            if (!Regex.IsMatch(newNumber, @"(48)?[0-9]{9}"))
            {
                return false;
            }

            return true;
        }

        private bool NotifyAboutAppointment(Appointment appointment)
        {
            string cellphone = null;

            if (appointment.Resource.DontSendSMS)
                return true;

            if (appointment.IsNewPatient)
            {
                if (appointment.NewPatient == null)
                    return true;

                cellphone = appointment.NewPatient.CellphoneNumber;
            }
            else
            {
                if (appointment.Patient == null)
                    return true;

                if (appointment.Patient.ContactData == null || appointment.Patient.ContactData.DontSendSMS)
                    return true;

                cellphone = appointment.PatientCellphoneNumber;
            }

            if (string.IsNullOrWhiteSpace(cellphone))
                return true;

            var validCellphone = cellphone;
            if (!CleanUpPhoneNumber(cellphone,out validCellphone))
            {
                dataService.LogStatus(SMSStatusLogType.Error,
                    String.Format("Niewłaściwy numer telefonu: {0} {1} - {2} {3}, {4}", 
                    appointment.StartTime.ToString("dd.MM.yyyy"), appointment.StartTime.ToShortTimeString(),
                    appointment.PatientFirstName, appointment.PatientLastName, cellphone));
                return true;
            }

            if (dataService.AlreadySent(appointment))
            {
                return true;
            }

            bool? result = false;
            currentAppointment = appointment;

            result = SendSMS(validCellphone, String.Format(Properties.Settings.Default.MessageNotification,
                appointment.StartTime.ToString("dd.MM.yyyy"), appointment.StartTime.ToShortTimeString()));

            if (result.HasValue && result.Value)
            {
                dataService.LogSMS(SMSLogType.Sent, validCellphone,
                    String.Format("{0} {1} - {2} {3}", appointment.StartTime.ToString("dd.MM.yyyy"), appointment.StartTime.ToShortTimeString(),
                        appointment.PatientFirstName, appointment.PatientLastName), appointment);
            }

            currentAppointment = null;
            return result.HasValue ? result.Value : true;
        }

        public System.IO.Stream RecieveSMS(System.IO.Stream content)
        {
            string  query;
            using (StreamReader streamReader = new StreamReader(content))
            {
                query = streamReader.ReadToEnd();
            }

            var @params = HttpUtility.ParseQueryString("?" + query);
            string sms_from = @params["sms_from"];
            string sms_text = @params["sms_text"];

            lock (mutex)
            {
                dataService.LogSMS(SMSLogType.Recieved, sms_from, sms_text);
            }

            Task.Factory.StartNew(() =>
                            RespondToSMS(sms_from, sms_text));

            OutgoingWebResponseContext outContext = WebOperationContext.Current.OutgoingResponse;
            outContext.ContentType = "text/plain";
            return new System.IO.MemoryStream(okResponse);
        }

        private void RespondToSMS(string sms_from, string sms_text)
        {
            lock (mutex)
            {
                sms_text = sms_text.ToLowerInvariant();
                if (Properties.Settings.Default.ReminderCommands.Contains(sms_text))
                {
                    CheckReminder(sms_from);
                }
            }
        }

        private void CheckReminder(string cellphone)
        {
            Appointment app = dataService.GetNextAppointmentFor(cellphone);

            if (app == null)
            {
                var result = SendSMS(cellphone, Properties.Settings.Default.MessageNoVisit);
                if (result.HasValue && result.Value)
                    dataService.LogSMS(SMSLogType.Sent, cellphone,
                    "Brak wizyty dla numeru.");
            }
            else
            {
                var result = SendSMS(cellphone, String.Format(Properties.Settings.Default.MessageReminder,
                    app.StartTime.ToString("dd.MM.yyyy"), app.StartTime.ToShortTimeString()));
                if (result.HasValue && result.Value)
                    dataService.LogSMS(SMSLogType.Sent, cellphone,
                    String.Format("Przypomnienie: {0} - {1} {2}", app.StartTime,
                        app.PatientFirstName, app.PatientLastName));
            }
        }

        public System.IO.Stream DummyRecieveSMS()
        {
            OutgoingWebResponseContext context = WebOperationContext.Current.OutgoingResponse;
            context.ContentType = "text/plain";
            return new System.IO.MemoryStream(okResponse);
        }
    }
}
