﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Fabryka.SMSServer.Services
{
    public interface ISMSService : INotifyPropertyChanged
    {
        void StartService();
        void StopService();

        void SetStartingDate(DateTime start);
        void StopSending();

        string CurrentStatus { get; }
    }
}
