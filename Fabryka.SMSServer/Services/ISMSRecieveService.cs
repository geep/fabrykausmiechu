﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.IO;

namespace Fabryka.SMSServer.Services
{
    [ServiceContract(Name = "SMSApiRecieve")]
    public interface ISMSRecieveService
    {
        [OperationContract]
        [WebGet(UriTemplate = "/recieve", BodyStyle = WebMessageBodyStyle.Bare)]
        System.IO.Stream DummyRecieveSMS();

        [OperationContract]
        [WebInvoke(UriTemplate = "/recieve", BodyStyle = WebMessageBodyStyle.Bare)]
        System.IO.Stream RecieveSMS(Stream content);
    }
}
