﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;

namespace Fabryka.DisconnectHandling
{
    /// <summary>
    /// Interaction logic for DisconnectedView.xaml
    /// </summary>
    [Export, PartCreationPolicy(System.ComponentModel.Composition.CreationPolicy.NonShared)]
    public partial class DisconnectedView : DXWindow
    {
        public DisconnectedView()
        {
            InitializeComponent();
        }

        [Import]
        public DisconnectedViewModel ViewModel
        {
            set
            {
                DataContext = value;
            }
        }
        

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            Application.Current.Shutdown();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
