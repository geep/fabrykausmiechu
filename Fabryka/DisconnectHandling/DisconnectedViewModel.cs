﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using Fabryka.Business;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;

namespace Fabryka.DisconnectHandling
{
    [Export, PartCreationPolicy(System.ComponentModel.Composition.CreationPolicy.NonShared)]
    public class DisconnectedViewModel : NotificationObject
    {
        public IEnumerable<DatabaseType> DatabaseTypes
        {
            get
            {
                return Enum.GetValues(typeof(DatabaseType)).Cast<DatabaseType>();
            }
        }

        public IEnumerable<ApplicationMode> ApplicationModes
        {
            get
            {
                return Enum.GetValues(typeof(ApplicationMode)).Cast<ApplicationMode>();
            }
        }

        public bool IsServer
        {
            get
            {
                return Properties.Settings.Default.ApplicationMode == ApplicationMode.Server;
            }
        }

        public bool IsRemoteDatabase
        {
            get
            {
                return Properties.Settings.Default.DatabaseType != DatabaseType.Access;
            }
        }

        public DelegateCommand SaveCommand { get; private set; }
        public DelegateCommand CancelCommand { get; private set; }

        public DisconnectedViewModel()
        {
            SaveCommand = new DelegateCommand(() => Properties.Settings.Default.Save());
            CancelCommand = new DelegateCommand(() => Properties.Settings.Default.Reload());
        }

    }
}
