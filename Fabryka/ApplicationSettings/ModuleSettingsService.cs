﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fabryka.Common.Services;
using System.Xml.Serialization;
using System.IO;
using Microsoft.Practices.ServiceLocation;
using System.ComponentModel.Composition;
using Fabryka.Common;

namespace Fabryka.ApplicationSettings
{
    [Export(typeof(IModulesSettingsService)), Export(typeof(ModuleSettingsService))]
    public class ModuleSettingsService : IModulesSettingsService
    {
        public class SettingsSerialized
        {
            public Fabryka.Business.DatabaseType DatabaseType;
            public string DatabaseHost;
            public string DatabaseUser;
            public string DatabasePassword;
            public string DatabaseName;
        }

        public string SettingsName
        {
            get { return "Application"; }
        }

        public string GetSettingsAsXml()
        {
            XmlSerializer xs = new XmlSerializer(typeof(SettingsSerialized));
            StringWriter sw = new StringWriter();

            var ds = Fabryka.Properties.Settings.Default;

            var ss = new SettingsSerialized()
            {
                DatabaseHost = ds.DatabaseHost,
                DatabaseName = ds.DatabaseName,
                DatabasePassword = ds.DatabasePassword,
                DatabaseType = ds.DatabaseType,
                DatabaseUser = ds.DatabaseUser
            };

            xs.Serialize(sw, ss);

            return sw.ToString();
        }

        public void SetSettingsAsXml(string xml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(SettingsSerialized));
            StringReader sr = new StringReader(xml);

            SettingsSerialized ss = (SettingsSerialized) xs.Deserialize(sr);

            var ds = Fabryka.Properties.Settings.Default;


            if (!ApplicationState.Current.ServerMode && (ss.DatabaseHost == "localhost" || ss.DatabaseHost == "127.0.0.1"))
                ds.DatabaseHost = ds.ServerHost;
            else
                ds.DatabaseHost = ss.DatabaseHost;

            ds.DatabaseName = ss.DatabaseName;
            ds.DatabasePassword = ss.DatabasePassword;
            ds.DatabaseType = ss.DatabaseType;
            ds.DatabaseUser = ss.DatabaseUser;
            ds.Save();
        }

        public void NotifyChanged()
        {
            ServiceLocator.Current.GetInstance<IGlobalSettingsService>().SettingsChanged(this);
        }
    }


}
