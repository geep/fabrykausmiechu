﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Practices.Prism.Commands;
using Fabryka.Business;

namespace Fabryka.ApplicationSettings
{
    [Export]
    public class ApplicationSettingsViewModel : NotificationObject
    {
        public string ViewName { 
            get 
            {
                return "Główne ustawienia";
            }
        }

        public IEnumerable<DatabaseType> DatabaseTypes
        {
            get
            {
                return Enum.GetValues(typeof(DatabaseType)).Cast<DatabaseType>();
            }
        }

        public IEnumerable<ApplicationMode> ApplicationModes
        {
            get
            {
                return Enum.GetValues(typeof(ApplicationMode)).Cast<ApplicationMode>();
            }
        }

        public bool IsServer
        {
            get
            {
                return Properties.Settings.Default.ApplicationMode == ApplicationMode.Server;
            }
        }

        public bool IsRemoteDatabase
        {
            get
            {
                return Properties.Settings.Default.DatabaseType != DatabaseType.Access;
            }
        }

        public DelegateCommand SaveCommand { get; private set; }
        public DelegateCommand CancelCommand { get; private set; }
        public ApplicationSettingsViewModel()
        {
            SaveCommand = new DelegateCommand(() => Properties.Settings.Default.Save());
            CancelCommand = new DelegateCommand(() => Properties.Settings.Default.Reload());
        }
    }
}
