﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Prism.MefExtensions;
using Microsoft.Practices.Prism.Modularity;
using System.ComponentModel.Composition.Hosting;
using Microsoft.Practices.Prism.Logging;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Prism.Regions;
using DevExpress.Xpf.Core;
using Fabryka.Common.Services;
using Fabryka.Common;
using DevExpress.Xpo.DB;
using Microsoft.Practices.Prism.Events;
using Fabryka.Common.Events;
using System.Windows;

namespace Fabryka
{
    class FabrykaBootstrapper : MefBootstrapper
    {
        private System.Windows.Window DisconnectScreen { get; set; }
        private bool servicesStarted = false;

        protected override System.Windows.DependencyObject CreateShell()
        {
            return new Shell();
        }

        public bool HandleReconnecting()
        {
            servicesStarted = false;

            this.DisconnectScreen = ServiceLocator.Current.GetInstance<DisconnectHandling.DisconnectedView>();
            this.DisconnectScreen.Owner = (Window) this.Shell;
            if (this.DisconnectScreen.ShowDialog() == false)
            {
                return false;
            }

            while (!servicesStarted)
            {
                try
                {
                    StartServices();
                }
                catch
                {
                    this.DisconnectScreen = ServiceLocator.Current.GetInstance<DisconnectHandling.DisconnectedView>();
                    this.DisconnectScreen.Owner = (Window)this.Shell;
                    if (this.DisconnectScreen.ShowDialog() == false)
                    {
                        return false;
                    }
                }

            }

            return true;
        }
        protected override void InitializeShell()
        {
            var regionViewRegistry = ServiceLocator.Current.GetInstance<IRegionViewRegistry>();
            base.InitializeShell();

            IEventAggregator eventAggregator = ServiceLocator.Current.GetInstance<IEventAggregator>();
            DisconnectEvent ev = eventAggregator.GetEvent<DisconnectEvent>();
            ev.Subscribe((args) =>
            {
                args.Connected = HandleReconnecting;
                args.WaitHandle.Set();
            }, ThreadOption.BackgroundThread);

            while (!servicesStarted)
            {
                try
                {
                    StartServices();
                }
                catch
                {
                    if (DevExpress.Xpf.Core.DXSplashScreen.IsActive)
                        DevExpress.Xpf.Core.DXSplashScreen.Close();
                    this.DisconnectScreen = ServiceLocator.Current.GetInstance<DisconnectHandling.DisconnectedView>();
                    if (this.DisconnectScreen.ShowDialog() == false)
                        return;
                    DXSplashScreen.Show<SplashScreenWindow>();
                }

            }

            App.Current.MainWindow = (Shell) this.Shell;

            var regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();
            
            regionViewRegistry.RegisterViewWithRegion("SettingsRegion", typeof(ApplicationSettings.ApplicationSettingsView));
            
            this.Shell.Dispatcher.BeginInvoke((Action)
                (() =>
                {
                    ((Shell)(this.Shell)).WindowState = System.Windows.WindowState.Minimized;
                    ((Shell)(this.Shell)).Show();
                    regionManager.RequestNavigate("SubMenuRegion", "PatientSubMenuView");
                    regionManager.RequestNavigate("MainRegion", "PatientDataView", EndOfNavigation);
                }));
            
        }

        private void EndOfNavigation(NavigationResult obj)
        {
            ((Shell)(this.Shell)).WindowState = System.Windows.WindowState.Maximized;
            if (DevExpress.Xpf.Core.DXSplashScreen.IsActive)
                DevExpress.Xpf.Core.DXSplashScreen.Close();
        }

        private void StartServices()
        {
            Fabryka.Common.ApplicationState.Current.ServerMode =
                Fabryka.Properties.Settings.Default.ApplicationMode == Business.ApplicationMode.Server;

            INetworkService networkService = ServiceLocator.Current.GetInstance<INetworkService>();
            networkService.HandleReconnect = () => HandleReconnecting();
            networkService.Start(Properties.Settings.Default.ServerHost, Properties.Settings.Default.ServerPort);

            IGlobalSettingsService settingsService = ServiceLocator.Current.GetInstance<IGlobalSettingsService>();
            settingsService.StartService(ApplicationState.Current.ServerMode);

            string connectionString;
            if (Fabryka.Properties.Settings.Default.DatabaseType == Business.DatabaseType.Firebird)
            {
                connectionString = FirebirdConnectionProvider.GetConnectionString(
                    Fabryka.Properties.Settings.Default.DatabaseHost,
                    Fabryka.Properties.Settings.Default.DatabaseUser,
                    Fabryka.Properties.Settings.Default.DatabasePassword,
                    Fabryka.Properties.Settings.Default.DatabaseName);
            }
            else if (Fabryka.Properties.Settings.Default.DatabaseType == Business.DatabaseType.Access)
            {
                connectionString = AccessConnectionProvider
                    .GetConnectionString(Fabryka.Properties.Settings.Default.DatabaseName);
            }
            else
            {
                throw new IndexOutOfRangeException();
            }

            IDataService dataService = ServiceLocator.Current.GetInstance<IDataService>();
            dataService.Start(connectionString);
            servicesStarted = true;
        }

        protected override IModuleCatalog CreateModuleCatalog()
        {
            return new ConfigurationModuleCatalog();
        }

        protected override void ConfigureAggregateCatalog()
        {
            base.ConfigureAggregateCatalog();

            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(FabrykaBootstrapper).Assembly));
            DirectoryCatalog catalog = new DirectoryCatalog("DirectoryModules");
            this.AggregateCatalog.Catalogs.Add(catalog); 
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
        }

        private CustomNLogger _logger = new CustomNLogger();

        public ILoggerFacade FLogger
        {
            get
            {
                return _logger;
            }
        }

        protected override ILoggerFacade CreateLogger()
        {
            return FLogger;
        }

        protected override RegionAdapterMappings ConfigureRegionAdapterMappings()
        {
            RegionAdapterMappings m = base.ConfigureRegionAdapterMappings();
            m.RegisterMapping(typeof(DXTabControl), Container.GetExportedValue<DXTabControlRegionAdapter>());
            return m;
        }
    }

}


