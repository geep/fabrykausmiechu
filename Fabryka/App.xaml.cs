﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using Microsoft.Practices.Prism.Logging;
using DevExpress.Xpo.DB;
using DevExpress.Xpf.Core;
using Microsoft.Practices.ServiceLocation;
using Fabryka.Common.Services;
using System.Threading;

namespace Fabryka
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static Mutex appMutex;
        private static bool quickShutdown;

        protected override void OnStartup(StartupEventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            DispatcherUnhandledException += App_DispatcherUnhandledException;

            Fabryka.Common.ApplicationState.Current.ServerMode =
                Fabryka.Properties.Settings.Default.ApplicationMode == Business.ApplicationMode.Server;

            bool createdNew = true;

            if (Fabryka.Common.ApplicationState.Current.ServerMode)
            {
                appMutex = new Mutex(true, "FabrykaAppMutex", out createdNew);
            }

            if (!createdNew)
            {
                quickShutdown = true;
                MessageBox.Show("Ten program jest już uruchomiony. Może istnieć tylko jedna instancja serwera.", "Fabryka - problem", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                Application.Current.Shutdown();
                return;
            }

            ThemeManager.ApplicationThemeName = "MetropolisLight";
            DXSplashScreen.Show<SplashScreenWindow>();
            var bootstrapper = new FabrykaBootstrapper();
            var logger = bootstrapper.FLogger;

            base.OnStartup(e);

            logger.Log("==========================================", Category.Info, Priority.None);
            logger.Log("App starting", Category.Info, Priority.None);

            DispatcherUnhandledException +=
                ((sender, args) => 
                    logger.Log(String.Format("Unhandled exception: {0}", args.Exception),
                        Category.Exception, Priority.High));

            AppDomain.CurrentDomain.UnhandledException +=
                ((sender, args) =>
                    logger.Log(String.Format("Unhandled exception: {0}", (args.ExceptionObject as Exception)),
                        Category.Exception, Priority.High));

            if (String.IsNullOrEmpty(Fabryka.Properties.Settings.Default.DatabaseName))
            {
                Fabryka.Properties.Settings.Default.DatabaseName =
                    String.Format("{0}\\Fabryka.fdb", Environment.GetFolderPath(Environment.SpecialFolder.Personal));
                //Fabryka.Properties.Settings.Default.Save();
            }

            Fabryka.Business.BussinesReg.Reg();



            bootstrapper.Run();
        }


        protected override void OnExit(ExitEventArgs e)
        {
            if (!quickShutdown)
            {
                INetworkService netService = ServiceLocator.Current.GetInstance<INetworkService>();
                IDataService dataService = ServiceLocator.Current.GetInstance<IDataService>();

                try
                {

                    if (dataService != null)
                        dataService.Stop();

                    if (netService != null)
                        netService.Stop();
                }
                catch { }; // TODO: Obsłuż. 
            }

            base.OnExit(e);
        }
        void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
#if !DEBUG
            if (DevExpress.Xpf.Core.DXSplashScreen.IsActive)
                DevExpress.Xpf.Core.DXSplashScreen.Close();
            string errorMessage = string.Format("Wystąpił nieoczekiwany błąd:\n{0}", e.Exception.Message);
            MessageBox.Show(errorMessage, "Poważny błąd programu", MessageBoxButton.OK, MessageBoxImage.Error);

            e.Handled = false;
#endif
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
#if !DEBUG
            if (DevExpress.Xpf.Core.DXSplashScreen.IsActive)
                DevExpress.Xpf.Core.DXSplashScreen.Close();
            string errorMessage = string.Format("Wystąpił nieoczekiwany błąd: {0}", (e.ExceptionObject as Exception).Message);
            MessageBox.Show(errorMessage, "Poważny błąd domeny programu", MessageBoxButton.OK, MessageBoxImage.Error);

#endif
        }
    }
}
