﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Fabryka.Common.Services;
using System.ComponentModel.Composition;
using System.ComponentModel;
using System.Windows.Markup;
using System.Threading;
using DevExpress.Xpf.Core;

namespace Fabryka
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [Export]
    public partial class Shell : DXWindow, INotifyPropertyChanged
    {
        public Shell()
        {
            InitializeComponent();
            this.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);
            this.Width = SystemParameters.WorkArea.Width;
            this.Height = SystemParameters.WorkArea.Height;
            this.Loaded += Shell_Loaded;
        }

        void Shell_Loaded(object sender, RoutedEventArgs e)
        {
            this.Activate();
        }


        // Fields...

        private ILoadingService _LoadingService;
        [Import]
        public ILoadingService LoadingService
        {
            get
            {
                return _LoadingService;
            }
            set
            {
                _LoadingService = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("LoadingService"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void DXWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.IsActive == true && MessageBox.Show("Czy na pewno chcesz zakończyć pracę programu?", "Fabryka", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
            {
                e.Cancel = true;
            }
        }
    }
}
