using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Prism.Logging;
using NLog;

namespace Fabryka
{
    public class CustomNLogger : ILoggerFacade
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public void Log(string message, Category category, Priority priority)
        {
            switch (category)
            {
                case Category.Debug:
                    _logger.Debug(message);
                    break;
                case Category.Exception:
                    _logger.Error(message);
                    break;
                case Category.Info:
                    _logger.Info(message);
                    break;
                case Category.Warn:
                    _logger.Warn(message);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
