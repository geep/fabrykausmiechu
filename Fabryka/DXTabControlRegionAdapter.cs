﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Regions;
using DevExpress.Xpf.Core;
using Microsoft.Practices.Prism.Regions.Behaviors;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.Specialized;
using System.Collections;
using System.Windows;
using System.ComponentModel.Composition;

namespace Fabryka
{
    [Export]
    public class DXTabControlRegionAdapter : RegionAdapterBase<DXTabControl> {
        [ImportingConstructor]
        public DXTabControlRegionAdapter(IRegionBehaviorFactory regionBehaviorFactory)
            : base(regionBehaviorFactory) {
        }
        protected override void Adapt(IRegion region, DXTabControl regionTarget) {
        }
        protected override void AttachBehaviors(IRegion region, DXTabControl regionTarget) {
            DXTabControlSyncBehavior behavior = new DXTabControlSyncBehavior(regionTarget);
            region.Behaviors.Add(DXTabControlSyncBehavior.BehaviorKey, behavior);
            base.AttachBehaviors(region, regionTarget);
        }
        protected override IRegion CreateRegion() {
            return new Region();
        }
    }
    public class DXTabControlSyncBehavior : RegionBehavior, IHostAwareRegionBehavior, IRegionBehavior
    {
        public static readonly string BehaviorKey = "DXTabControlSyncBehavior";
        public DXTabControl HostControl { get; private set; }
        public DXTabControlSyncBehavior(DXTabControl hostControl)
        {
            HostControl = hostControl;
        }

        protected override void OnAttach()
        {
            if ((HostControl.ItemsSource != null) || (BindingOperations.GetBinding(HostControl, ItemsControl.ItemsSourceProperty) != null)) return;
            this.SynchronizeItems();
            HostControl.SelectionChanged += HostControlSelectionChanged;
            base.Region.ActiveViews.CollectionChanged += ActiveViewsCollectionChanged;
            base.Region.Views.CollectionChanged += ViewsCollectionChanged;
        }
        void HostControlSelectionChanged(object sender, TabControlSelectionChangedEventArgs e)
        {
            try
            {
                this.updatingActiveViewsInHostControlSelectionChanged = true;
                if (e.OldSelectedItem != null && base.Region.Views.Contains(e.OldSelectedItem) && base.Region.ActiveViews.Contains(e.OldSelectedItem))
                    base.Region.Deactivate(e.OldSelectedItem);
                if (e.NewSelectedItem != null && !(!base.Region.Views.Contains(e.NewSelectedItem) || base.Region.ActiveViews.Contains(e.NewSelectedItem)))
                    base.Region.Activate(e.NewSelectedItem);
            }
            finally
            {
                this.updatingActiveViewsInHostControlSelectionChanged = false;
            }
        }
        void ActiveViewsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!this.updatingActiveViewsInHostControlSelectionChanged)
            {
                if (e.Action == NotifyCollectionChangedAction.Add)
                {
                    if (((HostControl.SelectedItem != null) && (HostControl.SelectedItem != e.NewItems[0])) && base.Region.ActiveViews.Contains(HostControl.SelectedItem))
                    {
                        base.Region.Deactivate(HostControl.SelectedItem);
                    }
                    HostControl.SelectedItem = e.NewItems[0];
                }
                else if ((e.Action == NotifyCollectionChangedAction.Remove) && e.OldItems.Contains(HostControl.SelectedItem))
                {
                    HostControl.SelectedItem = null;
                }
            }
        }
        void ViewsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                int startIndex = e.NewStartingIndex;
                foreach (object newItem in e.NewItems) HostControl.Items.Insert(startIndex++, newItem);
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (object oldItem in e.OldItems) HostControl.Items.Remove(oldItem);
            }
        }
        void SynchronizeItems()
        {
            List<object> existingItems = new List<object>();
            foreach (object childItem in (IEnumerable)HostControl.Items) existingItems.Add(childItem);
            foreach (object view in base.Region.Views) HostControl.Items.Add(view);
            foreach (object existingItem in existingItems) base.Region.Add(existingItem);
        }

        bool updatingActiveViewsInHostControlSelectionChanged;

        DependencyObject IHostAwareRegionBehavior.HostControl
        {
            get { return HostControl; }
            set { HostControl = value as DXTabControl; }
        }
    }
}
