﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Fabryka.Common.Services;
using Microsoft.Practices.Prism.ViewModel;

namespace Fabryka.ModuleServices
{
    [Export(typeof(ILoadingService))]
    public class LoadingService : NotificationObject, ILoadingService
    {
        private bool _IsLoading;

        public bool IsLoading
        {
            get
            {
                return _IsLoading;
            }
            set
            {
                if (_IsLoading == value)
                    return;

                _IsLoading = value;

                RaisePropertyChanged(() => IsLoading);
            }
        }
    }
}
