﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fabryka.Common.Services;
using Fabryka.Common;
using Fabryka.ModuleServices.Network;
using System.ServiceModel;
using System.ComponentModel.Composition;
using System.ServiceModel.Description;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Prism.Logging;
using System.Net.Sockets;
using Microsoft.Practices.Prism.Events;
using Fabryka.Common.Events;
using Fabryka.Business;

namespace Fabryka.ModuleServices
{
    [Export(typeof(INetworkService))]
    public class NetworkService : INetworkService
    {
        private string endpoint;
        private DuplexChannelFactory<IServer> factory;
        private NetTcpBinding clientBinding;
        private IServer serverProxy;

        static ILoggerFacade logger;

        private ServiceHost serverHost;

        ISelectedPatientService selectedPatientService;

        [Import]
        private IEventAggregator eventAggregator;

        public void PostChangedAppointments(HashSet<Appointment> changedAppointments)
        {
            CleanServerCall(() => serverProxy.PostingChangedAppointments(changedAppointments.Select((ob) => ob.Oid).ToList()));
        }
        public void PostChangedPatients(HashSet<Patient> changedPatients)
        {
            CleanServerCall(() => serverProxy.PostingChangedPatients(changedPatients.Select((ob) => ob.Oid).ToList()));
        }
        public void PostMassChanges(HashSet<UpdateType> updates)
        {
            CleanServerCall(() => serverProxy.PostingMassChanges(updates));
        }
        public void Start(string hostname, int port)
        {
            if (logger == null)
                logger = ServiceLocator.Current.GetInstance<ILoggerFacade>();

            if (ApplicationState.Current.ServerMode)
            {
                hostname = "localhost";
                StartServer(hostname, port);
            }

            StartClient(hostname, port);
        }

        public void Stop()
        {
            if (!ApplicationState.Current.ServerMode)
            {
                StopClient();
            }

            if (ApplicationState.Current.ServerMode)
            {
                StopServer();
            }
        }

        private void StartClient(string hostname, int port)
        {
            
            selectedPatientService = ServiceLocator.Current.GetInstance<ISelectedPatientService>();
            selectedPatientService.SelectionChanged += selectedPatientService_SelectionChanged;

            endpoint = String.Format(@"net.tcp://{0}:{1}", hostname, port);

            var newNetTcpBinding = new NetTcpBinding();
            newNetTcpBinding.Security.Mode = SecurityMode.None;
            newNetTcpBinding.ReliableSession.Enabled = true;
            newNetTcpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromSeconds(10);
            newNetTcpBinding.SendTimeout = TimeSpan.MaxValue;
            newNetTcpBinding.ReceiveTimeout = TimeSpan.MaxValue;
            newNetTcpBinding.OpenTimeout = TimeSpan.FromSeconds(20);
            newNetTcpBinding.CloseTimeout = TimeSpan.FromSeconds(20);

            clientBinding = newNetTcpBinding;

            ConnectToServer();
        }

        private bool faulted = false;

        private void ConnectToServer()
        {
            IClient callback = new ClientWCFService();
            factory = new DuplexChannelFactory<IServer>(callback, clientBinding, endpoint);
            serverProxy = factory.CreateChannel();

            //((ICommunicationObject)serverProxy).Faulted += NetworkService_Faulted;
            (serverProxy as ICommunicationObject).Open();
            CleanServerCall(() => serverProxy.Connect());


            if (selectedPatientService.SelectedPatient != null)
                NotifyPatientChanged();

            //StartTicker();
        }

        private void NotifyPatientChanged()
        {
            var result = -2;

            if (selectedPatientService.SelectedPatient != null)
            {
                result = selectedPatientService.SelectedPatient.Oid;
            }


            CleanServerCall(() => serverProxy.ChangingSelectedPatient(result));
         }

        void selectedPatientService_SelectionChanged(object sender, EventArgs e)
        {
            NotifyPatientChanged();
        }

        private void StopClient()
        {
            try
            {
                serverProxy.Disconnect();
            }
            catch
            {
            }
            factory.Close();
        }

        private void StartServer(string hostname, int port)
        {
            serverHost = new ServiceHost(typeof(ServerWCFService));
            var ep = String.Format(@"net.tcp://{0}:{1}", hostname, port);

            var newNetTcpBinding = new NetTcpBinding();
            newNetTcpBinding.Security.Mode = SecurityMode.None;
            newNetTcpBinding.SendTimeout = TimeSpan.MaxValue;
            newNetTcpBinding.ReliableSession.Enabled = true;
            newNetTcpBinding.ReliableSession.InactivityTimeout = TimeSpan.FromSeconds(10);
            newNetTcpBinding.ReceiveTimeout = TimeSpan.MaxValue;

            serverHost.AddServiceEndpoint(typeof(IServer), newNetTcpBinding, ep);
            serverHost.Open();
        }

        private void StopServer()
        {
            serverHost.Close();
        }

        public void PostChanges(UpdateType type)
        {
            CleanServerCall(() => serverProxy.PostingChanges(type));
        }

        public void CheckStatus()
        {
            CleanServerCall(() => serverProxy.CheckStatus());
        }

        public void PostSettingChanged(string settingsName, string settings)
        {
            CleanServerCall(() => serverProxy.PostSettingsChanged(settingsName, settings));
        }

        public Dictionary<string, string> GetSettings()
        {
            return CleanServerCall(() => serverProxy.GetSettings());
        }

        public void CleanServerCall(Action action)
        {
            bool handled = false;
            while (!(handled))
            {
                try
                {
                    action();
                    handled = true;
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        public TResult CleanServerCall<TResult>(Func<TResult> action)
        {
            while (true)
            {
                try
                {
                    var result = action();
                    return result;
                }
                catch (CommunicationException ex)
                {
                    HandleException(ex);
                }
                catch (TimeoutException ex)
                {
                    HandleException(ex);
                }
                catch 
                {
                    throw;
                }
            }
        }

        public Func<bool> HandleReconnect { get; set; }

        private void HandleException(Exception ex)
        {
            logger.Log(String.Format("Disconnection exception: {0}", ex),
                Category.Warn, Priority.High);

            if (HandleReconnect() != true)
                throw new ApplicationException();
        }
    }
}
