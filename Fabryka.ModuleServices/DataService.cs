﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using Fabryka.Business;
using Fabryka.Common.Services;
using System.ComponentModel.Composition;
using System.IO;
using System.Windows.Media.Imaging;
using DevExpress.Data.Filtering;
using Microsoft.Practices.ServiceLocation;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using Fabryka.Common.Events;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Logging;

namespace Fabryka.ModuleServices
{
    [Export(typeof(IDataService)), PartCreationPolicy(CreationPolicy.Shared)]
    public class DataService : IDataService
    {
        private XPCollection<Appointment> _Appointments;
        private XPCollection<AppointmentResource> _AppointmentResources;
        private XPCollection<AppointmentType> _AppointmentTypes;
        private XPCollection<Doctor> _DoctorsSecondary;
        private XPCollection<NFZProcedure> _NFZProcedures;
        private UnitOfWork _Session;
        private UnitOfWork _SecondarySession;

        private ISelectedPatientService selectedPatientService;
        private INetworkService networkService;
        private ILoggerFacade logger;

        private HashSet<UpdateType> updates = new HashSet<UpdateType>();

        private HashSet<Appointment> changedAppointments = new HashSet<Appointment>();
        private HashSet<Patient> changedPatients = new HashSet<Patient>();

        [Import]
        private IEventAggregator eventAggregator;

        public DataService()
        {
            
        }

        private Dictionary<RTFTemplateType, XPCollection<RTFTemplateNode>> typedTemplates = new Dictionary<RTFTemplateType,XPCollection<RTFTemplateNode>>();
        private Dictionary<RTFTemplateType, XPCollection<RTFTemplateNode>> typedTemplateRoots = new Dictionary<RTFTemplateType, XPCollection<RTFTemplateNode>>();

        public int GetPhotoCount(Patient patient)
        {
            XPQuery<Photo> photos = _Session.Query<Photo>();

            var list = from p in photos where p.Patient == patient
                       select p;

            return list.ToList().Where(p => !p.IsDeleted).Count();
        }
        public XPCollection<RTFTemplateNode> GetTypedTemplateRoots(RTFTemplateType rtfTemplateType)
        {
            if (typedTemplateRoots.ContainsKey(rtfTemplateType))
                return typedTemplateRoots[rtfTemplateType];

            var result = new XPCollection<RTFTemplateNode>(_Session,
                CriteriaOperator.Parse("Parent is null && TemplateType = ?", rtfTemplateType));
            GetTypedTemplates(rtfTemplateType).ListChanged += (s, e) => result.Reload();

            typedTemplateRoots[rtfTemplateType] = result;

            return result;
        }
        public XPCollection<RTFTemplateNode> GetTypedTemplates(RTFTemplateType selectedType)
        {
            if (typedTemplates.ContainsKey(selectedType))
                return typedTemplates[selectedType];

            var result = new XPCollection<RTFTemplateNode>(_Session, CriteriaOperator.Parse("TemplateType = ?", selectedType));

            typedTemplates[selectedType] = result;

            return result;
        }

        public XPCollection<RTFTemplateNode> GetVisitTemplates()
        {
            return GetTypedTemplates(RTFTemplateType.Visits);
        }


        public void Start(string connectionString)
        {
            XpoDefault.DataLayer = XpoDefault.GetDataLayer(connectionString,
                Fabryka.Common.ApplicationState.Current.ServerMode ? DevExpress.Xpo.DB.AutoCreateOption.DatabaseAndSchema : DevExpress.Xpo.DB.AutoCreateOption.SchemaAlreadyExists);
            if (logger == null)
                logger = ServiceLocator.Current.GetInstance<ILoggerFacade>();
        }

        public void Stop()
        {
            CommitChanges();
            XpoDefault.DataLayer.Connection.Close();
        }

        private void CheckSession()
        {
            if (_Session == null)
            {
                _Session = new UnitOfWork();
                _Session.ObjectChanged += Session_ObjectChanged;
                _SecondarySession = new UnitOfWork();
                _SecondarySession.ObjectChanged += Session_ObjectChanged;
            }
        }


        HashSet<UpdateType> localUpdates = new HashSet<UpdateType>();

        void Session_ObjectChanged(object sender, ObjectChangeEventArgs e)
        {
            if (inCommit)
                return;

            localUpdates.Clear();
            if (e.Object is IUpdateTypeObject)
            {
                localUpdates.Add((e.Object as IUpdateTypeObject).UpdateType);
                if (e.PropertyName != null && e.Object is IPropertyUpdateTypeObject)
                {
                    var type = (e.Object as IPropertyUpdateTypeObject).PropertyUpdateType(e.PropertyName);
                    localUpdates.Add(type);

                }
            }
            else
                localUpdates.Add(UpdateType.Patient);

            
            if (localUpdates.Contains(UpdateType.NewPatient))
            {
                changedPatients.Add(e.Object as Patient);
            }
            else
            if (localUpdates.Contains(UpdateType.Appointments))
            {
                changedAppointments.Add(e.Object as Appointment);
            }

            updates.UnionWith(localUpdates);

            if (transactionCount == 0)
                CommitChanges();

        }

        private XPCollection<Patient> _Patients;

        private XPCollection<Doctor> _Doctors;


        public BitmapImage CreateThumbnail(BitmapImage bmImage, MemoryStream mem)
        {
            BitmapImage bi = new BitmapImage();

            int height = bmImage.PixelHeight;
            int width = bmImage.PixelWidth;
            double ratio = width / (double)height;

            if (height > width)
            {
                height = Definitions.THUMBNAIL_SIZE;
                width = (int)(height * ratio);
            }
            else
            {
                width = Definitions.THUMBNAIL_SIZE;
                height = (int)(width / ratio);
            }

            width = Math.Min(Math.Max(width, 1), Definitions.THUMBNAIL_SIZE);
            height = Math.Min(Math.Max(height, 1), Definitions.THUMBNAIL_SIZE);


            bi.BeginInit();
            bi.CacheOption = BitmapCacheOption.OnLoad;
            bi.DecodePixelWidth = width;

            bi.DecodePixelHeight = height;

            bi.StreamSource = mem;

            bi.EndInit();

            return bi;
        }

        public Photo CreatePhotoFromFile(Patient patient, string filename)
        {
            CheckSession();

            BeginTransaction();

            try
            {

                byte[] buffer = File.ReadAllBytes(filename);
                BitmapImage bmImage = new BitmapImage();
                using (MemoryStream mem = new MemoryStream(buffer))
                {
                    mem.Seek(0, SeekOrigin.Begin);
                    bmImage.BeginInit();
                    bmImage.CacheOption = BitmapCacheOption.OnLoad;
                    bmImage.UriSource = new Uri(filename);
                    //bmImage.StreamSource = mem;
                    bmImage.EndInit();
                 //   bmImage.Freeze();
                }

                if (bmImage.PixelWidth == 1 && bmImage.PixelHeight == 1)
                {
                    throw (new System.OutOfMemoryException("Za mało pamięci na dodanie zdjęcia!"));
                }

                BitmapImage bi;

                using(MemoryStream mem = new MemoryStream(buffer))
                {
                    mem.Seek(0, SeekOrigin.Begin);
                    bi = CreateThumbnail(bmImage, mem);
                }

                Photo newPhoto = new Photo(_Session);
                newPhoto.Patient = patient;
                newPhoto.Name = System.IO.Path.GetFileName(filename);

                newPhoto.Picture = bmImage;
                newPhoto.Thumbnail = bi;
                patient.Photos.Add(newPhoto);

                return newPhoto;
            }
            finally
            {
                EndTransaction(UpdateType.Photos);
            }
        }

        public PhotoComparison CreatePhotoComparison(Patient selectedPatient)
        {
            BeginTransaction();

            try
            {
                PhotoComparison newComparison = new PhotoComparison(_Session)
                {
                    Patient = selectedPatient
                };

                selectedPatient.PhotoComparisons.Add(newComparison);

                return newComparison;
            }
            finally
            {
                EndTransaction(UpdateType.PhotoComparisons);
            }
        }

        public Visit CreateVisit(Patient patient)
        {
            BeginTransaction();

            try
            {
                Visit newVisit = new Visit(_Session) { Patient = patient, Time = DateTime.Now };
                patient.Visits.Add(newVisit);
                return newVisit;
            }
            finally
            {
                EndTransaction(UpdateType.Visits);
            }

        }

        public void RemoveVisit(Visit selectedVisit)
        {
            BeginTransaction();

            try
            {
                selectedVisit.Delete();
            }
            finally
            {
                EndTransaction(UpdateType.Visits);
            }
        }

        public void CreateActivePhase(TreatmentPlan plan)
        {
            BeginTransaction();

            try
            {
                TreatmentPhase tp = new TreatmentPhase(_Session) { PhaseName = "Faza" };
                plan.TreatmentPhases.Add(tp);
            }
            finally
            {
                EndTransaction(UpdateType.TreatmentPlan);
            }
        }

        public void RemoveActivePhase(TreatmentPhase treatmentPhase)
        {
            BeginTransaction();

            try
            {
                treatmentPhase.Plan.TreatmentPhases.Remove(treatmentPhase);
                treatmentPhase.Delete();
            }
            finally
            {
                EndTransaction(UpdateType.TreatmentPlan);
            }
        }

        public void RemovePhoto(Photo selectedPhoto)
        {
            BeginTransaction();

            try
            {
                foreach (var comp in selectedPhoto.Patient.PhotoComparisons)
                {
                    if (comp.FirstPhoto == selectedPhoto)
                        comp.FirstPhoto = null;
                    if (comp.SecondPhoto == selectedPhoto)
                        comp.SecondPhoto = null;
                }
                selectedPhoto.Patient.Photos.Remove(selectedPhoto);
                selectedPhoto.Delete();
            }
            finally
            {
                EndTransaction(UpdateType.Photos);
            }
        }
        public XPCollection<Appointment> GetAppointments()
        {
            CheckSession();
            if (_Appointments == null)
            {
                _Appointments = new XPCollection<Appointment>(_SecondarySession);
                _Appointments.Criteria =
                                    DevExpress.Data.Filtering.CriteriaOperator
                                        .Parse("(StartTime >= (?) AND StartTime < (?)) OR (EndTime >= (?) AND EndTime < (?))",
                                            DateTime.Today, DateTime.Today, DateTime.Today, DateTime.Today);
            }

            return _Appointments;
        }

        public XPCollection<NFZProcedure> GetNFZProcedures()
        {
            CheckSession();
            if (_NFZProcedures == null)
                _NFZProcedures = new XPCollection<NFZProcedure>(_Session);
            return _NFZProcedures;
        }

        public NFZVisit GetNFZVisit(Visit selectedVisit)
        {
            if (selectedVisit == null || selectedVisit.Patient == null || !selectedVisit.Patient.FabrykaData.IsNFZPatient)
                return null;

            var ret = _Session.FindObject<NFZVisit>(CriteriaOperator.Parse("[ParentVisit] = ?", selectedVisit));

            if (ret == null)
            {
                BeginTransaction();
                ret = new NFZVisit(_Session)
                {
                    ParentVisit = selectedVisit,
                    TotalPoints = 0
                };
                EndTransaction();
            }

            return ret;
        }

        public IEnumerable<NFZCompletedProcedure> GetRangedNFZVisits(DateTime @from, DateTime to)
        {
            XPQuery<NFZCompletedProcedure> nfzvisits = _Session.Query<NFZCompletedProcedure>();

            var res = from visit in nfzvisits
                      where visit.ParentVisit.ParentVisit.Time >= @from && visit.ParentVisit.ParentVisit.Time <= to
                      select visit;

            return res;
        }

        public XPCollection<AppointmentResource> GetAppointmentResources()
        {
            CheckSession();
            if (_AppointmentResources == null)
                _AppointmentResources = new XPCollection<AppointmentResource>(_SecondarySession);
            return _AppointmentResources;
        }

        public XPCollection<AppointmentType> GetAppointmentTypes()
        {
            CheckSession();
            if (_AppointmentTypes == null)
                _AppointmentTypes = new XPCollection<AppointmentType>(_SecondarySession, null,
                    new SortProperty("Position", DevExpress.Xpo.DB.SortingDirection.Ascending));
            return _AppointmentTypes;
        }
        public Session GetCurrentSession()
        {
            CheckSession();
            return _Session;
        }
        public AppointmentType GetDefaultAppointmentType(bool allDay)
        {
            return AppointmentType.GetDefault(_SecondarySession, allDay);
        }
        public XPCollection<Patient> GetPatients()
        {
            CheckSession();
            if (_Patients == null)
                _Patients = new XPCollection<Patient>(_SecondarySession);
            return _Patients;
        }

        public Patient GetPatientByOid(int oid, bool secondarySession = false)
        {
            var session = secondarySession ? _SecondarySession : _Session;

            return session.GetObjectByKey<Patient>(oid);
        }

        public XPCollection<Doctor> GetDoctors(bool secondarySession = false)
        {
            CheckSession();

            if (!secondarySession)
            {
                if (_Doctors == null)
                    _Doctors = new XPCollection<Doctor>(_Session);
                return _Doctors;
            }

            if (_DoctorsSecondary == null)
                _DoctorsSecondary = new XPCollection<Doctor>(_SecondarySession);
            return _DoctorsSecondary;
        }

        public void SaveChanges()
        {
            if (_Session != null)
                _Session.CommitChanges();
        }

        public Patient CreatePatient()
        {
            if (_Patients == null)
                GetPatients();

            BeginTransaction();

            try
            {

                //var result = _Session.Evaluate<Patient>(CriteriaOperator.Parse("Max(FabrykaData.PatientNumber)"),
                //    CriteriaOperator.Parse("FabrykaData.MonthNumber = ? and FabrykaData.YearNumber = ?",
                //    DateTime.Now.Month, DateTime.Now.Year));

                Patient newPatient = new Patient(_Session) { RegistrationDate = DateTime.Now };

                //newPatient.FabrykaData.PatientNumber = (result == null ? 1 : ((int)result) + 1);
                //newPatient.FabrykaData.MonthNumber = DateTime.Now.Month;
                //newPatient.FabrykaData.YearNumber = DateTime.Now.Year;
                return newPatient;
            }
            finally
            {
                EndTransaction(UpdateType.NewPatient);
            }

        }

        public void RemovePatient(Patient removedPatient)
        {
            if (_Patients == null)
                GetPatients();

            BeginTransaction();

            try
            {
                _Patients.Remove(removedPatient);
                removedPatient.Delete();
            }
            finally
            {
                EndTransaction(UpdateType.PatientDelete);
            }
        }

        public void RemovePhotoComparison(PhotoComparison pc)
        {
            BeginTransaction();
            try
            {
                pc.Patient.PhotoComparisons.Remove(pc);
                pc.Delete();
            }
            finally
            {
                EndTransaction(UpdateType.PhotoComparisons);
            }
        }
        
        // TODO: Apps, Resources, AppTypes (AllDay?, sort by position), GetTypeById

        private int transactionCount = 0;

        public void BeginTransaction()
        {
            transactionCount++;
        }

        public void EndTransaction(UpdateType type = UpdateType.None)
        {
            if (type != UpdateType.None)
                updates.Add(type);
            if (transactionCount == 1)
                CommitChanges();
            transactionCount--;
        }

        public void EndTransactionAndIgnore()
        {
            transactionCount--;

            if (inCommit)
                return;

            changedAppointments.Clear();
            changedPatients.Clear();
            updates.Clear();
        }

        bool inCommit = false;

        private void CommitChanges()
        {
            if (inCommit)
                return;

            if (networkService == null)
                networkService = ServiceLocator.Current.GetInstance<INetworkService>();

            inCommit = true;
            try
            {
                //if (_Session)
                if (_Session.GetObjectsToSave().Count > 0 || _Session.GetObjectsToDelete().Count > 0
                    || _SecondarySession.GetObjectsToSave().Count > 0 || _SecondarySession.GetObjectsToDelete().Count > 0)
                {
                    try
                    {
                        _Session.CommitChanges();
                        _SecondarySession.CommitChanges();
                    }
                    catch (DevExpress.Xpo.DB.Exceptions.LockingException e)
                    {
                        logger.Log("Locking exception " + e.ToString(), Category.Exception, Priority.High);
                        _Session.ReloadChangedObjects();
                        _SecondarySession.ReloadChangedObjects();
                        return;
                    }
                    catch (Exception e)
                    {
                        logger.Log("Database exception " + e.ToString(), Category.Exception, Priority.High);
                        return;
                    }

                    networkService.PostMassChanges(updates);
                    updates.Clear();

                    if (changedAppointments.Count > 0)
                    {
                        networkService.PostChangedAppointments(changedAppointments);
                        changedAppointments.Clear();
                    }

                    if (changedPatients.Count > 0)
                    {
                        networkService.PostChangedPatients(changedPatients);
                        UpdateChangedPatients(changedPatients);
                        changedPatients.Clear();
                    }

                }
            }
            catch (ApplicationException)
            {
                return;
            }
            finally
            {
                inCommit = false;
            }
        }

        public void UpdateChangedPatients(IEnumerable<int> oids)
        {
            if (_Patients == null)
                return;

            BeginTransaction();

            try
            {
                foreach (int oid in oids)
                {
                    var patient = _Patients.Lookup(oid);
                    if (patient != null)
                    {
                        patient.Reload();
                    }
                    else
                    {
                        patient = GetPatientByOid(oid, true);
                        _Patients.Add(patient);
                    }
                }
            }
            finally
            {
                EndTransactionAndIgnore();
            }
        }

        public void UpdateChangedAppointments(IEnumerable<int> oids)
        {
            if (!updateAppointmenst)
                return;

            BeginTransaction();

            try
            {
                foreach (int oid in oids)
                {
                    var appointment = _Appointments.Lookup(oid);
                    if (appointment != null)
                    {
                        appointment.Reload();
                    }
                    else
                    {
                        appointment = _SecondarySession.GetObjectByKey<Appointment>(oid);
                        _Appointments.Add(appointment);
                    }
                }
            }
            finally
            {
                EndTransactionAndIgnore();
            }
        }

        private void UpdateChangedPatients(HashSet<Patient> changedPatients)
        {
            UpdateChangedPatients(changedPatients.Select((pat) => pat.Oid));
        }

        private bool updateAppointmenst = false;

        public void StartAppointmentUpdates()
        {
            updateAppointmenst = true;
           // if (_Appointments.IsLoaded)
           //     UpdateAppointments();
        }

        public void StopAppointmentUpdates()
        {
            //updateAppointmenst = false;
        }


        public void UpdateAppointments()
        {
            if (!updateAppointmenst)
                return;

            BeginTransaction();

            try
            {
                if (_Appointments != null)
                    _Appointments.Reload();
            }
            finally
            {
                EndTransactionAndIgnore();
            }
        }

        public void UpdatePhotoComparisons()
        {
            if (selectedPatientService == null)
                selectedPatientService = ServiceLocator.Current.GetInstance<ISelectedPatientService>();

            BeginTransaction();

            try
            {
                if (selectedPatientService.SelectedPatient != null)
                    selectedPatientService.SelectedPatient.PhotoComparisons.Reload();
            }
            finally
            {
                EndTransactionAndIgnore();
            }
        }
        public void UpdatePhotos()
        {
            if (selectedPatientService == null)
                selectedPatientService = ServiceLocator.Current.GetInstance<ISelectedPatientService>();

            BeginTransaction();

            try
            {
                if (selectedPatientService.SelectedPatient != null)
                    selectedPatientService.SelectedPatient.Photos.Reload();
            }
            finally
            {
                EndTransactionAndIgnore();
            }
        }
        public void UpdateSelectedPatient()
        {
            if (selectedPatientService == null)
                selectedPatientService = ServiceLocator.Current.GetInstance<ISelectedPatientService>();

            BeginTransaction();

            try
            {
                if (selectedPatientService.SelectedPatient != null)
                    selectedPatientService.SelectedPatient.Reload();
            }
            finally
            {
                EndTransactionAndIgnore();
            }
        }

        public void UpdateTreatmentPlans()
        {
            if (selectedPatientService == null)
                selectedPatientService = ServiceLocator.Current.GetInstance<ISelectedPatientService>();

            BeginTransaction();

            try
            {
                if (selectedPatientService.SelectedPatient != null)
                {
                    selectedPatientService.SelectedPatient.Treatment.Plan.Reload();
                    selectedPatientService.SelectedPatient.Treatment.Plan.TreatmentPhases.Reload();
                }
            }
            finally
            {
                EndTransactionAndIgnore();
            }
        }
        public void UpdateVisits()
        {
            if (selectedPatientService == null)
                selectedPatientService = ServiceLocator.Current.GetInstance<ISelectedPatientService>();

            IsWpfGuiThread();

            BeginTransaction();

            try
            {
                if (selectedPatientService.SelectedPatient != null)
                    selectedPatientService.SelectedPatient.Visits.Reload();
            }
            finally
            {
                EndTransactionAndIgnore();
            }
        }

        public void UpdateAllPatients()
        {
            //BeginTransaction();

            try
            {
                if (_Patients != null)
                    _Patients.Reload();
            }
            finally
            {
                //EndTransactionAndIgnore();
            }
        }


        public static bool IsWpfGuiThread()
        {
            if (System.Windows.Threading.Dispatcher.FromThread(Thread.CurrentThread) != null)
                return true;

            return false;
        }
    }
}
