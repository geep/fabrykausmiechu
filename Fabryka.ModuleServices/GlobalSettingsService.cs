﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fabryka.Common.Services;
using System.ComponentModel.Composition;

namespace Fabryka.ModuleServices
{
    [Export(typeof(IGlobalSettingsService))]      
    public class GlobalSettingsService : IGlobalSettingsService
    {
        [ImportMany(typeof(IModulesSettingsService))]
        private IEnumerable<IModulesSettingsService> moduleSettings;

        [Import]
        private INetworkService networkService;

        private bool serverMode;

        private Dictionary<string, string> settings;
        private Dictionary<string, IModulesSettingsService> settingsModules;

        public void StartService(bool _serverMode)
        {
            serverMode = _serverMode;

            InitSettings();

            if (serverMode)
                return;

            Dictionary<string, string> sett = networkService.GetSettings();
            foreach (var kv in sett)
            {
                RecieveSettingsChanges(kv.Key, kv.Value);
            }
        }

        public void SettingsChanged(IModulesSettingsService moduleSettings)
        {
            if (!serverMode)
                return;

            string sett = moduleSettings.GetSettingsAsXml();
            settings[moduleSettings.SettingsName] = sett;

            networkService.PostSettingChanged(moduleSettings.SettingsName, sett);
        }

        public void RecieveSettingsChanges(string settingsName, string _settings)
        {
            if (!settings.ContainsKey(settingsName))
                return;

            settings[settingsName] = _settings;
            settingsModules[settingsName].SetSettingsAsXml(_settings);                        
        }

        private void InitSettings()
        {
            if (settings == null)
            {
                settings = new Dictionary<string, string>();
                settingsModules = new Dictionary<string, IModulesSettingsService>();

                foreach (var module in moduleSettings)
                {
                    settings.Add(module.SettingsName, module.GetSettingsAsXml());
                    settingsModules.Add(module.SettingsName, module);
                }
            }
        }
        public Dictionary<string, string> GetSettings()
        {
            InitSettings();

            return settings;
        }
    }
}
