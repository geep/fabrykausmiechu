﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Fabryka.Common.Services;
using Fabryka.Business;
using DevExpress.Xpo;

namespace Fabryka.ModuleServices
{
    [Export(typeof(ISelectedPatientService))]
    public class SelectedPatientService : ISelectedPatientService
    {
        private Patient _SelectedPatient;

        public const int STACK_SIZE = 5;

        private readonly LinkedList<Patient> BackwardStack = new LinkedList<Patient>();
        private readonly LinkedList<Patient> ForewardStack = new LinkedList<Patient>();

        [Import]
        private IDataService dataService;
                                         
        public Patient SelectedPatient
        {
            get
            {
                return _SelectedPatient;
            }
            set
            {
                Patient oldPatient = null;

                if (SelectedPatient == null || value == null)
                {
                    if (SelectedPatient == value)
                        return;
                }
                else
                {
                    if (_SelectedPatient.Oid == value.Oid)
                        return;
                }

                if (_SelectedPatient != null)
                {
                    if (_SelectedPatient.Photos.IsLoaded)
                    {
                        oldPatient = _SelectedPatient;
                    }
                    BackwardStack.AddFirst(_SelectedPatient);
                    if (BackwardStack.Count > STACK_SIZE)
                        BackwardStack.RemoveLast();

                }

                ForewardStack.Clear();

                _SelectedPatient = value;
                dataService.UpdateSelectedPatient();

                if (SelectionChanged != null)
                    SelectionChanged(this, EventArgs.Empty);

                if (oldPatient != null)
                {
                    oldPatient.Photos.Reload();
                    oldPatient.PhotoComparisons.Reload();
                }
            }
        }

        public bool CanGoBack { get { return BackwardStack.Count > 0; } }
        public bool CanGoForeward { get { return ForewardStack.Count > 0; } }

        public event EventHandler SelectionChanged;


        public void GoBack()
        {
            if (!CanGoBack)
                return;

            if (SelectedPatient != null)
            {
                ForewardStack.AddFirst(SelectedPatient);
                if (ForewardStack.Count > STACK_SIZE)
                    ForewardStack.RemoveLast();
            }

            _SelectedPatient = BackwardStack.First.Value;
            BackwardStack.RemoveFirst();

            if (SelectionChanged != null)
                SelectionChanged(this, EventArgs.Empty);

        }

        public void GoForeward()
        {
            if (!CanGoForeward)
                return;

            if (SelectedPatient != null)
            {
                BackwardStack.AddFirst(SelectedPatient);
                if (BackwardStack.Count > STACK_SIZE)
                    BackwardStack.RemoveLast();
            }

            _SelectedPatient = ForewardStack.First.Value;
            ForewardStack.RemoveFirst();

            if (SelectionChanged != null)
                SelectionChanged(this, EventArgs.Empty);
        }

        public void CreatePatient()
        {
            Patient p;
            dataService.BeginTransaction();
            try
            {
                p = dataService.CreatePatient();
            }
            finally
            {
                dataService.EndTransaction();
            }

            SelectedPatient = p; 
        }

        public void RemovePatient()
        {
            if (SelectedPatient == null)
                return;

            dataService.BeginTransaction();
            try
            {

                Patient removedPatient = SelectedPatient;
                while (CanGoBack && SelectedPatient == removedPatient)
                    GoBack();
            
                if (SelectedPatient == removedPatient)
                    SelectedPatient = null;

                while(ForewardStack.Remove(removedPatient));
                while(BackwardStack.Remove(removedPatient));

                dataService.RemovePatient(removedPatient);

                if (SelectionChanged != null)
                    SelectionChanged(this, EventArgs.Empty);

            }
            finally
            {
                dataService.EndTransaction();
            }

            dataService.UpdateAllPatients();
        }
    }
}
