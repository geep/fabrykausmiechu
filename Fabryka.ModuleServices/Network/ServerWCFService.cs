﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Fabryka.Business;
using Fabryka.Common.Services;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Prism.Logging;
using System.ComponentModel.Composition;
using System.Threading.Tasks;
using System.ServiceModel.Channels;

namespace Fabryka.ModuleServices.Network
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext=false)]
    public class ServerWCFService : IServer
    {

        static ILoggerFacade logger;

        class ClientState
        {
            public IClient callback;
            public int clientId;
            public int selectedPatientId;
        }

        private static Object syncObj = new Object();
        private static int currentClientId = 1;
        private static Dictionary<int, ClientState> clients = new Dictionary<int, ClientState>();

        private static IGlobalSettingsService settingsService;

        private int clientId;
        private ClientState state;


        public void Connect()
        {
            lock (syncObj)
            {
                if (logger == null)
                    logger = ServiceLocator.Current.GetInstance<ILoggerFacade>();

                clientId = currentClientId;
                currentClientId++;


                
                state = new ClientState()
                {
                    callback = OperationContext.Current.GetCallbackChannel<IClient>(),
                    selectedPatientId = -1,
                    clientId = clientId
                };

                OperationContext context = OperationContext.Current;
                MessageProperties messageProperties = context.IncomingMessageProperties;
                RemoteEndpointMessageProperty endpointProperty =
                                messageProperties[RemoteEndpointMessageProperty.Name]
                                as RemoteEndpointMessageProperty;

                var address = endpointProperty.Address;

                logger.Log(String.Format("Client connected: {0} - {1}", clientId, address),
                    Category.Info, Priority.Low);

                clients.Add(clientId, state);

                OperationContext.Current.Channel.Faulted += Channel_Faulted;
                OperationContext.Current.Channel.Closed += Channel_Closed;
            }
        }

        void Channel_Closed(object sender, EventArgs e)
        {
            Disconnect();
        }

        void Channel_Faulted(object sender, EventArgs e)
        {
            logger.Log(String.Format("Channel faulted: {0}", clientId),
                Category.Warn, Priority.Medium);
            Disconnect();
        }

        public void Disconnect()
        {
            lock (syncObj)
            {
                logger.Log(String.Format("Client disconnected: {0}", clientId),
                    Category.Info, Priority.Low);
                if (clients.ContainsKey(clientId))
                    clients.Remove(clientId);
            }
        }

        public void ChangingSelectedPatient(int id)
        {
            lock (syncObj)
            {
                state.selectedPatientId = id;
            }
            //return true;
        }

        public void PostingChangedAppointments(List<int> oids)
        {
            logger.Log(String.Format("Posting changed appointment from {0}", clientId),
                Category.Debug, Priority.Low);
            lock (syncObj)
            {
                foreach (var s in clients.Values)
                {
                    if (s != state)
                    {
                        var currentState = s;
                        Task.Factory.StartNew(() =>
                            ReportAppointmentsChanged(oids, currentState));
                    }
                }
            }
        }

        private void ReportAppointmentsChanged(List<int> oids, ClientState s)
        {
            try
            {
                s.callback.RecieveAppointmentsChanged(oids);
            }
            catch (TimeoutException)
            {
                logger.Log(String.Format("Failed in posting changes to {0}", s.clientId),
                    Category.Warn, Priority.Low);
            }
            catch (Exception e)
            {
                logger.Log(String.Format("Unhandled exception in posting to {0} - {1})", s.clientId, e), Category.Exception, Priority.High);
                throw;
            }
        }

        public void PostingChangedPatients(List<int> oids)
        {
            logger.Log(String.Format("Posting changed potients from {0}", clientId),
                Category.Debug, Priority.Low);
            lock (syncObj)
            {
                foreach (var s in clients.Values)
                {
                    if (s != state)
                    {
                        var currentState = s;
                        Task.Factory.StartNew(() =>
                            ReportPatientsChanged(oids, currentState));
                    }
                }
            }
        }

        private void ReportPatientsChanged(List<int> oids, ClientState s)
        {
            try
            {
                s.callback.RecievePatientsChanged(oids);
            }
            catch (TimeoutException)
            {
                logger.Log(String.Format("Failed in posting changes to {0}", s.clientId),
                    Category.Warn, Priority.Low);
            }
            catch (Exception e)
            {
                logger.Log(String.Format("Unhandled exception in posting to {0} - {1})", s.clientId, e), Category.Exception, Priority.High);
                throw;
            }
        }

        public void PostingChanges(UpdateType type)
        {
            logger.Log(String.Format("Posting changes from {0} - {1}", clientId, type),
                Category.Debug, Priority.Low);
            lock(syncObj)
            {
                foreach (var s in clients.Values)
                {
                    var clientState = s;
                    if (clientState != state && (type != UpdateType.Patient || clientState.selectedPatientId == state.selectedPatientId))
                    {
                        Task.Factory.StartNew(() =>
                            ReportChanges(type, state.selectedPatientId, clientState));
                    }
                }
            }
        }

        public void PostingMassChanges(HashSet<UpdateType> updates)
        {
            logger.Log(String.Format("Posting changes from {0}", clientId),
                Category.Debug, Priority.Low);
            lock (syncObj)
            {
                foreach (var s in clients.Values)
                {
                    var clientState = s;
                    if (clientState != state && (updates.Contains(UpdateType.Appointments) || clientState.selectedPatientId == state.selectedPatientId))
                    {
                        Task.Factory.StartNew(() =>
                            ReportMassChanges(updates, state.selectedPatientId, clientState));
                    }
                }
            }
        }

        private void ReportMassChanges(HashSet<UpdateType> updates, int selectedPatientId, ServerWCFService.ClientState s)
        {
            try
            {
                s.callback.RecieveMassChanges(updates, selectedPatientId);
            }
            catch (TimeoutException)
            {
                logger.Log(String.Format("Failed in posting changes to {0}", s.clientId),
                    Category.Warn, Priority.Low);
            }
            catch (Exception e)
            {
                logger.Log(String.Format("Unhandled exception in posting to {0} - {1})", s.clientId, e), Category.Exception, Priority.High);
                throw;
            }
        }

        private static void ReportChanges(UpdateType type, int selectedPatientId, ClientState s)
        {
            try
            {
                s.callback.RecieveChanges(type, selectedPatientId);
            }
            catch (TimeoutException)
            {
                logger.Log(String.Format("Failed in posting changes to {0} - {1}", s.clientId, type),
                    Category.Warn, Priority.Low);
            }
            catch (Exception e)
            {
                logger.Log(String.Format("Unhandled exception in posting to {0} - {1})", s.clientId, e), Category.Exception, Priority.High);
                throw;
            }
        }


        public void PostSettingsChanged(string settingsName, string settings)
        {
            logger.Log(String.Format("Posting settings changes from {0} - {1}", clientId, settings),
                Category.Debug, Priority.Low);
            lock (syncObj)
            {
                foreach (var s in clients.Values)
                {
                    if (s != state)
                    {
                        var currentState = s;
                        Task.Factory.StartNew(() =>
                            ReportSettingsChanged(settingsName, settings, currentState));
                    }
                }
            }
        }

        private void ReportSettingsChanged(string settingsName, string settings, ClientState s)
        {
            try
            {
                s.callback.RecieveSettingsChanges(settingsName, settings);
            }
            catch (TimeoutException)
            {
                logger.Log(String.Format("Failed in posting sett changes to {0} - {1}", clients.First((p) => p.Value == s).Key, settings),
                    Category.Warn, Priority.Low);
            }
            catch (Exception e)
            {
                logger.Log(String.Format("Unhandled exception in posting to {0} - {1})", s.clientId, e), Category.Exception, Priority.High);
                throw;
            }
        }
        public Dictionary<string, string> GetSettings()
        {
            if (settingsService == null)
                settingsService = ServiceLocator.Current.GetInstance<IGlobalSettingsService>();

            return settingsService.GetSettings();
        }

        public void CheckStatus()
        {
            return;
        }
    }
}
