﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Fabryka.Business;

namespace Fabryka.ModuleServices.Network
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract=typeof(IClient))]
    interface IServer
    {
        [OperationContract(IsOneWay=true, IsInitiating=true, IsTerminating=false)]
        void Connect();

        [OperationContract(IsOneWay=true, IsInitiating=false, IsTerminating=true)]
        void Disconnect();

        [OperationContract(IsOneWay = true, IsInitiating = false, IsTerminating = false)]
        void ChangingSelectedPatient(int id);

        [OperationContract(IsOneWay = true, IsInitiating = false, IsTerminating = false)]
        void PostingChanges(UpdateType type);

        [OperationContract(IsOneWay = true, IsInitiating = false, IsTerminating = false)]
        void PostingMassChanges(HashSet<UpdateType> updates);

        [OperationContract(IsOneWay = true, IsInitiating = false, IsTerminating = false)]
        void PostSettingsChanged(string settingsName, string settings);

        [OperationContract(IsOneWay = false, IsInitiating = false, IsTerminating = false)]
        Dictionary<string, string> GetSettings();

        [OperationContract(IsOneWay = true, IsInitiating = false, IsTerminating = false)]
        void CheckStatus();

        [OperationContract(IsOneWay = true, IsInitiating = false, IsTerminating = false)]
        void PostingChangedPatients(List<int> oids);

        [OperationContract(IsOneWay = true, IsInitiating = false, IsTerminating = false)]
        void PostingChangedAppointments(List<int> oids);
    }
}
