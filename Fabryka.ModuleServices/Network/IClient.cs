﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Fabryka.Business;

namespace Fabryka.ModuleServices.Network
{
    interface IClient
    {
        [OperationContract(IsOneWay=true)]
        void RecieveChanges(UpdateType type, int selectedPatientId);

        [OperationContract(IsOneWay = true)]
        void RecieveMassChanges(HashSet<UpdateType> updates, int selectedPatientId);

        [OperationContract(IsOneWay=true)]
        void RecieveSettingsChanges(string settingsName, string settings);

        [OperationContract(IsOneWay = true)]
        void RecieveAppointmentsChanged(List<int> oids);

        [OperationContract(IsOneWay = true)]
        void RecievePatientsChanged(List<int> oids);
    }
}
