﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fabryka.Common.Services;
using Microsoft.Practices.ServiceLocation;
using System.ServiceModel;
using Fabryka.Business;
using Microsoft.Practices.Prism.Logging;

namespace Fabryka.ModuleServices.Network
{
    [CallbackBehavior(UseSynchronizationContext = true)]
    public class ClientWCFService : IClient
    {
        private IDataService dataService;
        static ILoggerFacade logger;
        private ISelectedPatientService selectedService;

        public ClientWCFService()
        {
        }

        private void InitServices()
        {
            if (dataService == null)
            {
                dataService = ServiceLocator.Current.GetInstance<IDataService>();
                logger = ServiceLocator.Current.GetInstance<ILoggerFacade>();
                selectedService = ServiceLocator.Current.GetInstance<ISelectedPatientService>();
            }
        }
        public void RecieveChanges(UpdateType type, int selectedPatientId)
        {
            InitServices();

            logger.Log(String.Format("Update recieved: {0} - {1}", type, OperationContext.Current.Channel.RemoteAddress),
                Category.Info, Priority.Low);

            var patientId = selectedService.SelectedPatient == null ? -2 : selectedService.SelectedPatient.Oid;

            if (type != UpdateType.Appointments && type != UpdateType.NewPatient && selectedPatientId != patientId)
                return;

            switch (type)
            {
                case UpdateType.Appointments:
                    //dataService.UpdateAppointments();
                    break;
                case UpdateType.Patient:
                    dataService.UpdateSelectedPatient();
                    break;
                case UpdateType.Visits:
                    dataService.UpdateVisits();
                    break;
                case UpdateType.Photos:
                    dataService.UpdatePhotos();
                    break;
                case UpdateType.PhotoComparisons:
                    dataService.UpdatePhotoComparisons();
                    break;
                case UpdateType.TreatmentPlan:
                    dataService.UpdateTreatmentPlans();
                    break;
                case UpdateType.NewPatient:
                default:
                    //dataService.UpdateAllPatients();
                    break;
            }
        }

        public void RecieveMassChanges(HashSet<UpdateType> updates, int selectedPatientId)
        {
            foreach (var update in updates)
            {
                RecieveChanges(update, selectedPatientId);
            }
        }
        public void RecieveSettingsChanges(string settingsName, string settings)
        {
            var service = ServiceLocator.Current.GetInstance<IGlobalSettingsService>();
            service.RecieveSettingsChanges(settingsName, settings);
        }


        public void RecieveAppointmentsChanged(List<int> oids)
        {
            InitServices();
            try
            {
                dataService.UpdateChangedAppointments(oids);
            }
            catch (Exception e)
            {
                logger.Log(String.Format("Unhandled exception in getting appointments {0}", e), Category.Exception, Priority.High);
                throw;                
            }
        }

        public void RecievePatientsChanged(List<int> oids)
        {
            InitServices();
            try
            {
                dataService.UpdateChangedPatients(oids);
            }
            catch (Exception e)
            {
                logger.Log(String.Format("Unhandled exception in getting appointments {0}", e), Category.Exception, Priority.High);
                throw;         
            }

        }
    }
}
