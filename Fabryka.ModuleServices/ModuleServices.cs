﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using Microsoft.Practices.Prism.Modularity;

namespace Fabryka.ModuleServices
{
    [ModuleExport(typeof(ModuleServices), InitializationMode = InitializationMode.WhenAvailable)]
    public class ModuleServices : IModule
    {
        public void Initialize()
        {

        }
    }
}
