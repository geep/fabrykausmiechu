using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
namespace BAZA
{

    public class PROPS : XPLiteObject
    {
        string fKEY;
        [Key]
        [Size(50)]
        public string KEY
        {
            get { return fKEY; }
            set { SetPropertyValue<string>("KEY", ref fKEY, value); }
        }
        string fVAL;
        [Size(200)]
        public string VAL
        {
            get { return fVAL; }
            set { SetPropertyValue<string>("VAL", ref fVAL, value); }
        }
        public PROPS(Session session) : base(session) { }
        public PROPS() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class IMAGES : XPLiteObject
    {
        PATIENT fPATIENT_ID;
        [Association(@"IMAGES.PATIENT_ID_PATIENT")]
        public PATIENT PATIENT_ID
        {
            get { return fPATIENT_ID; }
            set { SetPropertyValue<PATIENT>("PATIENT_ID", ref fPATIENT_ID, value); }
        }
        int fID;
        [Key]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        byte[] fIMAGE;
        public byte[] IMAGE
        {
            get { return fIMAGE; }
            set { SetPropertyValue<byte[]>("IMAGE", ref fIMAGE, value); }
        }
        byte[] fTHUMBNAIL;
        public byte[] THUMBNAIL
        {
            get { return fTHUMBNAIL; }
            set { SetPropertyValue<byte[]>("THUMBNAIL", ref fTHUMBNAIL, value); }
        }
        DateTime fCDATE;
        public DateTime CDATE
        {
            get { return fCDATE; }
            set { SetPropertyValue<DateTime>("CDATE", ref fCDATE, value); }
        }
        string fCAPTION;
        public string CAPTION
        {
            get { return fCAPTION; }
            set { SetPropertyValue<string>("CAPTION", ref fCAPTION, value); }
        }
        public IMAGES(Session session) : base(session) { }
        public IMAGES() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class EXAMINATIONS : XPLiteObject
    {
        PATIENT fPATIENT_ID;
        [Key]
        [Association(@"EXAMINATIONS.PATIENT_ID_PATIENT")]
        public PATIENT PATIENT_ID
        {
            get { return fPATIENT_ID; }
            set { SetPropertyValue<PATIENT>("PATIENT_ID", ref fPATIENT_ID, value); }
        }
        int fANSWERS;
        public int ANSWERS
        {
            get { return fANSWERS; }
            set { SetPropertyValue<int>("ANSWERS", ref fANSWERS, value); }
        }
        float fMMS;
        public float MMS
        {
            get { return fMMS; }
            set { SetPropertyValue<float>("MMS", ref fMMS, value); }
        }
        int fTOOTHS;
        public int TOOTHS
        {
            get { return fTOOTHS; }
            set { SetPropertyValue<int>("TOOTHS", ref fTOOTHS, value); }
        }
        int fTCROSS;
        public int TCROSS
        {
            get { return fTCROSS; }
            set { SetPropertyValue<int>("TCROSS", ref fTCROSS, value); }
        }
        int fREMOVED;
        public int REMOVED
        {
            get { return fREMOVED; }
            set { SetPropertyValue<int>("REMOVED", ref fREMOVED, value); }
        }
        int fMILKIES;
        public int MILKIES
        {
            get { return fMILKIES; }
            set { SetPropertyValue<int>("MILKIES", ref fMILKIES, value); }
        }
        int fROOTS;
        public int ROOTS
        {
            get { return fROOTS; }
            set { SetPropertyValue<int>("ROOTS", ref fROOTS, value); }
        }
        int fCROWNS;
        public int CROWNS
        {
            get { return fCROWNS; }
            set { SetPropertyValue<int>("CROWNS", ref fCROWNS, value); }
        }
        int fBRIDGES;
        public int BRIDGES
        {
            get { return fBRIDGES; }
            set { SetPropertyValue<int>("BRIDGES", ref fBRIDGES, value); }
        }
        string fADNOTATIONS;
        [Size(50)]
        public string ADNOTATIONS
        {
            get { return fADNOTATIONS; }
            set { SetPropertyValue<string>("ADNOTATIONS", ref fADNOTATIONS, value); }
        }
        public EXAMINATIONS(Session session) : base(session) { }
        public EXAMINATIONS() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class CURETIME : XPLiteObject
    {
        PATIENT fPATIENT_ID;
        [Key]
        [Association(@"CURETIME.PATIENT_ID_PATIENT")]
        public PATIENT PATIENT_ID
        {
            get { return fPATIENT_ID; }
            set { SetPropertyValue<PATIENT>("PATIENT_ID", ref fPATIENT_ID, value); }
        }
        DateTime fBDATE;
        public DateTime BDATE
        {
            get { return fBDATE; }
            set { SetPropertyValue<DateTime>("BDATE", ref fBDATE, value); }
        }
        DateTime fEDATE;
        public DateTime EDATE
        {
            get { return fEDATE; }
            set { SetPropertyValue<DateTime>("EDATE", ref fEDATE, value); }
        }
        public CURETIME(Session session) : base(session) { }
        public CURETIME() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class DOCUMENTS : XPLiteObject
    {
        PATIENT fPATIENT_ID;
        [Key]
        [Association(@"DOCUMENTS.PATIENT_ID_PATIENT")]
        public PATIENT PATIENT_ID
        {
            get { return fPATIENT_ID; }
            set { SetPropertyValue<PATIENT>("PATIENT_ID", ref fPATIENT_ID, value); }
        }
        byte[] fDIAGNOSE;
        public byte[] DIAGNOSE
        {
            get { return fDIAGNOSE; }
            set { SetPropertyValue<byte[]>("DIAGNOSE", ref fDIAGNOSE, value); }
        }
        byte[] fHEALING;
        public byte[] HEALING
        {
            get { return fHEALING; }
            set { SetPropertyValue<byte[]>("HEALING", ref fHEALING, value); }
        }
        byte[] fPROBLEM;
        public byte[] PROBLEM
        {
            get { return fPROBLEM; }
            set { SetPropertyValue<byte[]>("PROBLEM", ref fPROBLEM, value); }
        }
        byte[] fAPARATUS;
        public byte[] APARATUS
        {
            get { return fAPARATUS; }
            set { SetPropertyValue<byte[]>("APARATUS", ref fAPARATUS, value); }
        }
        byte[] fPAINT0;
        public byte[] PAINT0
        {
            get { return fPAINT0; }
            set { SetPropertyValue<byte[]>("PAINT0", ref fPAINT0, value); }
        }
        byte[] fPAINT1;
        public byte[] PAINT1
        {
            get { return fPAINT1; }
            set { SetPropertyValue<byte[]>("PAINT1", ref fPAINT1, value); }
        }
        byte[] fPAINT2;
        public byte[] PAINT2
        {
            get { return fPAINT2; }
            set { SetPropertyValue<byte[]>("PAINT2", ref fPAINT2, value); }
        }
        public DOCUMENTS(Session session) : base(session) { }
        public DOCUMENTS() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PATIENT : XPLiteObject
    {
        int fID;
        [Key]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fNAME;
        [Size(30)]
        public string NAME
        {
            get { return fNAME; }
            set { SetPropertyValue<string>("NAME", ref fNAME, value); }
        }
        string fSURNAME;
        [Size(30)]
        public string SURNAME
        {
            get { return fSURNAME; }
            set { SetPropertyValue<string>("SURNAME", ref fSURNAME, value); }
        }
        DateTime fBIRTH_DATE;
        public DateTime BIRTH_DATE
        {
            get { return fBIRTH_DATE; }
            set { SetPropertyValue<DateTime>("BIRTH_DATE", ref fBIRTH_DATE, value); }
        }
        byte[] fIMAGE;
        public byte[] IMAGE
        {
            get { return fIMAGE; }
            set { SetPropertyValue<byte[]>("IMAGE", ref fIMAGE, value); }
        }
        string fSEX;
        [Size(20)]
        public string SEX
        {
            get { return fSEX; }
            set { SetPropertyValue<string>("SEX", ref fSEX, value); }
        }
        string fTELEPHONE;
        [Size(20)]
        public string TELEPHONE
        {
            get { return fTELEPHONE; }
            set { SetPropertyValue<string>("TELEPHONE", ref fTELEPHONE, value); }
        }
        string fTELEPHONE2;
        [Size(20)]
        public string TELEPHONE2
        {
            get { return fTELEPHONE2; }
            set { SetPropertyValue<string>("TELEPHONE2", ref fTELEPHONE2, value); }
        }
        string fMAIL;
        [Size(50)]
        public string MAIL
        {
            get { return fMAIL; }
            set { SetPropertyValue<string>("MAIL", ref fMAIL, value); }
        }
        string fADDRESS;
        [Size(50)]
        public string ADDRESS
        {
            get { return fADDRESS; }
            set { SetPropertyValue<string>("ADDRESS", ref fADDRESS, value); }
        }
        string fCITY;
        [Size(30)]
        public string CITY
        {
            get { return fCITY; }
            set { SetPropertyValue<string>("CITY", ref fCITY, value); }
        }
        string fPOSTAL_CODE;
        [Size(20)]
        public string POSTAL_CODE
        {
            get { return fPOSTAL_CODE; }
            set { SetPropertyValue<string>("POSTAL_CODE", ref fPOSTAL_CODE, value); }
        }
        string fPESEL;
        [Size(20)]
        public string PESEL
        {
            get { return fPESEL; }
            set { SetPropertyValue<string>("PESEL", ref fPESEL, value); }
        }
        string fPROFESSION;
        [Size(30)]
        public string PROFESSION
        {
            get { return fPROFESSION; }
            set { SetPropertyValue<string>("PROFESSION", ref fPROFESSION, value); }
        }
        DOCTOR fDOCTOR_ID;
        [Association(@"PATIENT.DOCTOR_ID_DOCTOR")]
        public DOCTOR DOCTOR_ID
        {
            get { return fDOCTOR_ID; }
            set { SetPropertyValue<DOCTOR>("DOCTOR_ID", ref fDOCTOR_ID, value); }
        }
        DateTime fWITSDATE;
        public DateTime WITSDATE
        {
            get { return fWITSDATE; }
            set { SetPropertyValue<DateTime>("WITSDATE", ref fWITSDATE, value); }
        }
        string fWITSID;
        [Size(255)]
        public string WITSID
        {
            get { return fWITSID; }
            set { SetPropertyValue<string>("WITSID", ref fWITSID, value); }
        }
        string fDOCTORMOVEDATA;
        [Size(255)]
        public string DOCTORMOVEDATA
        {
            get { return fDOCTORMOVEDATA; }
            set { SetPropertyValue<string>("DOCTORMOVEDATA", ref fDOCTORMOVEDATA, value); }
        }
        string fCONSULTATION;
        [Size(255)]
        public string CONSULTATION
        {
            get { return fCONSULTATION; }
            set { SetPropertyValue<string>("CONSULTATION", ref fCONSULTATION, value); }
        }
        [Association(@"IMAGES.PATIENT_ID_PATIENT")]
        public XPCollection<IMAGES> IMAGESs
        {
            get { return GetCollection<IMAGES>(@"IMAGESs"); }
        }
        [Association(@"EXAMINATIONS.PATIENT_ID_PATIENT")]
        public XPCollection<EXAMINATIONS> EXAMINATIONSs
        {
            get { return GetCollection<EXAMINATIONS>(@"EXAMINATIONSs"); }
        }
        [Association(@"CURETIME.PATIENT_ID_PATIENT")]
        public XPCollection<CURETIME> CURETIMEs
        {
            get { return GetCollection<CURETIME>(@"CURETIMEs"); }
        }
        [Association(@"DOCUMENTS.PATIENT_ID_PATIENT")]
        public XPCollection<DOCUMENTS> DOCUMENTSs
        {
            get { return GetCollection<DOCUMENTS>(@"DOCUMENTSs"); }
        }
        [Association(@"APPOINTMENTS.PATIENT_ID_PATIENT")]
        public XPCollection<APPOINTMENTS> APPOINTMENTSs
        {
            get { return GetCollection<APPOINTMENTS>(@"APPOINTMENTSs"); }
        }
        public PATIENT(Session session) : base(session) { }
        public PATIENT() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class APPOINTMENTS : XPLiteObject
    {
        int fID;
        [Key]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fHEADER;
        [Size(50)]
        public string HEADER
        {
            get { return fHEADER; }
            set { SetPropertyValue<string>("HEADER", ref fHEADER, value); }
        }
        string fNOTE;
        [Size(1000)]
        public string NOTE
        {
            get { return fNOTE; }
            set { SetPropertyValue<string>("NOTE", ref fNOTE, value); }
        }
        int fAHOUR;
        public int AHOUR
        {
            get { return fAHOUR; }
            set { SetPropertyValue<int>("AHOUR", ref fAHOUR, value); }
        }
        int fAMINUTE;
        public int AMINUTE
        {
            get { return fAMINUTE; }
            set { SetPropertyValue<int>("AMINUTE", ref fAMINUTE, value); }
        }
        int fDURATION;
        public int DURATION
        {
            get { return fDURATION; }
            set { SetPropertyValue<int>("DURATION", ref fDURATION, value); }
        }
        PATIENT fPATIENT_ID;
        [Association(@"APPOINTMENTS.PATIENT_ID_PATIENT")]
        public PATIENT PATIENT_ID
        {
            get { return fPATIENT_ID; }
            set { SetPropertyValue<PATIENT>("PATIENT_ID", ref fPATIENT_ID, value); }
        }
        DOCTOR fDOCTOR_ID;
        [Association(@"APPOINTMENTS.DOCTOR_ID_DOCTOR")]
        public DOCTOR DOCTOR_ID
        {
            get { return fDOCTOR_ID; }
            set { SetPropertyValue<DOCTOR>("DOCTOR_ID", ref fDOCTOR_ID, value); }
        }
        double fPRICE;
        public double PRICE
        {
            get { return fPRICE; }
            set { SetPropertyValue<double>("PRICE", ref fPRICE, value); }
        }
        double fPAID;
        public double PAID
        {
            get { return fPAID; }
            set { SetPropertyValue<double>("PAID", ref fPAID, value); }
        }
        byte[] fPROCEEDINGS;
        public byte[] PROCEEDINGS
        {
            get { return fPROCEEDINGS; }
            set { SetPropertyValue<byte[]>("PROCEEDINGS", ref fPROCEEDINGS, value); }
        }
        string fUPPERARC;
        [Size(255)]
        public string UPPERARC
        {
            get { return fUPPERARC; }
            set { SetPropertyValue<string>("UPPERARC", ref fUPPERARC, value); }
        }
        string fLOWERARC;
        [Size(255)]
        public string LOWERARC
        {
            get { return fLOWERARC; }
            set { SetPropertyValue<string>("LOWERARC", ref fLOWERARC, value); }
        }
        DateTime fCDATE;
        public DateTime CDATE
        {
            get { return fCDATE; }
            set { SetPropertyValue<DateTime>("CDATE", ref fCDATE, value); }
        }
        int fHYGENE;
        public int HYGENE
        {
            get { return fHYGENE; }
            set { SetPropertyValue<int>("HYGENE", ref fHYGENE, value); }
        }
        int fCOLOUR;
        public int COLOUR
        {
            get { return fCOLOUR; }
            set { SetPropertyValue<int>("COLOUR", ref fCOLOUR, value); }
        }
        int fPRESENT;
        public int PRESENT
        {
            get { return fPRESENT; }
            set { SetPropertyValue<int>("PRESENT", ref fPRESENT, value); }
        }
        string fDOCTORMOVEDATA;
        [Size(255)]
        public string DOCTORMOVEDATA
        {
            get { return fDOCTORMOVEDATA; }
            set { SetPropertyValue<string>("DOCTORMOVEDATA", ref fDOCTORMOVEDATA, value); }
        }
        int fMASTERCHECK;
        public int MASTERCHECK
        {
            get { return fMASTERCHECK; }
            set { SetPropertyValue<int>("MASTERCHECK", ref fMASTERCHECK, value); }
        }
        public APPOINTMENTS(Session session) : base(session) { }
        public APPOINTMENTS() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class DOCTOR : XPLiteObject
    {
        int fID;
        [Key]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fNAME;
        [Size(30)]
        public string NAME
        {
            get { return fNAME; }
            set { SetPropertyValue<string>("NAME", ref fNAME, value); }
        }
        string fSURNAME;
        [Size(30)]
        public string SURNAME
        {
            get { return fSURNAME; }
            set { SetPropertyValue<string>("SURNAME", ref fSURNAME, value); }
        }
        byte[] fIMAGE;
        public byte[] IMAGE
        {
            get { return fIMAGE; }
            set { SetPropertyValue<byte[]>("IMAGE", ref fIMAGE, value); }
        }
        [Association(@"PATIENT.DOCTOR_ID_DOCTOR")]
        public XPCollection<PATIENT> PATIENTs
        {
            get { return GetCollection<PATIENT>(@"PATIENTs"); }
        }
        [Association(@"APPOINTMENTS.DOCTOR_ID_DOCTOR")]
        public XPCollection<APPOINTMENTS> APPOINTMENTSs
        {
            get { return GetCollection<APPOINTMENTS>(@"APPOINTMENTSs"); }
        }
        public DOCTOR(Session session) : base(session) { }
        public DOCTOR() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
