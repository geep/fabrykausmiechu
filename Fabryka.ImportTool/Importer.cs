﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using Fabryka.Business;
using BAZA;
using System.Windows.Documents;
using System.IO;
using System.Windows;
using System.Text.RegularExpressions;

namespace Fabryka.ImportTool
{
    public class Importer
    {

        private readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        IDataLayer _oldLayer, _newLayer;

        XPCollection<DOCTOR> oldDoctors;
        XPCollection<Doctor> newDoctors;

        public Importer(IDataLayer oldLayer, IDataLayer newLayer)
        {
            _oldLayer = oldLayer;
            _newLayer = newLayer;
        }

        public void ImportAll()
        {
            ImportDoctors();
            ImportPatients();
            //ImportPhotos();
        }

        public void ImportDoctors()
        {
            logger.Info("Importuję lekarzy");

            using (UnitOfWork _oldSession = new UnitOfWork(_oldLayer))
            {
                using (UnitOfWork _newSession = new UnitOfWork(_newLayer))
                {
                    oldDoctors = new XPCollection<DOCTOR>(_oldSession);
                    newDoctors = new XPCollection<Doctor>(_newSession);

                    int count = 1;
                    foreach (var oldDoctor in oldDoctors)
                    {
                        logger.Info("Importuję lekarza: {0} {1} ({2}/{3})", oldDoctor.NAME, oldDoctor.SURNAME, count, oldDoctors.Count);
                        var doctor = new Doctor(_newSession)
                        {
                            Oid = oldDoctor.ID,
                            FirstName = oldDoctor.NAME,
                            LastName = oldDoctor.SURNAME,
                            CellphoneNumber = null,
                            Picture = BitmapImageSourceConverter.ImageFromBuffer(oldDoctor.IMAGE), // TODO: Przekonwertuj zdjęcie, format?
                        };

                        doctor.Save();

                        newDoctors.Add(doctor);
                        count++;
                    }
                    _newSession.CommitChanges();
                }
            }
        }

        private void ImportVisits(PATIENT oldPatient, Patient patient, UnitOfWork _OldSession, UnitOfWork _NewSession)
        {
            int count = 0;
            foreach (var visit in oldPatient.APPOINTMENTSs)
            {
                using (UnitOfWork _newSession = new UnitOfWork(_newLayer))
                {
                    var newVisit = new Visit(_newSession)
                    {
                        Oid = visit.ID,
                        Patient = _newSession.GetObjectByKey<Patient>(patient.Oid),
                        Doctor = (visit.DOCTOR_ID == null ? null : _newSession.GetObjectByKey<Doctor>(visit.DOCTOR_ID.ID)),
                        Description = BytesRTFToString(visit.PROCEEDINGS),
                        Payed = (decimal)visit.PAID,
                        Receivable = (decimal)visit.PRICE,
                        Time = visit.CDATE,
                        UpperArc = visit.UPPERARC,
                        LowerArc = visit.LOWERARC
                    };
                    _newSession.CommitChanges();
                }
                count++;
            }
            logger.Info("Importowanych wizyt: {0}", count);
        }


        private void ImportPatients()
        {
            logger.Info("Importuję pacjentów");
            using (UnitOfWork _tempSession = new UnitOfWork(_oldLayer))
            {
                var oldPatients = new XPCursor(_tempSession, typeof(PATIENT)) { PageSize = 10 };
                //newPatients = new XPCollection<Patient>(_newSession);

                int count = 1;
                foreach (PATIENT tmpPatient in oldPatients)
                {
                    using (UnitOfWork _oldSession = new UnitOfWork(_oldLayer))
                    {
                        var oldPatient = _oldSession.GetObjectByKey<PATIENT>(tmpPatient.ID);
                        using (UnitOfWork _newSession = new UnitOfWork(_newLayer))
                        {
                            logger.Info("Importuję pacjenta: {0} {1} ({2}/{3})", oldPatient.NAME, oldPatient.SURNAME, count, oldPatients.Count);

                            var fname = oldPatient.NAME;
                            var sname = oldPatient.SURNAME;
                            var regdate = oldPatient.CURETIMEs.FirstOrDefault() == null ? DateTime.Now :
                                    oldPatient.CURETIMEs.First().BDATE;
                            PatientFabrykaData pf = new PatientFabrykaData(_newSession)
                                {
                                    PESEL = oldPatient.PESEL
                                };

                            if (GetFabrykaData(ref fname, ref sname, pf))
                            {
                                if (pf.YearNumber == null)
                                {
                                    if (oldPatient.CURETIMEs.FirstOrDefault() != null)
                                        pf.YearNumber = regdate.Year;
                                }
                                logger.Info("Wykryłem numerki: {0} {1}, {2}/{3}/{4}, {5}-{6}",
                                    fname, sname, pf.PatientNumber ?? 0, pf.MonthNumber ?? 0, pf.YearNumber ?? 0,
                                    pf.ClinicNumberType ?? "-", pf.ClinicNumber ?? 0);
                            }

                            var plan = new TreatmentPlan(_newSession)
                                    {
                                        UpperJaw = (oldPatient.DOCUMENTSs.FirstOrDefault() == null ? null :
                                        BytesRTFToString(oldPatient.DOCUMENTSs.First().DIAGNOSE))
                                    };

                            plan.TreatmentPhases[0].PhasePlan = (oldPatient.DOCUMENTSs.FirstOrDefault() == null ? null :
                                        BytesRTFToString(oldPatient.DOCUMENTSs.First().HEALING));
                            
                            
                            var patient = new Patient(_newSession)
                            {
                                Oid = oldPatient.ID,
                                FirstName = fname,
                                LastName = sname,

                                Address = new Address(_newSession)
                                {
                                    City = oldPatient.CITY,
                                    Country = "Polska",
                                    Street = oldPatient.ADDRESS,
                                    PostalCode = oldPatient.POSTAL_CODE
                                },

                                Birthdate = oldPatient.BIRTH_DATE,

                                ContactData = new PatientContactData(_newSession)
                                {
                                    CellphoneNumber = oldPatient.TELEPHONE,
                                    SecondPhoneNumber = oldPatient.TELEPHONE2,
                                    EmailAddress = oldPatient.MAIL,
                                },

                                Doctor = (oldPatient.DOCTOR_ID == null ? null : _newSession.GetObjectByKey<Doctor>(oldPatient.DOCTOR_ID.ID)),
                                Gender = oldPatient.SEX == null ? Gender.None : oldPatient.SEX.Trim() == "M" ? Gender.Male : oldPatient.SEX.Trim() == "F" ? Gender.Female : Gender.None,

                                FabrykaData = pf,

                                Treatment = new Treatment(_newSession)
                                {
                                    Diagnosis = (oldPatient.DOCUMENTSs.FirstOrDefault() == null ? null :
                                        BytesRTFToString(oldPatient.DOCUMENTSs.First().DIAGNOSE)),
                                    DoctorProblems =
                                            (oldPatient.DOCUMENTSs.FirstOrDefault() == null ? null :
                                            BytesRTFToString(oldPatient.DOCUMENTSs.First().APARATUS)),
                                    PatientProblems =
                                            (oldPatient.DOCUMENTSs.FirstOrDefault() == null ? null :
                                            BytesRTFToString(oldPatient.DOCUMENTSs.First().PROBLEM)),
                                    Plan = plan
                                    
                                },

                                RegistrationDate = (regdate),
                            };

                            patient.Save();
                            _newSession.CommitChanges();
                            //newPatients.Add(patient);

                            

                            count++;

                            ImportPhotos(oldPatient, patient, _oldSession, _newSession);
                            ImportVisits(oldPatient, patient, _oldSession, _newSession);
                        }

                        
                    }
                }
            }
        }

        private bool GetFabrykaData(ref string fname, ref string sname, PatientFabrykaData pf)
        {
            bool ret = false;

            CleanPartial(ref fname, pf, ref ret);
            CleanPartial(ref sname, pf, ref ret);

            if (pf.YearNumber != null)
            {
                if (pf.YearNumber < 100)
                {
                    if (pf.YearNumber > 20)
                    {
                        pf.YearNumber += 1900;
                    }
                    else
                    {
                        pf.YearNumber += 2000;
                    }
                }
            }

            return ret;
        }

        private void CleanPartial(ref string name, PatientFabrykaData pf, ref bool ret)
        {
            var pattern = @"(\d+)\s*/\s*(\d+)\s*/\s*(\d+)";
            var res = Regex.Match(name, pattern);
            if (res.Success)
            {
                ret = true;
                pf.PatientNumber = int.Parse(res.Groups[1].Value);
                pf.MonthNumber = int.Parse(res.Groups[2].Value);
                pf.YearNumber = int.Parse(res.Groups[3].Value);
                name = Regex.Replace(name, pattern, String.Empty);
            }

            pattern = @"(\d+)\s*/\s*(\d+)";
            res = Regex.Match(name, pattern);
            if (pf.PatientNumber == null && res.Success)
            {
                ret = true;
                pf.PatientNumber = int.Parse(res.Groups[1].Value);
                pf.YearNumber = int.Parse(res.Groups[2].Value);
                name = Regex.Replace(name, pattern, String.Empty);
            }

            pattern = @"(G|S|g|s)\s*-\s*(\d+)";
            res = Regex.Match(name, pattern);
            if (res.Success)
            {
                ret = true;
                pf.ClinicNumberType = res.Groups[1].Value.ToUpper();
                pf.ClinicNumber = int.Parse(res.Groups[2].Value);
                name = Regex.Replace(name, pattern, String.Empty);
            }

            pattern = @"\(\s*(S|s)\s*\)";
            res = Regex.Match(name, pattern);
            if (res.Success)
            {
                ret = true;
                pf.ClinicNumberType = "S";
                name = Regex.Replace(name, pattern, String.Empty);
            }

            pattern = @"(\d+)";
            res = Regex.Match(name, pattern);
            if (pf.PatientNumber == null && res.Success)
            {
                ret = true;
                pf.PatientNumber = int.Parse(res.Groups[1].Value);
                name = Regex.Replace(name, pattern, String.Empty);
            }

            name = name.Trim();

            pattern = @"\s+";
            res = Regex.Match(name, pattern);
            if (res.Success)
            {
                logger.Error("BŁĄD: Ciągle coś nie tak: {0}.", name);
            }
        }
        int largeOID = 20000;

        private void ImportPhotos(PATIENT oldPatient, Patient newPatient, UnitOfWork _oldSession, UnitOfWork patientSession)
        {
            //logger.Info("Importuję zdjęcia");
            //oldPhotos = new XPCollection<IMAGES>(_oldSession);
                //newPhotos = new XPCollection<Photo>(_newSession);

            int count = 1;
            foreach (IMAGES oldPhoto in oldPatient.IMAGESs)
            {
                logger.Debug("Importuję zdjęcie: {0} - {1} ({2}/{3})", oldPhoto.ID, oldPhoto.CAPTION, count, oldPatient.IMAGESs.Count);
                using (UnitOfWork _newSession = new UnitOfWork(_newLayer))
                {
                    var newPhoto = new Photo(_newSession)
                    {
                        Oid = oldPhoto.ID,
                        Name = oldPhoto.CAPTION,
                        Patient = _newSession.GetObjectByKey<Patient>(newPatient.Oid),
                        Picture = BitmapImageSourceConverter.ImageFromBuffer(oldPhoto.IMAGE),
                        Thumbnail = BitmapImageSourceConverter.ImageFromBuffer(oldPhoto.THUMBNAIL)
                    };

                    //newPhotos.Add(newPhoto);
                    newPhoto.Save();
                    _newSession.CommitChanges();
                    count++;
                }
            }

            if (oldPatient.IMAGE != null)
            {
                using (UnitOfWork _newSession = new UnitOfWork(_newLayer))
                {
                    var patient = _newSession.GetObjectByKey<Patient>(newPatient.Oid);
                    var newPhoto = new Photo(_newSession)
                    {
                        Oid = largeOID++,
                        Name = String.Format("{0} - główne zdjęcie", newPatient.LastName),
                        Patient = patient,
                        Picture = BitmapImageSourceConverter.ImageFromBuffer(oldPatient.IMAGE),
                        Thumbnail = BitmapImageSourceConverter.ImageFromBuffer(oldPatient.IMAGE),
                    };
                    newPhoto.Save();
                    patient.MainPhoto = newPhoto;
                    patient.Save();
                    _newSession.CommitChanges();
                }
            }
        }

        public static string BytesRTFToString(byte[] content)
        {
            //return null;
            if (content != null)
            {
                MemoryStream stream = new MemoryStream(content);
                //FlowDocument flow = new FlowDocument();
                //TextRange range = new TextRange(flow.ContentStart, flow.ContentEnd);
                //range.Load(stream, DataFormats.Rtf);
                //stream.Close();
                StreamReader sr = new StreamReader(stream);
                string result = sr.ReadToEnd();
                sr.Close();
                return result;
            }
            return null;
        }
    }
}
