﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Prism.Commands;
using System.IO;
using System.ComponentModel;
using NLog;
using NLog.Targets;
using System.Windows.Threading;
using System.Threading.Tasks;
using DevExpress.Xpo.DB;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using System.Reflection; 

namespace Fabryka.ImportTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public DelegateCommand ConvertCommand { get; set; }

        private readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        DispatcherTimer timer = new DispatcherTimer();

        private static List<string> logs = new List<string>();

        public static void LogMethod(string message)
        {
            lock (logs)
            {
                logs.Add(message);
            }
        } 

        private string _ortodoncjaFile = Properties.Settings.Default.LastOrtodoncjaPath;
        public string OrtodoncjaFile
        {
            get
            {
                return _ortodoncjaFile;
            }
            set
            {
            	_ortodoncjaFile = value;
                RaisePropertyChanged("OrtodoncjaFile");
                ConvertCommand.RaiseCanExecuteChanged();
            }
        }

        private string _fabrykaFile = Properties.Settings.Default.LastFabrykaPath;
        public string FabrykaFile
        {
            get
            {
                return _fabrykaFile;
            }
            set
            {
            	_fabrykaFile = value;
                RaisePropertyChanged("FabrykaFile");
                ConvertCommand.RaiseCanExecuteChanged();
            }
        }

        public MainWindow()
        {
            MethodCallTarget target = new MethodCallTarget();
            target.ClassName = typeof(MainWindow).AssemblyQualifiedName;
            target.MethodName = "LogMethod";
            target.Parameters.Add(new MethodCallParameter("${message}")); 

            NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Info);
            ConvertCommand = new DelegateCommand(() => StartConversion(), () => ValidatePaths());

            InitializeComponent();
        }

        private void createFabrykaFile_Click(object sender, RoutedEventArgs e)
        {

        }

        private void fabrykaFilePath_DefaultButtonClick(object sender, RoutedEventArgs e)
        {

        }

        private void ortodoncjaFilePath_DefaultButtonClick(object sender, RoutedEventArgs e)
        {

        }

        private bool ValidatePaths()
        {
            if (String.IsNullOrWhiteSpace(FabrykaFile) || String.IsNullOrWhiteSpace(OrtodoncjaFile))
                return false;

            return File.Exists(FabrykaFile) && File.Exists(OrtodoncjaFile);
        }

        private void StartConversion()
        {
            databaseGroup.IsEnabled = false;

            Properties.Settings.Default.LastOrtodoncjaPath = OrtodoncjaFile;
            Properties.Settings.Default.LastFabrykaPath = FabrykaFile;
            Properties.Settings.Default.Save();

            timer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            timer.Tick += timer_Tick;
            timer.Start();

            try
            {

                XPDictionary oldDict = new ReflectionDictionary();
                oldDict.CollectClassInfos(Assembly.GetAssembly(typeof(BAZA.CURETIME)));
                var connectionString = FirebirdConnectionProvider.GetConnectionString("127.0.0.1", "ortodoncja", "ortodoncja", OrtodoncjaFile);
                var oldLayer = XpoDefault.GetDataLayer(connectionString, oldDict, AutoCreateOption.None);

                XPDictionary newDict = new ReflectionDictionary();
                newDict.CollectClassInfos(Assembly.GetAssembly(typeof(Fabryka.Business.Appointment)));
                connectionString = FirebirdConnectionProvider.GetConnectionString("127.0.0.1", "fabryka", "fzu", FabrykaFile);
                var newLayer = XpoDefault.GetDataLayer(connectionString, oldDict, AutoCreateOption.DatabaseAndSchema);
               // newSession.BeginTransaction();
                Importer importer = new Importer(oldLayer, newLayer);

                Task.Factory
                    .StartNew(() => { importer.ImportAll(); })
                    .ContinueWith(task =>
                    {
                        if (task.IsFaulted)
                        {
                            //newSession.RollbackTransaction();
                            logger.ErrorException("Błąd konwersji!", task.Exception);
                            logger.Error(string.Format("Błąd: {0}.", task.Exception));
                        }
                        else
                        {
                            //newSession.CommitTransaction();
                            logger.Info("Zakończono konwersję.");
                        }

                        //oldSession.Disconnect();
                        //newSession.Disconnect();
                        timer.Stop();
                        databaseGroup.IsEnabled = true;
                        PrintBacklog();
                    },
                        TaskScheduler.FromCurrentSynchronizationContext()
                    );
            }
            catch (Exception ex)
            {
                logger.ErrorException("Błąd łączenia z bazą danych!", ex);

                timer.Stop();
                databaseGroup.IsEnabled = true;
                PrintBacklog();

                return;
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            PrintBacklog();
        }

        private void PrintBacklog()
        {
            lock (logs)
            {
                foreach (var log in logs)
                {
                    textBox1.AppendText(String.Format("{0}\n", log));
                }
                logs.Clear();
            }
        }
        private void RaisePropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }

}
