﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;

namespace Fabryka.Logger
{
    public class GlobalLogger
    {
        private static GlobalLogger _Current;

        public readonly static NLog.Logger Logger = LogManager.GetCurrentClassLogger();

        public static GlobalLogger Current
        {
            get
            {
                if (_Current == null)
                {
                    _Current = new GlobalLogger();
                }

                return _Current;
            }
        }

        public static void Log(string message)
        {
            Logger.Fatal(message);
        }


    }
}
