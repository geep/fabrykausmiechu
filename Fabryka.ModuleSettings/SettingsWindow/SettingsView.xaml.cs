﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.Modularity;
using DevExpress.Xpf.Core;

namespace Fabryka.ModuleSettings.SettingsWindow
{
    /// <summary>
    /// Interaction logic for SettingsView.xaml
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class SettingsView : DXWindow, IPartImportsSatisfiedNotification //TODO: Rozważyć IDisposable
    {
        public SettingsView()
        {
            InitializeComponent();
        }

        [Import(AllowRecomposition = false)]
        public IModuleManager ModuleManager;

        [Import(AllowRecomposition = false)]
        public IRegionManager RegionManager;

        public void OnImportsSatisfied()
        {
            this.RegionManager.RequestNavigate(
                "SettingsRegion",
                new Uri("ApplicationSettingsView", UriKind.Relative));
        }
    }
}
