﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.ViewModel;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.ServiceLocation;

namespace Fabryka.ModuleSettings.Menu
{
    [Export]
    public class SettingsButtonViewModel : NotificationObject
    {
        public DelegateCommand OpenWindow { get; set; }

        public SettingsButtonViewModel()
        {
            OpenWindow = new DelegateCommand(
                () =>
                {
                    var sv = ServiceLocator.Current.GetInstance<SettingsWindow.SettingsView>();
                    sv.ShowDialog();
                });
        }
    }
}
