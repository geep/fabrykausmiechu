﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using Microsoft.Practices.Prism.Modularity;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.Regions;

namespace Fabryka.ModuleSettings
{
    [ModuleExport(typeof(ModuleSettings), InitializationMode = InitializationMode.WhenAvailable)]
    public class ModuleSettings : IModule
    {
        private readonly IRegionViewRegistry regionViewRegistry;

        [ImportingConstructor]
        public ModuleSettings(IRegionViewRegistry registry)
        {
            regionViewRegistry = registry;
        }

        public void Initialize()
        {
            regionViewRegistry.RegisterViewWithRegion("MainMenuButtonsRegion", typeof(Menu.SettingsButton));
        }
    }
}
