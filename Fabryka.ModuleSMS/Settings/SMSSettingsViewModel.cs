﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.ViewModel;
using System.Collections;
using System.Windows.Documents;
using Microsoft.Practices.Prism.Commands;

namespace Fabryka.ModuleSMS.Settings
{
    [Export]
    public class SMSSettingsViewModel : NotificationObject
    {
        public string ViewName
        {
            get
            {
                return "SMSy";
            }
        }

        // Fields...

        public DelegateCommand SaveCommand { get; private set; }
        public DelegateCommand CancelCommand { get; private set; }

        public SMSSettingsViewModel()
        {
            SaveCommand = new DelegateCommand(
                () =>
                {
                    //Properties.Settings.Default.NotifySettingsChanging();
                    Properties.Settings.Default.Save();
                }
            );
            CancelCommand = new DelegateCommand(
                () =>
                { 
                    Properties.Settings.Default.Reload(); 
                    //RaisePropertyChanged(() => ArcList); 
                });
        }
    }
}
