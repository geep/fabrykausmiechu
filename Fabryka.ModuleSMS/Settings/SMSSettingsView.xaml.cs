﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;

namespace Fabryka.ModuleSMS.Settings
{
    /// <summary>
    /// Interaction logic for VisitSettingsView.xaml
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class SMSSettingsView : UserControl
    {
        public SMSSettingsView()
        {
            InitializeComponent();
        }

        [Import]
        public SMSSettingsViewModel ViewModel
        {
            set
            {
                DataContext = value;
            }
        }

    }
}
