﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fabryka.Common.Services;
using System.Xml.Serialization;
using System.IO;
using Microsoft.Practices.ServiceLocation;
using System.ComponentModel.Composition;
using Fabryka.Common;

namespace Fabryka.ModuleSMS.Settings
{
    [Export(typeof(IModulesSettingsService)), Export(typeof(ModuleSettingsService))]
    public class ModuleSettingsService : IModulesSettingsService
    {
        public class SettingsSerialized
        {
            public string SMSAPIUser;
            public string SMSAPIPassword;
        }

        public string SettingsName
        {
            get { return "SMS"; }
        }

        public string GetSettingsAsXml()
        {
            XmlSerializer xs = new XmlSerializer(typeof(SettingsSerialized));
            StringWriter sw = new StringWriter();

            var ds = Fabryka.ModuleSMS.Properties.Settings.Default;

            var ss = new SettingsSerialized()
            {
                SMSAPIPassword = ds.SMSAPIPassword,
                SMSAPIUser = ds.SMSAPIUser
            };

            xs.Serialize(sw, ss);

            return sw.ToString();
        }

        public void SetSettingsAsXml(string xml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(SettingsSerialized));
            StringReader sr = new StringReader(xml);

            SettingsSerialized ss = (SettingsSerialized) xs.Deserialize(sr);

            var ds = Fabryka.ModuleSMS.Properties.Settings.Default;

            ds.SMSAPIPassword = ss.SMSAPIPassword;
            ds.SMSAPIUser = ss.SMSAPIUser;
            ds.Save();
        }

        public void NotifyChanged()
        {
            ServiceLocator.Current.GetInstance<IGlobalSettingsService>().SettingsChanged(this);
        }
    }


}
