﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using Microsoft.Practices.Prism.Modularity;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.Regions;

namespace Fabryka.ModuleSMS
{
    [ModuleExport(typeof(ModuleSMS), InitializationMode = InitializationMode.WhenAvailable)]
    public class ModuleSMS : IModule
    {
        private readonly IRegionViewRegistry regionViewRegistry;

        [ImportingConstructor]
        public ModuleSMS(IRegionViewRegistry registry)
        {
            regionViewRegistry = registry;
        }

        public void Initialize()
        {
            regionViewRegistry.RegisterViewWithRegion("SettingsRegion", typeof(Settings.SMSSettingsView));
            regionViewRegistry.RegisterViewWithRegion("MainMenuButtonsRegion", typeof(Menu.SMSButtonView));
        }
    }
}
