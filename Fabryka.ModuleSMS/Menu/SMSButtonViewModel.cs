﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.Composition;
using Fabryka.CommonWPF.Menu;

namespace Fabryka.ModuleSMS.Menu
{
    [Export]
    public class SMSButtonViewModel : MainMenuButtonViewModel
    {
        public SMSButtonViewModel()
        {
            UriName = "SMSView";
            SubMenuUriName = "SMSSubMenuView";
        }
    }
}
