﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.Regions;

namespace Fabryka.ModuleSMS.Menu
{
    /// <summary>
    /// Interaction logic for PhotosButton.xaml
    /// </summary>
    [Export]
    [ViewSortHint("050")]
    public partial class SMSButtonView : UserControl
    {
        public SMSButtonView()
        {
            InitializeComponent();
        }

        [Import]
        public SMSButtonViewModel ViewModel { set { DataContext = value; } }

    }
}
