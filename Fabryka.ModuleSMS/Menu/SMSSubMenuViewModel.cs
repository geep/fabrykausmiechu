﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Fabryka.Common.Services;
using Microsoft.Practices.Prism.Commands;
using Fabryka.Business;

namespace Fabryka.ModuleSMS.Menu
{
    [Export]
    public class SMSSubMenuViewModel
    {
        private readonly ISelectedPatientService selectedService;

        [ImportingConstructor]
        public SMSSubMenuViewModel(ISelectedPatientService _selectedService, IDataService _dataService)
        {
            selectedService = _selectedService;

            selectedService.SelectionChanged += (s, e) =>
                {

                };

        }
    }
}
