﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.ViewModel;
using Fabryka.Common.Services;
using Fabryka.Business;
using Microsoft.Practices.Prism.Commands;
using Fabryka.ModuleSMS.SMSApi;
using System.ServiceModel;

namespace Fabryka.ModuleSMS.SMS
{
    [Export]
    public class SMSViewModel : NotificationObject
    {
        public const int SMS_FIRST_MAX_LENGTH = 160;
        public const int SMS_MULTIPLE_MAX_LENGTH = 153;

        private string _MessageText;
        private readonly IDataService dataService;
        private readonly ISelectedPatientService selectedService;

        private Patient _SelectedPatient;
        public Patient SelectedPatient
        {
            get
            {
                return _SelectedPatient;
            }
            set
            {
                _SelectedPatient = value;
                SendSMS.RaiseCanExecuteChanged();
                RaisePropertyChanged(() => SelectedPatient);
            }
        }


        public string MessageText
        {
            get { return _MessageText; }
            set
            {
                _MessageText = value;
                RaisePropertyChanged(() => LetterCount);
                RaisePropertyChanged(() => NextLetterLimit);
                RaisePropertyChanged(() => SMSCount);
                SendSMS.RaiseCanExecuteChanged();
            }
        }
        
        public int LetterCount
        {
            get { return MessageText == null ? 0 : MessageText.Length; }
        }

        public int SMSCount
        {
            get
            {
                return LetterCount <= SMS_FIRST_MAX_LENGTH ? 1 :
                    (LetterCount + SMS_MULTIPLE_MAX_LENGTH - 1) / SMS_MULTIPLE_MAX_LENGTH;
            }
        }


        public int NextLetterLimit
        {
            get { return Math.Max(SMS_FIRST_MAX_LENGTH, SMSCount * SMS_MULTIPLE_MAX_LENGTH); }
        }

        public DelegateCommand SendSMS { get; set; }

        [ImportingConstructor]
        public SMSViewModel(ISelectedPatientService _selectedService, IDataService _dataService)
        {
            SendSMS = new DelegateCommand(
                () => SendMessage(),
                () => LetterCount > 0 && SelectedPatient != null
            );

            dataService = _dataService;
            selectedService = _selectedService;

            selectedService.SelectionChanged += selectedService_SelectionChanged;
            SelectedPatient = selectedService.SelectedPatient;

        }

        private static smsapiClient getClient()
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.SendTimeout = TimeSpan.FromMinutes(1);
            binding.OpenTimeout = TimeSpan.FromMinutes(1);
            binding.CloseTimeout = TimeSpan.FromMinutes(1);
            binding.ReceiveTimeout = TimeSpan.FromMinutes(10);

            binding.AllowCookies = false;
            binding.BypassProxyOnLocal = false;
            binding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            binding.MessageEncoding = WSMessageEncoding.Text;
            binding.TextEncoding = System.Text.Encoding.UTF8;
            binding.TransferMode = TransferMode.Buffered;
            binding.UseDefaultWebProxy = true;
            binding.MaxBufferSize = 65536;
            binding.MaxReceivedMessageSize = 65536;
            binding.MaxBufferPoolSize = 524288;

            var rq = new System.Xml.XmlDictionaryReaderQuotas()
            {
                MaxDepth = 32,
                MaxStringContentLength = 8192,
                MaxArrayLength = 16384,
                MaxBytesPerRead = 4096,
                MaxNameTableCharCount = 16384
            };

            binding.ReaderQuotas = rq;


            var sec = new BasicHttpSecurity()
            {
                Transport = new HttpTransportSecurity()
                {
                    ClientCredentialType = HttpClientCredentialType.None,
                    ProxyCredentialType = HttpProxyCredentialType.None
                },
                Message = new BasicHttpMessageSecurity()
                {
                    ClientCredentialType = BasicHttpMessageCredentialType.UserName,
                },
                Mode = BasicHttpSecurityMode.Transport
            };

            binding.Security = sec;

            smsapiClient service = new smsapiClient(binding, new EndpointAddress(@"https://ssl.smsapi.pl/api/soap/v2/webservices"));
            return service;
        }

        private void SendMessage()
        {
            smsapiClient service = getClient();
            Client myClient = new Client()
            {
                username = Properties.Settings.Default.SMSAPIUser,
                password = Properties.Settings.Default.SMSAPIPassword
            };

            SMSApi.SMS newSms = new SMSApi.SMS()
            {
                sender = @"SMSAPI",
                recipient = selectedService.SelectedPatient.ContactData.CellphoneNumber,
                eco = 1,
                date_send = 0,
                details = true,
                message = MessageText,
                //@params = new string[] { @"parametr 1", @"parametr 2", @"parametr 3", @"parametr 4" },
                idx = "0",
            };
            SendSms_Input sms_input = new SendSms_Input() { client = myClient, sms = newSms };

            SMSResult result = service.send_sms(sms_input);
        }

        void selectedService_SelectionChanged(object sender, EventArgs e)
        {
            SelectedPatient = selectedService.SelectedPatient;
        }
    }
}
