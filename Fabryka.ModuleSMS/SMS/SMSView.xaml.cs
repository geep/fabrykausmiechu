﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;

namespace Fabryka.ModuleSMS.SMS
{
    /// <summary>
    /// Interaction logic for SMSView.xaml
    /// </summary>
    [Export("SMSView", typeof(SMSView))]
    public partial class SMSView : UserControl
    {
        public SMSView()
        {
            InitializeComponent();
        }

        [Import]
        public SMSViewModel ViewModel
        {
            set
            {
                DataContext = value;
            }
        }
    }
}
