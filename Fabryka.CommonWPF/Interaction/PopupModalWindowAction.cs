﻿
namespace Fabryka.CommonWPF.Interaction
{
    using System.Windows;
    using Microsoft.Practices.Prism.Interactivity.InteractionRequest;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class PopupModalWindowAction : PopupModalWindowActionBase
    {
        /// <summary>
        /// The child window to display as part of the popup.
        /// </summary>
        public static readonly DependencyProperty ChildWindowProperty =
            DependencyProperty.Register(
                "ChildWindow",
                typeof(Window),
                typeof(PopupModalWindowAction),
                new PropertyMetadata(null));

        /// <summary>
        /// The <see cref="DataTemplate"/> to apply to the popup content.
        /// </summary>
        public static readonly DependencyProperty ContentTemplateProperty =
            DependencyProperty.Register(
                "ContentTemplate",
                typeof(DataTemplate),
                typeof(PopupModalWindowAction),
                new PropertyMetadata(null));

        /// <summary>
        /// Gets or sets the child window to pop up.
        /// </summary>
        /// <remarks>
        /// If not specified, a default child window is used instead.
        /// </remarks>
        public Window ChildWindow
        {
            get { return (Window)GetValue(ChildWindowProperty); }
            set { SetValue(ChildWindowProperty, value); }
        }

        /// <summary>
        /// Gets or sets the content template for a default child window.
        /// </summary>
        public DataTemplate ContentTemplate
        {
            get { return (DataTemplate)GetValue(ContentTemplateProperty); }
            set { SetValue(ContentTemplateProperty, value); }
        }

        /// <summary>
        /// Returns the child window to display as part of the trigger action.
        /// </summary>
        /// <param name="notification">The notification to display in the child window.</param>
        /// <returns></returns>
        protected override Window GetChildWindow(Notification notification)
        {
            var childWindow = this.ChildWindow ?? this.CreateDefaultWindow(notification);
            childWindow.DataContext = notification;

            return childWindow;
        }

        private Window CreateDefaultWindow(Notification notification)
        {
            return notification is Confirmation
                ? (Window)new DefaultConfirmationWindow { ConfirmationTemplate = this.ContentTemplate }
                : new DefaultNotificationWindow { NotificationTemplate = this.ContentTemplate };
        }
    }
}
