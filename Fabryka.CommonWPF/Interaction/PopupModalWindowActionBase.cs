﻿// -----------------------------------------------------------------------
// <copyright file="PopupChildWindowActionBase.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Fabryka.CommonWPF.Interaction
{
    using System;
    using System.Windows;
    using System.Windows.Interactivity;
    using Microsoft.Practices.Prism.Interactivity.InteractionRequest;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public abstract class PopupModalWindowActionBase : TriggerAction<FrameworkElement>
    {
        /// <summary>
        /// Displays the child window and collects results for <see cref="IInteractionRequest"/>.
        /// </summary>
        /// <param name="parameter">The parameter to the action. If the action does not require a parameter, the parameter may be set to a null reference.</param>
        protected override void Invoke(object parameter)
        {
            var args = parameter as InteractionRequestedEventArgs;
            if (args == null)
            {
                return;
            }

            var childWindow = this.GetChildWindow(args.Context);
            childWindow.Owner = Application.Current.MainWindow;

            var callback = args.Callback;
            EventHandler handler = null;
            handler =
                (o, e) =>
                {
                    childWindow.Closed -= handler;
                    callback();
                };
            childWindow.Closed += handler;

            childWindow.ShowDialog();
        }

        /// <summary>
        /// Returns the child window to display as part of the trigger action.
        /// </summary>
        /// <param name="notification">The notification to display in the child window.</param>
        /// <returns></returns>
        protected abstract Window GetChildWindow(Notification notification);
    }
}
