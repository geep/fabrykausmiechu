﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;

namespace Fabryka.CommonWPF.Interaction
{
    /// <summary>
    /// Interaction logic for NotificationChildWindow.xaml
    /// </summary>
    public partial class DefaultNotificationWindow : DXWindow
    {
        public static readonly DependencyProperty NotificationTemplateProperty =
           DependencyProperty.Register(
               "NotificationTemplate",
               typeof(DataTemplate),
               typeof(DefaultNotificationWindow),
               new PropertyMetadata(null));

        /// <summary>
        /// Creates a new instance of <see cref="DefaultNotificationWindow"/>
        /// </summary>
        public DefaultNotificationWindow()
        {
            InitializeComponent();
        }

        ///<summary>
        /// The <see cref="DataTemplate"/> to apply when displaying <see cref="Notification"/> data.
        ///</summary>
        public DataTemplate NotificationTemplate
        {
            get { return (DataTemplate)GetValue(NotificationTemplateProperty); }
            set { SetValue(NotificationTemplateProperty, value); }
        }
    }
}
