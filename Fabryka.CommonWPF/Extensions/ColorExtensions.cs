﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Fabryka.CommonWPF.Extensions
{
    public static class ColorExtensions
    {
        public static System.Windows.Media.Color AsMediaColor(this System.Drawing.Color color) 
        {
            return System.Windows.Media.Color.FromArgb(color.A, color.R, color.G, color.B);
        }

        public static System.Drawing.Color AsDrawingColor(this System.Windows.Media.Color color)
        {
            return System.Drawing.Color.FromArgb(color.A, color.R, color.G, color.B);
        }
    }
}