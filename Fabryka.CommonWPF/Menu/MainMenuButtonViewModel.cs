﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.ViewModel;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using System.Windows.Threading;
using Fabryka.Common.Services;

namespace Fabryka.CommonWPF.Menu
{
    public class MainMenuButtonViewModel : NotificationObject
    {
        [Import]
        public ILoadingService LoadingService { get; set; }

        public DelegateCommand OpenWindow { get; set; }

        private void EndOfNavigation(NavigationResult obj)
        {
            LoadingService.IsLoading = false;
        }

        protected string UriName { get; set; }

        protected string SubMenuUriName { get; set; }

        public MainMenuButtonViewModel()
        {
            OpenWindow = new DelegateCommand(
                () =>
                {
                    // Initialize
                    var regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();

                    // Show Main Region.
                    // LoadingService.IsLoading = true;
                    var moduleARibbonTab = new Uri(UriName, UriKind.Relative);
                    var subMenuUri = new Uri(SubMenuUriName ?? "EmptySubMenu", UriKind.Relative);

                    Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
                        {
                            regionManager.RequestNavigate("SubMenuRegion", subMenuUri);
                            regionManager.RequestNavigate("MainRegion", moduleARibbonTab, EndOfNavigation);
                        }));
                });

        }
    }
}
