using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using System.Reflection;
using System.ComponentModel;
using System.Globalization;

namespace Fabryka.CommonWPF.Converters
{
    public class EnumToDescriptionConverter : IValueConverter
    {
        public static string GetEnumDescription(Enum enumObj)
        {
            FieldInfo fieldInfo = enumObj.GetType().GetField(enumObj.ToString());

            var attribArray = fieldInfo.GetCustomAttributes(false).OfType<DescriptionAttribute>();

            if (attribArray.Count() == 0)
            {
                return enumObj.ToString();
            }
            else
            {

                return attribArray.First().Description;
            }
        }

        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Enum myEnum = (Enum)value;
            string description = GetEnumDescription(myEnum);
            return description;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Empty;
        }
    }

}