using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using System.Windows.Documents;

namespace Fabryka.CommonWPF.Converters
{
    [ValueConversion(typeof(bool), typeof(bool))]
    public class BooleanReverseConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(bool?))
            {
                if(targetType != typeof(bool))
                    throw new InvalidOperationException("The target must be a boolean");
                return !(bool)value;
            }
            var val = (bool?)!(bool)value;
            return val;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(bool))
                throw new InvalidOperationException("The target must be a boolean");

            return !(bool)value;
        }

        #endregion
    }
}
