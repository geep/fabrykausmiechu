using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Fabryka.CommonWPF.Converters
{
    public class EmptyNullStringConverter : IValueConverter
    {
        public object Convert(System.Object value, System.Type targetType, System.Object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is string))
                return value;
            return ((string)value == String.Empty) ? null : value;
        }
        public object ConvertBack(System.Object value, System.Type targetType, System.Object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is string))
                return value;
            return ((string)value == String.Empty) ? null : value;
        }
    }
}
