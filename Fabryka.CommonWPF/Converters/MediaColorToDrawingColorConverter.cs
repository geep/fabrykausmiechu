using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using System.Globalization;
using Fabryka.CommonWPF.Extensions;

namespace Fabryka.CommonWPF.Converters
{
    public class MediaColorToDrawingColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is System.Drawing.Color)
            {
                return ((System.Drawing.Color)value).AsMediaColor();
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is System.Windows.Media.Color)
            {
                return ((System.Windows.Media.Color)value).AsDrawingColor();
            }

            return null;
        }
    }
}
