﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Events;
using System.Threading;

namespace Fabryka.Common.Events
{
    public class DisconnectEvent : CompositePresentationEvent<DisconnectEventArgs>
    {
    }

    public class DisconnectEventArgs
    {
        public DisconnectEventArgs()
        {
            this.WaitHandle = new ManualResetEvent(false);
        }
        public ManualResetEvent WaitHandle { get; private set; }

        public Func<bool> Connected { get; set; }

        public string Message { get; set; }
    }
}
