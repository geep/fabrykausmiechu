﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fabryka.Common.Services
{
    public interface IModulesSettingsService
    {
        string SettingsName { get; }
        string GetSettingsAsXml();
        void SetSettingsAsXml(string xml);
    }
}
