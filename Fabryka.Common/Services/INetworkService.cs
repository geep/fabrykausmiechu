﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fabryka.Business;

namespace Fabryka.Common.Services
{
    public interface INetworkService
    {
        void Start(string hostname, int port);
        void Stop();
        void PostChanges(Fabryka.Business.UpdateType type);
        void PostMassChanges(HashSet<UpdateType> updates);
        void PostChangedAppointments(HashSet<Appointment> changedAppointments);
        void PostChangedPatients(HashSet<Patient> changedPatients);
        void PostSettingChanged(string settingsName, string settings);
        void CheckStatus();
        Dictionary<string, string> GetSettings();
        Func<bool> HandleReconnect { get; set; }
    }
}
