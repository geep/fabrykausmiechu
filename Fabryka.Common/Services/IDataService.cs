﻿using System;
using DevExpress.Xpo;
using Fabryka.Business;
using System.Collections.Generic;
namespace Fabryka.Common.Services
{
    public interface IDataService
    {
        void UpdateChangedAppointments(System.Collections.Generic.IEnumerable<int> oids);
        void UpdateChangedPatients(System.Collections.Generic.IEnumerable<int> oids);
        void Start(string connectionString);
        void Stop();
        Session GetCurrentSession();
        void RemovePatient(Patient removedPatient);
        void RemovePhoto(Photo selectedPhoto);
        int GetPhotoCount(Patient patient);
        void RemovePhotoComparison(PhotoComparison pc);
        Patient CreatePatient();
        void CreateActivePhase(TreatmentPlan plan);
        void RemoveActivePhase(TreatmentPhase treatmentPhase);
        Visit CreateVisit(Patient patient);
        void RemoveVisit(Visit selectedVisit);
        Photo CreatePhotoFromFile(Patient patient, string filename);
        PhotoComparison CreatePhotoComparison(Patient selectedPatient);
        XPCollection<Appointment> GetAppointments();
        XPCollection<AppointmentType> GetAppointmentTypes();
        XPCollection<Doctor> GetDoctors(bool secondarySession = false);
        XPCollection<Patient> GetPatients();
        Patient GetPatientByOid(int oid, bool secondarySession = false);
        XPCollection<NFZProcedure> GetNFZProcedures();
        NFZVisit GetNFZVisit(Visit selectedVisit);
        IEnumerable<NFZCompletedProcedure> GetRangedNFZVisits(DateTime from, DateTime to);
        XPCollection<AppointmentResource> GetAppointmentResources();
        AppointmentType GetDefaultAppointmentType(bool allDay);
        XPCollection<RTFTemplateNode> GetVisitTemplates();
        XPCollection<RTFTemplateNode> GetTypedTemplates(RTFTemplateType selectedType);
        XPCollection<RTFTemplateNode> GetTypedTemplateRoots(RTFTemplateType rTFTemplateType);
        void SaveChanges();

        void StartAppointmentUpdates();
        void StopAppointmentUpdates();
        void UpdateAppointments();
        void UpdatePhotos();
        void UpdatePhotoComparisons();
        void UpdateSelectedPatient();
        void UpdateTreatmentPlans();
        void UpdateVisits();
        void UpdateAllPatients();

        void BeginTransaction();
        void EndTransaction(UpdateType type = UpdateType.Patient);


    }
}
