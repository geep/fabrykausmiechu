﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Fabryka.Common.Services
{
    public interface ILoadingService : INotifyPropertyChanged
    {
        bool IsLoading { get; set; }
    }
}
