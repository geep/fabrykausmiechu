﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fabryka.Common.Services
{
    public interface ITeethPickerService
    {
        string GetResult();
    }
}
