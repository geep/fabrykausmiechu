﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fabryka.Business;
using System.Windows;

namespace Fabryka.Common.Services
{
    public interface ISelectedPatientService
    {
        void CreatePatient();
        void RemovePatient();
        Patient SelectedPatient { get; set; }

        bool CanGoBack { get; }
        bool CanGoForeward { get; }

        event EventHandler SelectionChanged;

        void GoBack();
        void GoForeward();

    }
}
