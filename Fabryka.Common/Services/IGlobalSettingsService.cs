﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fabryka.Common.Services
{
    public interface IGlobalSettingsService
    {
        void StartService(bool serverMode);
        void SettingsChanged(IModulesSettingsService moduleSettings);
        void RecieveSettingsChanges(string settingsName, string settings);
        Dictionary<string, string> GetSettings();
    }
}
