﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Microsoft.Practices.Prism.ViewModel;

namespace Fabryka.Common
{
    public class ApplicationState : NotificationObject
    {
        private static ApplicationState _Current;
        private bool _ServerMode;

        public static ApplicationState Current
        {
            get
            {
                if (_Current == null)
                    _Current = new ApplicationState();
                return _Current;
            }
        }


        public bool ServerMode
        {
            get { return _ServerMode; }
            set
            {
                _ServerMode = value;
                RaisePropertyChanged(() => ServerMode);
            }
        }
        
    }
}
