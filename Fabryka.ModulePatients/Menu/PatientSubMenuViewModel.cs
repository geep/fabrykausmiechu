﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Fabryka.Common.Services;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Fabryka.ModulePatient.Reports;
using DevExpress.XtraReports.UI;

namespace Fabryka.ModulePatient.Menu
{
    [Export]
    public class PatientSubMenuViewModel
    {
        private readonly ISelectedPatientService selectedService;

        public DelegateCommand CreatePatient { get; private set; }
        public DelegateCommand RemovePatient { get; private set; }
        public DelegateCommand PrintPatient { get; private set; }

        public InteractionRequest<Confirmation> ConfirmationRequest { get; private set; }

        [ImportingConstructor]
        public PatientSubMenuViewModel(ISelectedPatientService _selectedService)
        {
            selectedService = _selectedService;
            
            CreatePatient = new DelegateCommand(() => selectedService.CreatePatient());
            RemovePatient = new DelegateCommand(() => RemovePatientCall(), () => selectedService.SelectedPatient != null);
            PrintPatient = new DelegateCommand(() => PrintPatientReport(), () => selectedService.SelectedPatient != null);
            ConfirmationRequest = new InteractionRequest<Confirmation>();

            _selectedService.SelectionChanged += (s, e) =>
            {
                RemovePatient.RaiseCanExecuteChanged();
                PrintPatient.RaiseCanExecuteChanged();
            };

            
        }

        private void RemovePatientCall()
        {
            ConfirmationRequest.Raise(new Confirmation() { Content = "Czy na pewno chcesz skasować tego pacjenta?", Title = "Kasowanie pacjenta" },
                (cb) => { if (cb.Confirmed) selectedService.RemovePatient(); });           
        }

        private void PrintPatientReport()
        {
            if (selectedService.SelectedPatient == null)
                return;

            PatientReport report =
                                    new PatientReport();
            report.PatientID.Value = selectedService.SelectedPatient.Oid;
            ReportPrintTool printTool = new ReportPrintTool(report);
            printTool.ShowPreviewDialog();
        }

    }
}
