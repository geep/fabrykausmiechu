﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.Composition;
using Fabryka.CommonWPF.Menu;

namespace Fabryka.ModulePatient.Menu
{
    [Export]
    public class PatientButtonViewModel : MainMenuButtonViewModel
    {
        public PatientButtonViewModel()
        {
            UriName = "PatientDataView";
            SubMenuUriName = "PatientSubMenuView";

            //OpenWindow.Execute();
        }
    }
}
