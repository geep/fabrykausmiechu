﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.ViewModel;
using Fabryka.Common.Services;
using DevExpress.Xpo;
using Fabryka.Business;
using Microsoft.Practices.Prism.Commands;
using Fabryka.CommonWPF.Converters;

namespace Fabryka.ModulePatient.Settings
{
    [Export]
    public class TreatmentRTFTemplatesViewModel : NotificationObject
    {

        public string ViewName
        {
            get
            {
                return "Wzorce - leczenie";
            }
        }

        private RTFTemplateNode _SelectedNode;
        private RTFTemplateType _SelectedType = RTFTemplateType.Diagnosis;
        private IDataService dataService;

        public DelegateCommand AddGroup { get; set; }
        public DelegateCommand AddTemplate { get; set; }
        public DelegateCommand RemoveNode { get; set; }


        public XPCollection<RTFTemplateNode> TemplateNodes
        {
            get
            {
                return dataService.GetTypedTemplates(SelectedType);
            }
        }


        public static IEnumerable<Tuple<RTFTemplateType, string>> Types
        {
            get
            {
                return (new RTFTemplateType[] {RTFTemplateType.Diagnosis, RTFTemplateType.Braces, RTFTemplateType.TreatmentPlan})
                    .Select((st =>
                        Tuple.Create(st, EnumToDescriptionConverter.GetEnumDescription(st))));
            }
        }


        public RTFTemplateType SelectedType
        {
            get { return _SelectedType; }
            set
            {
                _SelectedType = value;
                RaisePropertyChanged(() => SelectedType);
                RaisePropertyChanged(() => TemplateNodes);
                SelectedNode = null;
            }
        }


        public RTFTemplateNode SelectedNode
        {
            get { return _SelectedNode; }
            set
            {
                _SelectedNode = value;
                RaisePropertyChanged(() => SelectedNode);
                AddGroup.RaiseCanExecuteChanged();
                RemoveNode.RaiseCanExecuteChanged();
                AddTemplate.RaiseCanExecuteChanged();
            }
        }

        [ImportingConstructor]
        public TreatmentRTFTemplatesViewModel(IDataService _dataService)
        {
            dataService = _dataService;

            AddGroup = new DelegateCommand(
                () =>
                    {
                        var node = SelectedNode;
                        TemplateNodes.Add(
                            new RTFTemplateNode(TemplateNodes.Session) { Name = "Nazwa grupy", IsLeaf = false, TemplateType = SelectedType, Parent = SelectedNode });
                        SelectedNode = node;
                    },
                () => SelectedNode == null || !SelectedNode.IsLeaf
                );

            AddTemplate = new DelegateCommand(
                () =>
                    {
                        var node = SelectedNode;
                        TemplateNodes.Add(new RTFTemplateNode(TemplateNodes.Session) { Name = "Nazwa wzorca", IsLeaf = true, TemplateType = SelectedType, Parent = SelectedNode });
                        SelectedNode = node;
                    },
                () => SelectedNode == null || !SelectedNode.IsLeaf
                );

            RemoveNode = new DelegateCommand(
                () => 
                {

                    if (SelectedNode != null)
                    {
                        var next = SelectedNode.Parent;
                        SelectedNode.Delete();
                        SelectedNode = next;
                    }
                }, () => SelectedNode != null);
            

        }
    }
}
