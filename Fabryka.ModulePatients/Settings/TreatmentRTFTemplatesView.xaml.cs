﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;

namespace Fabryka.ModulePatient.Settings
{
    /// <summary>
    /// Interaction logic for VisitRTFTemplatesView.xaml
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class TreatmentRTFTemplatesView : UserControl
    {
        public TreatmentRTFTemplatesView()
        {
            InitializeComponent();
        }

        [Import]
        public TreatmentRTFTemplatesViewModel ViewModel
        {
            set
            {
                DataContext = value;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            treeListView.FocusedRow = null;
        }
    }
}
