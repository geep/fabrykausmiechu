﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.ViewModel;
using DevExpress.Xpo;
using Fabryka.Business;
using Fabryka.Common.Services;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
using Fabryka.CommonWPF.Converters;
using Microsoft.Practices.ServiceLocation;

namespace Fabryka.ModulePatient.PatientData
{
    [Export]
    public class PatientDataViewModel : NotificationObject
    {
        private RTFTemplateType _SelectedTemplateType;
        private readonly IDataService dataService;
        private readonly ISelectedPatientService selectedService;
        private bool _IsNodePopupOpen;
        private RTFTemplateNode _SelectedNode;
        

        public DelegateCommand ManageActivePhases { get; private set; }
        public DelegateCommand AddPhase { get; private set; }
        public DelegateCommand<TreatmentPhase> RemovePhase { get; private set; }
        public InteractionRequest<Notification> ManagePhasesRequest { get; private set; }
        public InteractionRequest<Notification> TeethPickerRequest { get; private set; }

        public XPCollection<RTFTemplateNode> CurrentTemplateRoots { get; private set; }
        public RTFTemplateNode SelectedNode
        {
            get
            {
                return _SelectedNode;
            }

            private set
            {
                _SelectedNode = value;
                RaisePropertyChanged(() => SelectedNode);
                RaisePropertyChanged(() => SelectedNodeName);
            }
        }

        public string SelectedNodeName
        {
            get
            {
                return (_SelectedNode == null) ? 
                    EnumToDescriptionConverter.GetEnumDescription(SelectedTemplateType)
                    : _SelectedNode.Name;
            }
        }

        public RTFTemplateType SelectedTemplateType
        {
            get { return _SelectedTemplateType; }
            set { 
                _SelectedTemplateType = value;
                CurrentTemplateRoots = dataService.GetTypedTemplateRoots(value);
                RaisePropertyChanged(() => CurrentTemplateRoots);
                RaisePropertyChanged(() => SelectedNodeName);
            }
        }

        public bool IsNodePopupOpen
        {
            get { return _IsNodePopupOpen; }
            set
            {
                _IsNodePopupOpen = value;
                RaisePropertyChanged(() => IsNodePopupOpen);
            }
        }

        private XPCollection<RTFTemplateNode> _ChildrenNodes;
        public XPCollection<RTFTemplateNode> ChildrenNodes
        {
            get
            {
                return _ChildrenNodes;
            }
            private set
            {
                _ChildrenNodes = value;
                RaisePropertyChanged(() => ChildrenNodes);
            }
        }
        public DelegateCommand<RTFTemplateNode> OpenNode { get; private set; }
        public DelegateCommand ClosePopup { get; private set; }
        public Action<RTFTemplateNode, string> InsertRTFNode { get; set; }

        public DelegateCommand OpenBracesPopup { get; private set; }
        public DelegateCommand OpenDiagnosisPopup { get; private set; }
        public DelegateCommand OpenTreatmentPopup { get; private set; }

        private Patient _SelectedPatient;
        public Patient SelectedPatient
        {
            get
            {
                return _SelectedPatient;
            }
            set
            {
                if (_SelectedPatient != null)
                {
                    //_SelectedPatient.PropertyChanged +=  
                    _SelectedPatient.Changed -= _SelectedPatient_Changed;
                }

                _SelectedPatient = value;

                if (_SelectedPatient != null)
                {
                    _SelectedPatient.Changed += _SelectedPatient_Changed;
                }

                RaisePropertyChanged(() => SelectedPatient);

                if (_SelectedPatient != null)
                {
                    dataService.UpdateTreatmentPlans();
                    ImportantPhotos = _SelectedPatient.ImportantPhotos;
                    RaisePropertyChanged(() => ImportantPhotos);
                }

                AddPhase.RaiseCanExecuteChanged();
                RemovePhase.RaiseCanExecuteChanged();
            }
        }

        void _SelectedPatient_Changed(object sender, ObjectChangeEventArgs e)
        {
            if (e.PropertyName == "ImportantPhotos")
            {
                ImportantPhotos = _SelectedPatient.ImportantPhotos;
                RaisePropertyChanged(() => ImportantPhotos);
            }
        }

        public IList<Photo> ImportantPhotos { get; set; }

        public static IEnumerable<Tuple<Gender, string>> Genders
        {
            get
            {
                return Enum.GetValues(typeof(Gender))
                    .OfType<Gender>()
                    .Select((st =>
                        Tuple.Create(st, EnumToDescriptionConverter.GetEnumDescription(st))));
            }
        }

        public IList<Country> Countries = CountriesHelper.Countries;

        public XPCollection<Business.Doctor> Doctors
        {
            get
            {
                return dataService.GetDoctors();
            }
        }

        [ImportingConstructor]
        public PatientDataViewModel(ISelectedPatientService _selectedService, IDataService _dataService)
        {
            dataService = _dataService;
            selectedService = _selectedService;

            ClosePopup = new DelegateCommand(() => { IsNodePopupOpen = false;});

            OpenNode = new DelegateCommand<RTFTemplateNode>(
                (node) =>
                {
                    if (node == null)
                        return;

                    if (node.IsLeaf)
                    {


                        IsNodePopupOpen = false;
                        string teeth = String.Empty;
                        if (node.ShowTeethPicker)
                        {
                            TeethPickerRequest.Raise(new Notification() { Content = this, Title = "Wybór zębów" });
                            teeth = ServiceLocator.Current.GetInstance<ITeethPickerService>().GetResult();
                        }

                        InsertRTFNode(node, teeth);
                        return;
                    }

                    SelectedNode = node;
                    ChildrenNodes = node.Children;
                    IsNodePopupOpen = true;
                }
            );

            OpenBracesPopup = new DelegateCommand(() => OpenPopup(RTFTemplateType.Braces));
            OpenTreatmentPopup = new DelegateCommand(() => OpenPopup(RTFTemplateType.TreatmentPlan));
            OpenDiagnosisPopup = new DelegateCommand(() => OpenPopup(RTFTemplateType.Diagnosis));

            TeethPickerRequest = new InteractionRequest<Notification>();
            ManagePhasesRequest = new InteractionRequest<Notification>();

            ManageActivePhases = new DelegateCommand(

                () => ManagePhasesRequest.Raise(new Notification() { Content = this, Title = "Edycja aktywnych faz" }));
            AddPhase = new DelegateCommand(
                () =>
                    {
                        dataService.CreateActivePhase(SelectedPatient.Treatment.Plan);
                        AddPhase.RaiseCanExecuteChanged();
                        RemovePhase.RaiseCanExecuteChanged();
                    },
                () => SelectedPatient != null && SelectedPatient.Treatment.Plan.TreatmentPhases.Count < 4
                );
            RemovePhase = new DelegateCommand<TreatmentPhase>(
                (tp) =>
                {
                    dataService.RemoveActivePhase(tp);
                    AddPhase.RaiseCanExecuteChanged();
                    RemovePhase.RaiseCanExecuteChanged();
                },
                (tp) => SelectedPatient != null && SelectedPatient.Treatment.Plan.TreatmentPhases.Count > 1
                );

            selectedService.SelectionChanged += selectedService_SelectionChanged;
            SelectedPatient = selectedService.SelectedPatient;
            
        }

        private void OpenPopup(RTFTemplateType type)
        {
            SelectedTemplateType = type;
            IsNodePopupOpen = false;
            SelectedNode = null;
            IsNodePopupOpen = true;
            ChildrenNodes = dataService.GetTypedTemplateRoots(type);
        }

        void selectedService_SelectionChanged(object sender, EventArgs e)
        {
            SelectedPatient = selectedService.SelectedPatient;
        }
    }
}
