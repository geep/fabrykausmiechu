﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using System.Windows.Markup;
using System.Threading;
using Fabryka.Business;
using DevExpress.Xpo;
using Microsoft.Practices.Prism.Regions;
using DevExpress.Xpf.Editors;

namespace Fabryka.ModulePatient.PatientData
{
    /// <summary>
    /// Interaction logic for PatientDataView.xaml
    /// </summary>
    [Export("PatientDataView", typeof(PatientDataView))]
    public partial class PatientDataView : UserControl, IRegionMemberLifetime, INavigationAware
    {
        public PatientDataView()
        {
            this.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);
            InitializeComponent();
        }

        [Import]
        public PatientDataViewModel ViewModel
        {
            get
            {
                return DataContext as PatientDataViewModel;
            }
            set
            {
            	DataContext = value;
                value.InsertRTFNode = (node, text) => InsertRTFNode(node, text);
            }
        }

        private T FindChildControl<T>(DependencyObject control) where T : class
        {
            int childNumber = VisualTreeHelper.GetChildrenCount(control);
            for (int i = 0; i < childNumber; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(control, i);
                if (child != null && child is T)
                    return child as T;
                else
                    FindChildControl<T>(child);
            }
            return null;
        }

        DevExpress.Xpf.RichEdit.RichEditControl lastControl = null;

        private void InsertRTFNode(RTFTemplateNode node, string teeth)
        {
            DevExpress.XtraRichEdit.API.Native.Document doc;
            switch (node.TemplateType)
            {
                case RTFTemplateType.Diagnosis:
                    diagnosisREC.Focus();
                    doc = diagnosisREC.Document;
                    break;
                case RTFTemplateType.Braces:
                    bracesREC.Focus();
                    doc = bracesREC.Document;
                    break;
                case RTFTemplateType.TreatmentPlan:
                    //lastControl = FindChildControl<DevExpress.Xpf.RichEdit.RichEditControl>(Phases as DependencyObject);
                    lastControl.Focus();
                    doc = lastControl.Document;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("Template Type is invalid");
            }

            var text = node.Template.Replace("&",teeth);

            doc.InsertRtfText(doc.CaretPosition, text);
        }

        public bool KeepAlive
        {
            get { return true; }
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            ViewModel.ClosePopup.Execute();
        }

        private void richEditControl5_Loaded(object sender, RoutedEventArgs e)
        {
            lastControl = sender as DevExpress.Xpf.RichEdit.RichEditControl;
        }

    }
}
