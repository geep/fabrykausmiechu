﻿namespace Fabryka.ModulePatient.Reports
{
    partial class PatientReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PatientReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.patientCollection = new DevExpress.Xpo.XPCollection(this.components);
            this.PatientID = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.fakeBraces = new DevExpress.XtraReports.UI.XRRichText();
            this.xrRichText3 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.upperFake = new DevExpress.XtraReports.UI.XRRichText();
            this.lowerFake = new DevExpress.XtraReports.UI.XRRichText();
            this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrRichText2 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.RetentionReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.UpperJawReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail5 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrUpperJaw = new DevExpress.XtraReports.UI.XRRichText();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.LowerJawReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail6 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLowerJaw = new DevExpress.XtraReports.UI.XRRichText();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail7 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tonn1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.bolton1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.littl1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.kork1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tonn2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.bolton2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.littl2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.kork2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tonn3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.bolton3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.littl3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.kork3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tonn4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.bolton4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.littl4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.kork4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tonn5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.bolton5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.littl5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.kork5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.hasJawOrUpper = new DevExpress.XtraReports.UI.FormattingRule();
            this.hasJaw = new DevExpress.XtraReports.UI.FormattingRule();
            this.hasUpper = new DevExpress.XtraReports.UI.FormattingRule();
            this.DetailReport4 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail8 = new DevExpress.XtraReports.UI.DetailBand();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            ((System.ComponentModel.ISupportInitialize)(this.patientCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fakeBraces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upperFake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowerFake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrUpperJaw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrLowerJaw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel15,
            this.xrLabel14,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1,
            this.xrPictureBox2,
            this.xrPictureBox1});
            this.Detail.HeightF = 136.5417F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RegistrationDate", "{0:dd.MM.yyyy}")});
            this.xrLabel15.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(245F, 109.4167F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(230.2083F, 23F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.Text = "xrLabel15";
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(130.4167F, 109.4167F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(114.5833F, 23F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.Text = "Data rejestracji:";
            // 
            // xrLabel6
            // 
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Address.City", "{0:dd.MM.yyyy}")});
            this.xrLabel6.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(245F, 86.41669F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(230.2083F, 23F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "xrLabel6";
            // 
            // xrLabel5
            // 
            this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Address.Street", "{0:dd.MM.yyyy}")});
            this.xrLabel5.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(245F, 66.41668F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(230.21F, 20F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.Text = "xrLabel5";
            // 
            // xrLabel4
            // 
            this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Birthdate", "{0:dd.MM.yyyy}")});
            this.xrLabel4.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(245F, 43.41669F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(230.2083F, 23F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "xrLabel4";
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(130.4167F, 66.41668F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(114.5833F, 43.00002F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "Miejsce zamieszkania:";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(130.4167F, 43.41669F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(114.5833F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Data urodzenia:";
            // 
            // xrLabel1
            // 
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "FullName")});
            this.xrLabel1.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(130.4167F, 10.00001F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(344.7916F, 33.41667F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "xrLabel1";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(126.42F, 126.54F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.xrPictureBox2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrPictureBox2_BeforePrint);
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(485.6249F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(164.3751F, 108.1217F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // TopMargin
            // 
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 195F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // patientCollection
            // 
            this.patientCollection.ObjectType = typeof(Fabryka.Business.Patient);
            this.patientCollection.ResolveSession += new DevExpress.Xpo.ResolveSessionEventHandler(this.patientCollection_ResolveSession);
            // 
            // PatientID
            // 
            this.PatientID.Description = "PatientID";
            this.PatientID.Name = "PatientID";
            this.PatientID.Type = typeof(int);
            this.PatientID.ValueInfo = "0";
            this.PatientID.Visible = false;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(148.5416F, 32.79168F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.Text = "Plan leczenia:";
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.DetailReport1,
            this.RetentionReport,
            this.DetailReport3});
            this.DetailReport.DataMember = "Treatment";
            this.DetailReport.DataSource = this.patientCollection;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.fakeBraces,
            this.xrRichText3,
            this.xrLabel16,
            this.xrRichText1,
            this.xrLabel7});
            this.Detail1.HeightF = 109.875F;
            this.Detail1.Name = "Detail1";
            this.Detail1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail1_BeforePrint);
            this.Detail1.AfterPrint += new System.EventHandler(this.Detail1_AfterPrint);
            // 
            // fakeBraces
            // 
            this.fakeBraces.CanGrow = false;
            this.fakeBraces.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Rtf", null, "Treatment.Braces")});
            this.fakeBraces.Font = new System.Drawing.Font("Calibri", 12F);
            this.fakeBraces.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.fakeBraces.Name = "fakeBraces";
            this.fakeBraces.SerializableRtfString = resources.GetString("fakeBraces.SerializableRtfString");
            this.fakeBraces.SizeF = new System.Drawing.SizeF(650F, 2F);
            this.fakeBraces.StylePriority.UseFont = false;
            this.fakeBraces.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.fakeBraces_BeforePrint);
            // 
            // xrRichText3
            // 
            this.xrRichText3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Rtf", null, "Treatment.Braces")});
            this.xrRichText3.Font = new System.Drawing.Font("Calibri", 12F);
            this.xrRichText3.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 81.75002F);
            this.xrRichText3.Name = "xrRichText3";
            this.xrRichText3.SerializableRtfString = resources.GetString("xrRichText3.SerializableRtfString");
            this.xrRichText3.SizeF = new System.Drawing.SizeF(650F, 23F);
            this.xrRichText3.StylePriority.UseFont = false;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(0F, 53.54168F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(138.5416F, 28.20833F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.Text = "Aparaty:";
            this.xrLabel16.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel16_BeforePrint);
            // 
            // xrRichText1
            // 
            this.xrRichText1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Rtf", null, "Treatment.Diagnosis")});
            this.xrRichText1.Font = new System.Drawing.Font("Calibri", 12F);
            this.xrRichText1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 30.54167F);
            this.xrRichText1.Name = "xrRichText1";
            this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            this.xrRichText1.SizeF = new System.Drawing.SizeF(650F, 23F);
            this.xrRichText1.StylePriority.UseFont = false;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.333339F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(138.5416F, 28.20833F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.Text = "Diagnoza:";
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailReport2,
            this.Detail2});
            this.DetailReport1.DataMember = "Treatment.Plan";
            this.DetailReport1.DataSource = this.patientCollection;
            this.DetailReport1.Level = 1;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.upperFake,
            this.lowerFake,
            this.xrLabel8});
            this.Detail2.HeightF = 37.12502F;
            this.Detail2.Name = "Detail2";
            // 
            // upperFake
            // 
            this.upperFake.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Rtf", null, "Treatment.Plan.UpperJaw")});
            this.upperFake.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.upperFake.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 34.79168F);
            this.upperFake.Name = "upperFake";
            this.upperFake.SerializableRtfString = resources.GetString("upperFake.SerializableRtfString");
            this.upperFake.SizeF = new System.Drawing.SizeF(630F, 2F);
            this.upperFake.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.upperFake_BeforePrint);
            // 
            // lowerFake
            // 
            this.lowerFake.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Rtf", null, "Treatment.Plan.LowerJaw")});
            this.lowerFake.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.lowerFake.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 32.79168F);
            this.lowerFake.Name = "lowerFake";
            this.lowerFake.SerializableRtfString = resources.GetString("lowerFake.SerializableRtfString");
            this.lowerFake.SizeF = new System.Drawing.SizeF(630F, 2F);
            this.lowerFake.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.upperFake_BeforePrint);
            // 
            // DetailReport2
            // 
            this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.ReportHeader});
            this.DetailReport2.DataMember = "Treatment.Plan.TreatmentPhases";
            this.DetailReport2.DataSource = this.patientCollection;
            this.DetailReport2.Font = new System.Drawing.Font("Calibri Light", 14F);
            this.DetailReport2.Level = 0;
            this.DetailReport2.Name = "DetailReport2";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText2,
            this.xrLabel10});
            this.Detail3.HeightF = 62.66664F;
            this.Detail3.Name = "Detail3";
            // 
            // xrRichText2
            // 
            this.xrRichText2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Rtf", null, "Treatment.Plan.TreatmentPhases.PhasePlan")});
            this.xrRichText2.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrRichText2.LocationFloat = new DevExpress.Utils.PointFloat(20.00001F, 31.33329F);
            this.xrRichText2.Name = "xrRichText2";
            this.xrRichText2.SerializableRtfString = resources.GetString("xrRichText2.SerializableRtfString");
            this.xrRichText2.SizeF = new System.Drawing.SizeF(630F, 23F);
            // 
            // xrLabel10
            // 
            this.xrLabel10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Treatment.Plan.TreatmentPhases.PhaseName", "- {0}")});
            this.xrLabel10.Font = new System.Drawing.Font("Calibri Light", 14F, System.Drawing.FontStyle.Italic);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(20.00001F, 0F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(506.6666F, 31.33333F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.Text = "xrLabel10";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel9});
            this.ReportHeader.HeightF = 29.16667F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(138.5416F, 28.20833F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Fazy aktywne:";
            // 
            // RetentionReport
            // 
            this.RetentionReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.UpperJawReport,
            this.LowerJawReport});
            this.RetentionReport.DataMember = "Treatment.Plan";
            this.RetentionReport.DataSource = this.patientCollection;
            this.RetentionReport.Level = 2;
            this.RetentionReport.Name = "RetentionReport";
            this.RetentionReport.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.RetentionReport_BeforePrint);
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11});
            this.Detail4.HeightF = 28.20833F;
            this.Detail4.Name = "Detail4";
            this.Detail4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail4_BeforePrint);
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Bold);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(138.5416F, 28.20833F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.Text = "Fazy retencji:";
            // 
            // UpperJawReport
            // 
            this.UpperJawReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail5});
            this.UpperJawReport.Level = 0;
            this.UpperJawReport.Name = "UpperJawReport";
            this.UpperJawReport.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.UpperJawReport_BeforePrint);
            // 
            // Detail5
            // 
            this.Detail5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrUpperJaw,
            this.xrLabel13});
            this.Detail5.HeightF = 67.8751F;
            this.Detail5.Name = "Detail5";
            // 
            // xrUpperJaw
            // 
            this.xrUpperJaw.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Rtf", null, "Treatment.Plan.UpperJaw")});
            this.xrUpperJaw.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrUpperJaw.LocationFloat = new DevExpress.Utils.PointFloat(20.00001F, 31.33332F);
            this.xrUpperJaw.Name = "xrUpperJaw";
            this.xrUpperJaw.SerializableRtfString = resources.GetString("xrUpperJaw.SerializableRtfString");
            this.xrUpperJaw.SizeF = new System.Drawing.SizeF(630F, 23F);
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Calibri Light", 14F, System.Drawing.FontStyle.Italic);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(20.00001F, 0F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(506.6666F, 31.33333F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.Text = "- Szczęka:";
            // 
            // LowerJawReport
            // 
            this.LowerJawReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail6});
            this.LowerJawReport.Level = 1;
            this.LowerJawReport.Name = "LowerJawReport";
            this.LowerJawReport.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.LowerJawReport_BeforePrint);
            // 
            // Detail6
            // 
            this.Detail6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLowerJaw,
            this.xrLabel12});
            this.Detail6.HeightF = 63.54167F;
            this.Detail6.Name = "Detail6";
            // 
            // xrLowerJaw
            // 
            this.xrLowerJaw.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Rtf", null, "Treatment.Plan.LowerJaw")});
            this.xrLowerJaw.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLowerJaw.LocationFloat = new DevExpress.Utils.PointFloat(20.00001F, 31.33341F);
            this.xrLowerJaw.Name = "xrLowerJaw";
            this.xrLowerJaw.SerializableRtfString = resources.GetString("xrLowerJaw.SerializableRtfString");
            this.xrLowerJaw.SizeF = new System.Drawing.SizeF(630F, 23F);
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Calibri Light", 14F, System.Drawing.FontStyle.Italic);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(20.00001F, 0F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(506.6666F, 31.33333F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.Text = "- Żuchwa:";
            // 
            // DetailReport3
            // 
            this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail7});
            this.DetailReport3.Level = 0;
            this.DetailReport3.Name = "DetailReport3";
            // 
            // Detail7
            // 
            this.Detail7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail7.HeightF = 131.25F;
            this.Detail7.Name = "Detail7";
            this.Detail7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail7_BeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5});
            this.xrTable1.SizeF = new System.Drawing.SizeF(649.9999F, 125F);
            this.xrTable1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable1_BeforePrint);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tonn1,
            this.bolton1,
            this.littl1,
            this.kork1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // tonn1
            // 
            this.tonn1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tonn1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.tonn1.Name = "tonn1";
            this.tonn1.StylePriority.UseBorders = false;
            this.tonn1.StylePriority.UseFont = false;
            this.tonn1.Text = "Wskaźnik Tonn\'a";
            this.tonn1.Weight = 0.75961524466789787D;
            // 
            // bolton1
            // 
            this.bolton1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.bolton1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.bolton1.Name = "bolton1";
            this.bolton1.StylePriority.UseBorders = false;
            this.bolton1.StylePriority.UseFont = false;
            this.bolton1.Text = "Wskaźnik Boltona";
            this.bolton1.Weight = 0.78846157728567989D;
            // 
            // littl1
            // 
            this.littl1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.littl1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.littl1.Name = "littl1";
            this.littl1.StylePriority.UseBorders = false;
            this.littl1.StylePriority.UseFont = false;
            this.littl1.Text = "Wskaźnik Littla";
            this.littl1.Weight = 0.76442301958975167D;
            // 
            // kork1
            // 
            this.kork1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.kork1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.kork1.Name = "kork1";
            this.kork1.StylePriority.UseBorders = false;
            this.kork1.StylePriority.UseFont = false;
            this.kork1.Text = "Wskaźnik Korkhausa";
            this.kork1.Weight = 0.68750015845667045D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tonn2,
            this.bolton2,
            this.littl2,
            this.kork2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // tonn2
            // 
            this.tonn2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.tonn2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuickResults.Tonn", "{0} %")});
            this.tonn2.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.tonn2.Name = "tonn2";
            this.tonn2.StylePriority.UseBorders = false;
            this.tonn2.StylePriority.UseFont = false;
            this.tonn2.Text = "tonn2";
            this.tonn2.Weight = 0.75961531509308478D;
            // 
            // bolton2
            // 
            this.bolton2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.bolton2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuickResults.BoltonOverall", "overall ratio: {0} %")});
            this.bolton2.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.bolton2.Name = "bolton2";
            this.bolton2.StylePriority.UseBorders = false;
            this.bolton2.StylePriority.UseFont = false;
            this.bolton2.Text = "bolton2";
            this.bolton2.Weight = 0.78846147164789959D;
            // 
            // littl2
            // 
            this.littl2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.littl2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuickResults.Little", "{0} mm")});
            this.littl2.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.littl2.Name = "littl2";
            this.littl2.StylePriority.UseBorders = false;
            this.littl2.StylePriority.UseFont = false;
            this.littl2.Text = "littl2";
            this.littl2.Weight = 0.764423125227532D;
            // 
            // kork2
            // 
            this.kork2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.kork2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuickResults.KorkhausIwp", "Iwp {0} %")});
            this.kork2.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.kork2.Name = "kork2";
            this.kork2.StylePriority.UseBorders = false;
            this.kork2.StylePriority.UseFont = false;
            this.kork2.Text = "kork2";
            this.kork2.Weight = 0.68750008803148366D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tonn3,
            this.bolton3,
            this.littl3,
            this.kork3});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // tonn3
            // 
            this.tonn3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.tonn3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuickResults.TonnOver", "Nadmiar: {0} mm")});
            this.tonn3.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.tonn3.Name = "tonn3";
            this.tonn3.StylePriority.UseBorders = false;
            this.tonn3.StylePriority.UseFont = false;
            this.tonn3.Text = "tonn3";
            this.tonn3.Weight = 0.75961537378074051D;
            // 
            // bolton3
            // 
            this.bolton3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.bolton3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuickResults.BoltonAnterior", "anterior ratio: {0} %")});
            this.bolton3.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.bolton3.Name = "bolton3";
            this.bolton3.StylePriority.UseBorders = false;
            this.bolton3.StylePriority.UseFont = false;
            this.bolton3.Text = "bolton3";
            this.bolton3.Weight = 0.78846141296024386D;
            // 
            // littl3
            // 
            this.littl3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.littl3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuickResults.LittleSize", "słoczenie {0}")});
            this.littl3.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.littl3.Name = "littl3";
            this.littl3.StylePriority.UseBorders = false;
            this.littl3.StylePriority.UseFont = false;
            this.littl3.Text = "littl3";
            this.littl3.Weight = 0.764423125227532D;
            // 
            // kork3
            // 
            this.kork3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.kork3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuickResults.KorkhausComment")});
            this.kork3.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.kork3.Name = "kork3";
            this.kork3.StylePriority.UseBorders = false;
            this.kork3.StylePriority.UseFont = false;
            this.kork3.Text = "[QuickResults.KorkhausComment]";
            this.kork3.Weight = 0.68750008803148366D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tonn4,
            this.bolton4,
            this.littl4,
            this.kork4});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // tonn4
            // 
            this.tonn4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.tonn4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuickResults.TonnWhere", "w {0}")});
            this.tonn4.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.tonn4.Name = "tonn4";
            this.tonn4.StylePriority.UseBorders = false;
            this.tonn4.StylePriority.UseFont = false;
            this.tonn4.Text = "tonn4";
            this.tonn4.Weight = 0.75961537378074051D;
            // 
            // bolton4
            // 
            this.bolton4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.bolton4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuickResults.BoltonOver", "Nadmiar: {0} mm")});
            this.bolton4.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.bolton4.Name = "bolton4";
            this.bolton4.StylePriority.UseBorders = false;
            this.bolton4.StylePriority.UseFont = false;
            this.bolton4.Text = "bolton4";
            this.bolton4.Weight = 0.78846141296024386D;
            // 
            // littl4
            // 
            this.littl4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.littl4.Name = "littl4";
            this.littl4.StylePriority.UseBorders = false;
            this.littl4.Weight = 0.764423125227532D;
            // 
            // kork4
            // 
            this.kork4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.kork4.Name = "kork4";
            this.kork4.StylePriority.UseBorders = false;
            this.kork4.Weight = 0.68750008803148366D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tonn5,
            this.bolton5,
            this.littl5,
            this.kork5});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // tonn5
            // 
            this.tonn5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.tonn5.Name = "tonn5";
            this.tonn5.StylePriority.UseBorders = false;
            this.tonn5.Weight = 0.75961537378074051D;
            // 
            // bolton5
            // 
            this.bolton5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.bolton5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "QuickResults.BoltonWhere", "w {0}")});
            this.bolton5.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.bolton5.Name = "bolton5";
            this.bolton5.StylePriority.UseBorders = false;
            this.bolton5.StylePriority.UseFont = false;
            this.bolton5.Text = "bolton5";
            this.bolton5.Weight = 0.78846141296024386D;
            // 
            // littl5
            // 
            this.littl5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.littl5.Name = "littl5";
            this.littl5.StylePriority.UseBorders = false;
            this.littl5.Weight = 0.764423125227532D;
            // 
            // kork5
            // 
            this.kork5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.kork5.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.kork5.Name = "kork5";
            this.kork5.StylePriority.UseBorders = false;
            this.kork5.StylePriority.UseFont = false;
            this.kork5.Weight = 0.68750008803148366D;
            // 
            // hasJawOrUpper
            // 
            this.hasJawOrUpper.Condition = "IsNullOrEmpty([Treatment.Plan.UpperJaw]) And IsNullOrEmpty([Treatment.Plan.LowerJ" +
                "aw])";
            // 
            // 
            // 
            this.hasJawOrUpper.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.hasJawOrUpper.Name = "hasJawOrUpper";
            // 
            // hasJaw
            // 
            this.hasJaw.Condition = "IsNullOrEmpty([Treatment.Plan.LowerJaw])";
            // 
            // 
            // 
            this.hasJaw.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.hasJaw.Name = "hasJaw";
            // 
            // hasUpper
            // 
            this.hasUpper.Condition = "IsNullOrEmpty([Treatment.Plan.UpperJaw])";
            // 
            // 
            // 
            this.hasUpper.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.hasUpper.Name = "hasUpper";
            // 
            // DetailReport4
            // 
            this.DetailReport4.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail8,
            this.GroupHeader1});
            this.DetailReport4.DataMember = "Visits";
            this.DetailReport4.DataSource = this.patientCollection;
            this.DetailReport4.Level = 1;
            this.DetailReport4.Name = "DetailReport4";
            // 
            // Detail8
            // 
            this.Detail8.Name = "Detail8";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // PatientReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.DetailReport4,
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport});
            this.DataSource = this.patientCollection;
            this.FilterString = "[Oid] = ?PatientID";
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.hasJawOrUpper,
            this.hasJaw,
            this.hasUpper});
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 100, 195);
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.PatientID});
            this.Version = "12.2";
            ((System.ComponentModel.ISupportInitialize)(this.patientCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fakeBraces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upperFake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowerFake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrUpperJaw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrLowerJaw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.Xpo.XPCollection patientCollection;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        public DevExpress.XtraReports.Parameters.Parameter PatientID;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRRichText xrRichText1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRRichText xrRichText2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.DetailReportBand RetentionReport;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.DetailReportBand UpperJawReport;
        private DevExpress.XtraReports.UI.DetailBand Detail5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.FormattingRule hasJaw;
        private DevExpress.XtraReports.UI.DetailReportBand LowerJawReport;
        private DevExpress.XtraReports.UI.DetailBand Detail6;
        private DevExpress.XtraReports.UI.XRRichText xrLowerJaw;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.FormattingRule hasJawOrUpper;
        private DevExpress.XtraReports.UI.FormattingRule hasUpper;
        private DevExpress.XtraReports.UI.XRRichText upperFake;
        private DevExpress.XtraReports.UI.XRRichText lowerFake;
        private DevExpress.XtraReports.UI.XRRichText xrUpperJaw;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell tonn1;
        private DevExpress.XtraReports.UI.XRTableCell bolton1;
        private DevExpress.XtraReports.UI.XRTableCell littl1;
        private DevExpress.XtraReports.UI.XRTableCell kork1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell tonn2;
        private DevExpress.XtraReports.UI.XRTableCell bolton2;
        private DevExpress.XtraReports.UI.XRTableCell littl2;
        private DevExpress.XtraReports.UI.XRTableCell kork2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell tonn3;
        private DevExpress.XtraReports.UI.XRTableCell bolton3;
        private DevExpress.XtraReports.UI.XRTableCell littl3;
        private DevExpress.XtraReports.UI.XRTableCell kork3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell tonn4;
        private DevExpress.XtraReports.UI.XRTableCell bolton4;
        private DevExpress.XtraReports.UI.XRTableCell littl4;
        private DevExpress.XtraReports.UI.XRTableCell kork4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell tonn5;
        private DevExpress.XtraReports.UI.XRTableCell bolton5;
        private DevExpress.XtraReports.UI.XRTableCell littl5;
        private DevExpress.XtraReports.UI.XRTableCell kork5;
        private DevExpress.XtraReports.UI.XRRichText xrRichText3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRRichText fakeBraces;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport3;
        private DevExpress.XtraReports.UI.DetailBand Detail7;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport4;
        private DevExpress.XtraReports.UI.DetailBand Detail8;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
    }
}
