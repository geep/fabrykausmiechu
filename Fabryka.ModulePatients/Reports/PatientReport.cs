﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Windows.Media.Imaging;
using System.IO;
using Fabryka.Common.Services;
using Microsoft.Practices.ServiceLocation;

namespace Fabryka.ModulePatient.Reports
{
    public partial class PatientReport : DevExpress.XtraReports.UI.XtraReport
    {
        public PatientReport()
        {
            InitializeComponent();
        }

        private void xrPictureBox2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRPictureBox pb = (XRPictureBox)sender;
            var mainPhoto = Report.GetCurrentColumnValue<Business.Photo>("MainPhoto!");

            if (mainPhoto == null)
                return;

            pb.Image = BitmapImage2Bitmap((BitmapImage)mainPhoto.Thumbnail);
        }

        private static Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            // BitmapImage bitmapImage = new BitmapImage(new Uri("../Images/test.png", UriKind.Relative));

            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(outStream);

                // return bitmap; <-- leads to problems, stream is closed/closing ...
                return new Bitmap(bitmap);
            }
        }

        private void patientCollection_ResolveSession(object sender, DevExpress.Xpo.ResolveSessionEventArgs e)
        {
            var dataService = ServiceLocator.Current.GetInstance<IDataService>();
            e.Session = dataService.GetCurrentSession();
        }

        private void LowerJawReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            e.Cancel = lowerFake.Text.Length == 0;
        }

        private void UpperJawReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            e.Cancel = upperFake.Text.Length == 0;
        }

        private void RetentionReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            e.Cancel = upperFake.Text.Length == 0 && lowerFake.Text.Length == 0;
        }

        private void upperFake_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            e.Cancel = true;
        }

        private void Detail4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrTable1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var res = Report.GetCurrentColumnValue<Business.QuickAnalysisResults>("QuickResults!");

            if (res == null)
                throw new NullReferenceException("No quick result defined");

            if (!res.BoltonIsSet && !res.TonnIsSet && !res.LittleIsSet && !res.KorkhausIsSet)
            {
                xrTable1.Visible = false;
                xrTableRow2.Visible = false;
                xrTableRow3.Visible = false;
                xrTableRow4.Visible = false;
                xrTableRow5.Visible = false;
                xrTableRow1.Visible = false;
                return;
            }

           if (!res.BoltonIsSet)
           {
               xrTableRow1.Cells.Remove(bolton1);
               xrTableRow2.Cells.Remove(bolton2);
               xrTableRow3.Cells.Remove(bolton3);
               xrTableRow4.Cells.Remove(bolton4);
               xrTableRow5.Cells.Remove(bolton5);
           }

           if (!res.TonnIsSet)
           {
               xrTableRow1.Cells.Remove(tonn1);
               xrTableRow2.Cells.Remove(tonn2);
               xrTableRow3.Cells.Remove(tonn3);
               xrTableRow4.Cells.Remove(tonn4);
               xrTableRow5.Cells.Remove(tonn5);
           }

           if (!res.KorkhausIsSet)
           {
               xrTableRow1.Cells.Remove(kork1);
               xrTableRow2.Cells.Remove(kork2);
               xrTableRow3.Cells.Remove(kork3);
               xrTableRow4.Cells.Remove(kork4);
               xrTableRow5.Cells.Remove(kork5);
           }

           if (!res.LittleIsSet)
           {
               xrTableRow1.Cells.Remove(littl1);
               xrTableRow2.Cells.Remove(littl2);
               xrTableRow3.Cells.Remove(littl3);
               xrTableRow4.Cells.Remove(littl4);
               xrTableRow5.Cells.Remove(littl5);
           }


        }

        private void xrLabel16_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (fakeBraces.Text.Length == 0)
                xrLabel16.Visible = false;
        }

        private void fakeBraces_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            fakeBraces.Visible = false;
        }

        private void Detail1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void Detail1_AfterPrint(object sender, EventArgs e)
        {
            e.ToString();
        }

        private void Detail7_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var res = Report.GetCurrentColumnValue<Business.QuickAnalysisResults>("QuickResults!");

            if (res == null)
                throw new NullReferenceException("No quick result defined");

            if (!res.BoltonIsSet && !res.TonnIsSet && !res.LittleIsSet && !res.KorkhausIsSet)
            {
                Detail7.Visible = false;
                return;
            }
        }

    }
}
