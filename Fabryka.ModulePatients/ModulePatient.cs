﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using Microsoft.Practices.Prism.Regions;

namespace Fabryka.ModulePatient
{
    [ModuleExport(typeof(ModulePatient), InitializationMode = InitializationMode.WhenAvailable)]
    public class ModulePatient : IModule
    {
        private readonly IRegionViewRegistry regionViewRegistry;

        [ImportingConstructor]
        public ModulePatient(IRegionViewRegistry registry)
        {
            regionViewRegistry = registry;
        }

        public void Initialize()
        {
            regionViewRegistry.RegisterViewWithRegion("MainMenuButtonsRegion", typeof(Menu.PatientButtonView));
            regionViewRegistry.RegisterViewWithRegion("SettingsRegion", typeof(Settings.TreatmentRTFTemplatesView));
            //regionViewRegistry.RegisterViewWithRegion("MainRegion", typeof(PatientData.PatientDataView));
        }
    }
}
