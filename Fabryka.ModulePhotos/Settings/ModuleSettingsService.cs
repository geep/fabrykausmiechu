﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fabryka.Common.Services;
using System.Xml.Serialization;
using System.IO;
using Microsoft.Practices.ServiceLocation;
using System.ComponentModel.Composition;
using Fabryka.Common;

namespace Fabryka.ModulePhotos.Settings
{
    [Export(typeof(IModulesSettingsService)), Export(typeof(ModuleSettingsService))]
    public class ModuleSettingsService : IModulesSettingsService
    {
        public class SettingsSerialized
        {
            public PhotoComparisonList ComparisonList;
        }

        public string SettingsName
        {
            get { return "Photos"; }
        }

        public string GetSettingsAsXml()
        {
            XmlSerializer xs = new XmlSerializer(typeof(SettingsSerialized));
            StringWriter sw = new StringWriter();

            var ds = Fabryka.ModulePhotos.Properties.Settings.Default;

            var ss = new SettingsSerialized()
            {
                ComparisonList = ds.PhotoComparisonList
            };

            xs.Serialize(sw, ss);

            return sw.ToString();
        }

        public void SetSettingsAsXml(string xml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(SettingsSerialized));
            StringReader sr = new StringReader(xml);

            SettingsSerialized ss = (SettingsSerialized) xs.Deserialize(sr);

            var ds = Fabryka.ModulePhotos.Properties.Settings.Default;

            ds.PhotoComparisonList = ss.ComparisonList;
            ds.Save();
        }

        public void NotifyChanged()
        {
            ServiceLocator.Current.GetInstance<IGlobalSettingsService>().SettingsChanged(this);
        }
    }


}
