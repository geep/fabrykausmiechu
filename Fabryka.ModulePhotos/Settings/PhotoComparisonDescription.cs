﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Fabryka.ModulePhotos.Settings
{

    public class PhotoComparisonDescription
    {
        [UserScopedSetting()]
        [SettingsSerializeAs(System.Configuration.SettingsSerializeAs.Xml)]
        public string PhotoComparisonName { get; set; }

        public override string ToString()
        {
            return PhotoComparisonName;
        }
    }

    public class PhotoComparisonList : List<PhotoComparisonDescription>
    {
    }
}
