﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.ViewModel;
using System.Collections;
using System.Windows.Documents;
using Microsoft.Practices.Prism.Commands;

namespace Fabryka.ModulePhotos.Settings
{
    [Export]
    public class PhotosSettingsViewModel : NotificationObject
    {
        public string ViewName
        {
            get
            {
                return "Zdjęcia";
            }
        }

        // Fields...
        public IEnumerable<PhotoComparisonDescription> PhotoComparisonList
        {
            get
            {
                return Properties.Settings.Default.PhotoComparisonList;
            }
        }

        public DelegateCommand SaveCommand { get; private set; }
        public DelegateCommand CancelCommand { get; private set; }

        public PhotosSettingsViewModel()
        {
            Properties.Settings.Default.PropertyChanged += Default_PropertyChanged;
            SaveCommand = new DelegateCommand(
                () =>
                {
                    Properties.Settings.Default.NotifySettingsChanging();
                    Properties.Settings.Default.Save();
                }
            );
            CancelCommand = new DelegateCommand(
                () =>
                { 
                    Properties.Settings.Default.Reload(); 
                    RaisePropertyChanged(() => PhotoComparisonList); 
                });
        }

        void Default_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "PhotoComparisonList")
            {
                RaisePropertyChanged(() => PhotoComparisonList);
            }            
        }
    }
}
