﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.Composition;
using Fabryka.CommonWPF.Menu;

namespace Fabryka.ModulePhotos.Menu
{
    [Export]
    public class PhotosButtonViewModel : MainMenuButtonViewModel
    {
        public PhotosButtonViewModel()
        {
            UriName = "PatientPhotosView";
            SubMenuUriName = "PhotosSubMenuView";
        }
    }
}
