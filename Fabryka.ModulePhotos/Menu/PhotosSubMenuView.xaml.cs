﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;

namespace Fabryka.ModulePhotos.Menu
{
    /// <summary>
    /// Interaction logic for PatientSubMenuView.xaml
    /// </summary>
    [Export("PhotosSubMenuView", typeof(PhotosSubMenuView))]
    public partial class PhotosSubMenuView : UserControl
    {
        public PhotosSubMenuView()
        {
            InitializeComponent();
        }

        [Import]
        public PhotosSubMenuViewModel ViewModel
        {
            set
            {
                DataContext = value;
            }
        }
    }
}
