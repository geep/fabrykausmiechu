﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.Composition;
using Fabryka.Common.Services;
using Microsoft.Practices.Prism.Commands;
using Fabryka.Business;
using Fabryka.ModulePhotos.PatientPhotos;
using System.Windows.Media.Imaging;
using System.IO;

namespace Fabryka.ModulePhotos.Menu
{
    [Export]
    public class PhotosSubMenuViewModel
    {
        private readonly ISelectedPatientService selectedService;
        private readonly IDataService dataService;
        public PatientPhotosViewModel patientPhotosViewModel { get; private set; }

        public DelegateCommand AddPhotos { get; private set; }

        [ImportingConstructor]
        public PhotosSubMenuViewModel(ISelectedPatientService _selectedService, IDataService _dataService, PatientPhotosViewModel _patientPhotosViewModel)
        {
            selectedService = _selectedService;
            patientPhotosViewModel = _patientPhotosViewModel;
            dataService = _dataService;

            AddPhotos = new DelegateCommand(
                () => AddPhotosCommand(),
                () => selectedService.SelectedPatient != null
            );

            selectedService.SelectionChanged += selectedService_SelectionChanged;
            
        }

        void selectedService_SelectionChanged(object sender, EventArgs e)
        {
            AddPhotos.RaiseCanExecuteChanged();
        }

        private void AddPhotosCommand()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = "*.jpg";
            dlg.Filter = "Zdjecia jpg|*.jpg";
            dlg.Multiselect = true;

            Nullable<bool> result = dlg.ShowDialog();

            try
            {
                if (result == true)
                {
                    var patient = selectedService.SelectedPatient;
                    foreach (var filename in dlg.FileNames)
                    {
                        if (File.Exists(filename))
                        {


                            Photo newPhoto = dataService.CreatePhotoFromFile(patient, filename);

                            //patientPhotosViewModel.PhotosChanged();
                        }
                    }

                    if (patient.Photos.Count != dataService.GetPhotoCount(patient))
                    {
                        System.Windows.MessageBox.Show("Uwaga! Prawdopodobnie nie dodały się wszystkie zdjęcia.");
                    }

                }
            }
            catch (System.OutOfMemoryException)
            {
                System.Windows.MessageBox.Show("Uwaga! Prawdopodobnie nie dodały się wszystkie zdjęcia.");
            }
        }
    }
}
