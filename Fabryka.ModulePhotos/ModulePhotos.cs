﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using System.ComponentModel.Composition;

namespace Fabryka.ModulePhotos
{
    [ModuleExport(typeof(ModulePhotos), InitializationMode = InitializationMode.WhenAvailable)]
    public class ModulePhotos : IModule
    {
        private readonly IRegionViewRegistry regionViewRegistry;

        [ImportingConstructor]
        public ModulePhotos(IRegionViewRegistry registry)
        {
            regionViewRegistry = registry;
        }

        public void Initialize()
        {
            regionViewRegistry.RegisterViewWithRegion("MainMenuButtonsRegion", typeof(Menu.PhotosButtonView));
            regionViewRegistry.RegisterViewWithRegion("SettingsRegion", typeof(Settings.PhotosSettingsView));
        }
    }
}
