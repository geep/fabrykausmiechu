﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.Regions;
using System.Text.RegularExpressions;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Core.Native;
using Fabryka.Business;
using System.Windows.Media.Effects;
using ControlsDemo.GalleryDemo;

namespace Fabryka.ModulePhotos.PatientPhotos
{
    /// <summary>
    /// Interaction logic for PatientPhotosView.xaml
    /// </summary>
    [Export("PatientPhotosView", typeof(PatientPhotosView))]
    public partial class PatientPhotosView : UserControl, IRegionMemberLifetime
    {
        public PatientPhotosView()
        {
            InitializeComponent();
        }

        private PatientPhotosViewModel vm;

        private bool _isMouseDown;
        private Photo _dragged;
        private Point _dragStartPosition;
        private bool _isDragging;

        [Import]
        public PatientPhotosViewModel ViewModel
        {
            set
            {
                DataContext = value;
                value.PropertyChanged += value_PropertyChanged;
                vm = value;
            }
        }

        void value_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Photos")
            {
                group1.ItemsSource = null;
                group1.ItemsSource = vm.Photos;
            }
            else
                if (e.PropertyName == "SelectedPatient")
                {
                    CloseImageViewPopup();
                }
        }

        public bool KeepAlive
        {
            get { return true; }
        }

        private void galleryControl1_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            HitTestResult res = VisualTreeHelper.HitTest((GalleryControl)sender, e.GetPosition((GalleryControl)sender));

            if (res.VisualHit == null)
                return;

            GalleryItemControl galleryItemControl = LayoutHelper.FindParentObject<GalleryItemControl>(res.VisualHit);

            if (galleryItemControl == null)
                return;


            PopupMenu menu = new PopupMenu();
            vm.SelectedPhoto = galleryItemControl.Item.Tag as Photo;
            menu.ItemLinks.Add(photoDelete);
            menu.ItemLinks.Add(photoSetAsMain);
            menu.ItemLinks.Add(photoChangeImportant);
            photoChangeImportant.Content = (vm).ImportantPhotoDescription;
            menu.Manager = barManager1;

            var p = e.GetPosition(galleryItemControl);
            menu.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;



            menu.ShowPopup(galleryItemControl);
        }

        private void SaveRotation()
        {
            if (imageViewer.Rotation == Rotation.Rotate0)
                return;

            vm.RotatePhoto(currentPhoto, imageViewer.Rotation);
        }

        private void ControlPanel_CommandClick(object sender, ControlPanelEventArgs e) {

            switch(e.Command) {
                case ControlPanelCommand.ZoomValueChanged:
                    imageViewer.ScaleCenter(controlPanel.ZoomValue / 100);
                    break;
                case ControlPanelCommand.RotateLeft:
                    RotateCounterclockwise();
                    break;
                case ControlPanelCommand.RotateRight:
                    RotateClockwise();
                    break;
                case ControlPanelCommand.ZoomToOriginalSize:
                    controlPanel.SetAndAnimateZoomValue(100);
                    break;
                case ControlPanelCommand.VerSize:
                    controlPanel.SetAndAnimateZoomValue(imageViewer.VerticalFitScale * 100);
                    break;
                case ControlPanelCommand.HorSize:
                    controlPanel.SetAndAnimateZoomValue(imageViewer.HorizontalFitScale * 100);
                    break;
                case ControlPanelCommand.AutoSize:
                    controlPanel.SetAndAnimateZoomValue(imageViewer.GetBestFitScale() * 100);
                    break;
                case ControlPanelCommand.Next:
                    if (imageViewer.Tag is GalleryItem)
                        ShowItemInImageVewer(GetNextItem((GalleryItem)imageViewer.Tag));
                    break;
                case ControlPanelCommand.Prior:
                    if (imageViewer.Tag is GalleryItem)
                        ShowItemInImageVewer(GetPriorItem((GalleryItem)imageViewer.Tag));
                    break;
                case ControlPanelCommand.Print:
                    PrintCurrentImage();
                    break;
                case ControlPanelCommand.Save:
                    vm.SavePhoto(currentPhoto);
                    break;
                case ControlPanelCommand.SaveRotation:
                    SaveRotation();
                    break;
            }
        }

        private void RotateClockwise() {
            switch(imageViewer.Rotation) {
                case Rotation.Rotate0:
                    imageViewer.Rotation = Rotation.Rotate90;
                    break;
                case Rotation.Rotate90:
                    imageViewer.Rotation = Rotation.Rotate180;
                    break;
                case Rotation.Rotate180:
                    imageViewer.Rotation = Rotation.Rotate270;
                    break;
                case Rotation.Rotate270:
                    imageViewer.Rotation = Rotation.Rotate0;
                    break;
            }
        }
        private void RotateCounterclockwise() {
            switch(imageViewer.Rotation) {
                case Rotation.Rotate0:
                    imageViewer.Rotation = Rotation.Rotate270;
                    break;
                case Rotation.Rotate90:
                    imageViewer.Rotation = Rotation.Rotate0;
                    break;
                case Rotation.Rotate180:
                    imageViewer.Rotation = Rotation.Rotate90;
                    break;
                case Rotation.Rotate270:
                    imageViewer.Rotation = Rotation.Rotate180;
                    break;
            }
        }
        GalleryItem GetNextItem(GalleryItem item) {
            if(item.Group.Items[item.Group.Items.Count - 1] != item) {
                return item.Group.Items[item.Group.Items.IndexOf(item) + 1];
            }
            int groupIndex = item.Group.Gallery.Groups.IndexOf(item.Group);
            if(groupIndex != item.Group.Gallery.Groups.Count - 1)
                groupIndex++;
            else
                groupIndex = 0;
            return item.Group.Gallery.Groups[groupIndex].Items[0];
        }
        GalleryItem GetPriorItem(GalleryItem item) {
            if(item.Group.Items[0] != item) {
                return item.Group.Items[item.Group.Items.IndexOf(item) - 1];
            }
            int groupIndex = item.Group.Gallery.Groups.IndexOf(item.Group);
            if(groupIndex != 0)
                groupIndex--;
            else
                groupIndex = item.Group.Gallery.Groups.Count - 1;
            return item.Group.Gallery.Groups[groupIndex].Items[item.Group.Gallery.Groups[groupIndex].Items.Count - 1];
        }
        private void bntCloseImageViewer_Click(object sender, RoutedEventArgs e) {
            CloseImageViewPopup();
        }
        void CloseImageViewPopup() {
            //mainView.IsEnabled = true;
            mainView.Effect = null;
            imageViewPopup.Visibility = Visibility.Collapsed;
        }

        private void Gallery_ItemClick(object sender, DevExpress.Xpf.Bars.GalleryItemEventArgs e) {
            OpenImageViewPopup(e.Item);
        }
        void OpenImageViewPopup(GalleryItem item) {
            ShowItemInImageVewer(item);
            mainView.Effect = new BlurEffect() { Radius = 3 };
            //mainView.IsEnabled = false;
            imageViewPopup.Visibility = Visibility.Visible;
            imageViewPopup.Focus();
        }

        private Photo currentPhoto = null;

        private void ShowItemInImageVewer(GalleryItem item)
        {
            Photo photo = (Photo)item.Tag;
            currentPhoto = photo;
            imageViewer.Tag = item;
            ShowPhotoInImageViewer(photo);
        }

        private void ShowPhotoInImageViewer(Photo photo)
        {
            imageViewerTitle.Text = photo.Name;
            imageViewer.ImageSource = (BitmapSource)photo.Picture;
            imageViewer.Rotation = Rotation.Rotate0;
            imageViewer.LayoutUpdated += new EventHandler(imageViewer_LayoutUpdated);
            FitImageInViewport();
            imageViewPopup.Focus();
        }
        void imageViewer_LayoutUpdated(object sender, EventArgs e)
        {
            FitImageInViewport();
            imageViewer.LayoutUpdated -= new EventHandler(imageViewer_LayoutUpdated);
        }
        void FitImageInViewport() {
            if(imageViewer.ImageSource == null) {
                controlPanel.ZoomValue = 100;
                return;
            }
            double scaleWidth = Math.Min(1.0, (imageViewer.Viewport.ActualWidth - 20) / imageViewer.ImageSource.PixelWidth);
            double scaleHeight = Math.Min(1.0, (imageViewer.Viewport.ActualHeight - 20) / imageViewer.ImageSource.PixelHeight);
            controlPanel.ZoomValue = 100 * Math.Min(scaleWidth, scaleHeight);
        }

        private void imageViewer_MouseWheelZoom(object sender, EventArgs e) {
            controlPanel.ZoomValue = imageViewer.Scale * 100;
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e) {
            base.OnMouseLeftButtonDown(e);
        }
        protected override void OnMouseRightButtonDown(MouseButtonEventArgs e) {
            base.OnMouseRightButtonDown(e);
        }

        public void PrintCurrentImage() {
            PrintDialog printDialog = new PrintDialog();
            if(printDialog.ShowDialog() == true) {
                Photo photoInfo;

                if (imageViewer.Tag is GalleryItem)
                {
                    photoInfo = (Photo)((GalleryItem)imageViewer.Tag).Tag;
                }
                else
                {
                    photoInfo = (Photo) imageViewer.Tag;
                }

                Image imageViewer1 = new Image() { Source = photoInfo.Picture as BitmapImage };
                
                imageViewer1.Measure(new Size(printDialog.PrintableAreaWidth, printDialog.PrintableAreaHeight));
                imageViewer1.Arrange(new Rect(new Point(0, 0), imageViewer1.DesiredSize));

                //Image img = new Image() { Source = photoInfo.Picture as BitmapImage };
                printDialog.PrintVisual(imageViewer1, photoInfo.Name);
            }
        }
        protected override void OnKeyDown(KeyEventArgs e) {
            base.OnKeyDown(e);
            if (imageViewPopup.Visibility != Visibility.Visible)
            {
                return;
            }

           /* if (e.Key == Key.Escape)
            {
                CloseImageViewPopup();
            }
            else if (e.Key == Key.Left)
            {
                ShowItemInImageVewer(GetPriorItem((GalleryItem)imageViewer.Tag));
            }
            else if (e.Key == Key.Right)
            {
                ShowItemInImageVewer(GetNextItem((GalleryItem)imageViewer.Tag));
            }   */
        }

        private void imageViewPopup_KeyUp(object sender, KeyEventArgs e)
        {
            if (imageViewPopup.Visibility != Visibility.Visible)
            {
                return;
            }

            if (e.Key == Key.Escape)
            {
                    CloseImageViewPopup();
            }
            else if (e.Key == Key.Left)
            {
                if (imageViewer.Tag is GalleryItem)
                    ShowItemInImageVewer(GetPriorItem((GalleryItem)imageViewer.Tag));
            }
            else if (e.Key == Key.Right)
            {
                if (imageViewer.Tag is GalleryItem)
                    ShowItemInImageVewer(GetNextItem((GalleryItem)imageViewer.Tag));
            }
            e.Handled = true;
        }

        void galleryItem_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ResetState();
        }

        void galleryItem_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (_isMouseDown)
            {
                GalleryControl itemsControl = (GalleryControl)sender;
                Point currentPosition = e.GetPosition(itemsControl);
                if ((_isDragging == false) && (Math.Abs(currentPosition.X - _dragStartPosition.X) > SystemParameters.MinimumHorizontalDragDistance) ||
                    (Math.Abs(currentPosition.Y - _dragStartPosition.Y) > SystemParameters.MinimumVerticalDragDistance))
                {
                    DragStarted(itemsControl);
                }
            }
        }

        private void DragStarted(GalleryControl itemsControl)
        {
            _isDragging = true;
            DataObject dObject = new DataObject(typeof(Photo), _dragged);
            DragDropEffects e = DragDrop.DoDragDrop(itemsControl, dObject, DragDropEffects.Link);
            ResetState();
        }

        void galleryItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            GalleryControl galControl = (GalleryControl)sender;
            Point p =  e.GetPosition(galControl);
            HitTestResult res = VisualTreeHelper.HitTest(galControl, p);

            if (res.VisualHit == null)
                return;

            GalleryItemControl galleryItemControl = LayoutHelper.FindParentObject<GalleryItemControl>(res.VisualHit);

            if (galleryItemControl == null)
                return;

            _dragged = (Photo)galleryItemControl.Item.Tag;

            if (_dragged != null)
            {
                _isMouseDown = true;
                _dragStartPosition = p;
            }
        }

        private void ResetState()
        {
            _isMouseDown = false;
            _isDragging = false;
            _dragged = null;
        }

        private void Image_DropFirst(object sender, DragEventArgs e)
        {
            if (!(e.Data.GetDataPresent(typeof(Photo))))
                return;
            PhotoComparison pc = (PhotoComparison)((Image)sender).Tag;

            if (pc == null)
                return;

            pc.FirstPhoto = (Photo)e.Data.GetData(typeof(Photo));
        }

        private void Image_DropSecond(object sender, DragEventArgs e)
        {
            if (!(e.Data.GetDataPresent(typeof(Photo))))
                return;

            PhotoComparison pc = (PhotoComparison)((Image)sender).Tag;

            if (pc == null)
                return;

            pc.SecondPhoto = (Photo)e.Data.GetData(typeof(Photo));
        }

        private void Image_MouseLeftButtonUpFirst(object sender, MouseButtonEventArgs e)
        {
            PhotoComparison pc = (PhotoComparison)((Image) sender).Tag;
            imageViewer.Tag = pc.FirstPhoto;
            ShowPhotoInImageViewer(pc.FirstPhoto);
            mainView.Effect = new BlurEffect() { Radius = 3 };
            //mainView.IsEnabled = false;
            imageViewPopup.Visibility = Visibility.Visible;
            imageViewPopup.Focus();
        }

        private void Image_MouseLeftButtonUpSecond(object sender, MouseButtonEventArgs e)
        {
            PhotoComparison pc = (PhotoComparison)((Image)sender).Tag;
            imageViewer.Tag = pc.SecondPhoto;
            ShowPhotoInImageViewer(pc.SecondPhoto);
            mainView.Effect = new BlurEffect() { Radius = 3 };
            //mainView.IsEnabled = false;
            imageViewPopup.Visibility = Visibility.Visible;
            imageViewPopup.Focus();
        }

    }
}
