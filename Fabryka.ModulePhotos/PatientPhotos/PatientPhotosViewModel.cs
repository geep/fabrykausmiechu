﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.ViewModel;
using Fabryka.Common.Services;
using Fabryka.Business;
using DevExpress.Xpo;
using Microsoft.Practices.Prism.Commands;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;

namespace Fabryka.ModulePhotos.PatientPhotos
{
    [Export]
    public class PatientPhotosViewModel : NotificationObject
    {
        private string _ComparisonName;
        private Photo _SelectedPhoto;
        private bool updatingValues;

        private bool _ShowAllPhotos;
        private bool _ShowComparisonTable;
        private readonly ISelectedPatientService selectedService;

        private readonly IDataService dataService;


        public string ComparisonName
        {
            get { return _ComparisonName; }
            set
            {
                _ComparisonName = value;
                AddComparison.RaiseCanExecuteChanged();
            }
        }

        private List<string> _PhotoComparisonList;
        public List<string> PhotoComparisonList
        {
            get
            {
                return _PhotoComparisonList;
            }
        }
        

        public Photo SelectedPhoto
        {
            get
            {
                return _SelectedPhoto;
            }
            set
            {
                _SelectedPhoto = value;
                DeletePhoto.RaiseCanExecuteChanged();
                SetAsMainPhoto.RaiseCanExecuteChanged();
                ChangeImportantPhoto.RaiseCanExecuteChanged();
            }
        }

        private Patient _SelectedPatient;
        public Patient SelectedPatient
        {
            get
            {
                return _SelectedPatient;
            }

            set
            {
                _SelectedPatient = value;



                RaisePropertyChanged(() => SelectedPatient);
                if (_SelectedPatient != null)
                {
                    dataService.UpdatePhotos();
                    RaisePropertyChanged(() => Photos);
                    RaisePropertyChanged(() => ImportantPhotoDescription);                 
                }

                AddComparison.RaiseCanExecuteChanged();
            }
        }


        public bool ShowComparisonTable
        {
            get { return _ShowComparisonTable; }
            set
            {
                if (_ShowComparisonTable == value)
                    return;

                _ShowComparisonTable = value;

                if (!updatingValues)
                {
                    updatingValues = true;

                    if (value == false && ShowAllPhotos == false)
                        ShowAllPhotos = true;

                    updatingValues = false;
                }

                RaisePropertyChanged(() => ShowComparisonTable);
            }
        }


        public bool ShowAllPhotos
        {
            get { return _ShowAllPhotos; }
            set
            {
                if (_ShowAllPhotos == value)
                    return;

                _ShowAllPhotos = value;

                if (!updatingValues)
                {
                    updatingValues = true;

                    if (value == false && ShowComparisonTable == false)
                        ShowComparisonTable = true;

                    updatingValues = false;
                }

                RaisePropertyChanged(() => ShowAllPhotos);
            }
        }

        public DelegateCommand DeletePhoto { get; private set; }
		public DelegateCommand<PhotoComparison> RemoveComparison { get; private set; }
        public DelegateCommand SetAsMainPhoto { get; private set; }
        public DelegateCommand ChangeImportantPhoto { get; private set; }
        public DelegateCommand AddComparison { get; private set; }

        public string ImportantPhotoDescription
        {
            get
            {
                if (SelectedPhoto == null)
                    return "";

                return (!SelectedPhoto.IsImportantPhoto ? "Oznacz" : "Odznacz") + " ważne zdjęcie";
            }
        }

        public XPCollection<Photo> Photos
        {
            get { return SelectedPatient == null ? null : SelectedPatient.Photos; }
        }

        [ImportingConstructor]
        public PatientPhotosViewModel(ISelectedPatientService _selectedService, IDataService _dataService)
        {
            dataService = _dataService;

            DeletePhoto = new DelegateCommand(
                () =>
                {
                    if (SelectedPhoto != null)
                    {
                        var oldPhoto = SelectedPhoto;
                        SelectedPhoto = null;
                        _dataService.RemovePhoto(oldPhoto);
                    }
                    //PhotosChanged();
                },
                () => SelectedPhoto != null && SelectedPhoto != SelectedPatient.MainPhoto
            );
			
			RemoveComparison = new DelegateCommand<PhotoComparison>(
				(pc) =>
				{
					if(pc == null)
						return;
                    _dataService.RemovePhotoComparison(pc);
				}
			);

            SetAsMainPhoto = new DelegateCommand(
                () =>
                {
                    if (SelectedPhoto != null)
                    {
                        SelectedPatient.MainPhoto = SelectedPhoto;
                    }
                },
                () => SelectedPhoto != null && SelectedPhoto != SelectedPatient.MainPhoto
            );

            ChangeImportantPhoto = new DelegateCommand(
                () =>
                {
                    if (SelectedPhoto != null)
                    {
                        SelectedPhoto.IsImportantPhoto = !SelectedPhoto.IsImportantPhoto;
                        ChangeImportantPhoto.RaiseCanExecuteChanged();
                    }
                },
                () =>
                {
                    var result = SelectedPhoto != null && SelectedPatient != null &&
                    (SelectedPhoto.IsImportantPhoto == true || SelectedPatient.ImportantPhotos.Count() < 4);
                    return result;
                }
                );

            AddComparison = new DelegateCommand(
                () =>
                {
                    var comp = _dataService.CreatePhotoComparison(SelectedPatient);
                    comp.Name = ComparisonName;
                },
                () => SelectedPatient != null && !String.IsNullOrWhiteSpace(ComparisonName)
                );

            _ShowAllPhotos = true;
            selectedService = _selectedService;
            selectedService.SelectionChanged += selectedService_SelectionChanged;
            SelectedPatient = selectedService.SelectedPatient;
            if (SelectedPatient != null)
                SelectedPatient.Photos.ListChanged += Photos_ListChanged;

            _PhotoComparisonList = Properties.Settings.Default.PhotoComparisonList.ConvertAll((input) => input.PhotoComparisonName);
            Properties.Settings.Default.SettingChanging += Default_SettingChanging;
        }

        void Photos_ListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
        {
            if (e.ListChangedType == System.ComponentModel.ListChangedType.ItemAdded || e.ListChangedType == System.ComponentModel.ListChangedType.ItemDeleted || e.ListChangedType == System.ComponentModel.ListChangedType.Reset)
            {
                PhotosChanged();
            }
        }

        void Default_SettingChanging(object sender, System.Configuration.SettingChangingEventArgs e)
        {
            if (e.SettingName == "PhotoComparisonList")
            {
                _PhotoComparisonList = ((Settings.PhotoComparisonList)e.NewValue).ConvertAll((input) => input.PhotoComparisonName);
                RaisePropertyChanged(() => PhotoComparisonList);
            }
        }

        public BitmapImage CreateThumbnail(BitmapImage bmImage, MemoryStream mem)
        {
            BitmapImage bi = new BitmapImage();

            int height = bmImage.PixelHeight;
            int width = bmImage.PixelWidth;
            double ratio = width / (double)height;

            if (height > width)
            {
                height = Definitions.THUMBNAIL_SIZE;
                width = (int)(height * ratio);
            }
            else
            {
                width = Definitions.THUMBNAIL_SIZE;
                height = (int)(width / ratio);
            }

            width = Math.Min(Math.Max(width, 1), Definitions.THUMBNAIL_SIZE);
            height = Math.Min(Math.Max(height, 1), Definitions.THUMBNAIL_SIZE);


            bi.BeginInit();
            bi.CacheOption = BitmapCacheOption.OnLoad;
            bi.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
            bi.DecodePixelWidth = width;

            bi.DecodePixelHeight = height;

            Stream bmImageStreamSource = mem;
            mem.Seek(0, SeekOrigin.Begin);

            bi.StreamSource = bmImageStreamSource;

            bi.EndInit();

            //bi.Freeze();
            return bi;
        }

        private BitmapImage GetBitmapImage<T>(BitmapSource bitmapSource) where T : BitmapEncoder, new()
        {
            var frame = BitmapFrame.Create(bitmapSource);
            var encoder = new T();
            encoder.Frames.Add(frame);
            var bitmapImage = new BitmapImage();
            bool isCreated;
            try
            {
                using(var ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.StreamSource = ms;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    isCreated = true;
                }
            }
            catch
            {
                isCreated = false;
            }
            return isCreated ? bitmapImage : null;
        }

        public void RotatePhoto(Photo currentPhoto, Rotation rotation)
        {
            dataService.BeginTransaction();
            double degRotation = 0;
            switch (rotation)
            {
                case Rotation.Rotate0:
                    degRotation = 0;
                    break;
                case Rotation.Rotate180:
                    degRotation = 180;
                    break;
                case Rotation.Rotate270:
                    degRotation = 270;
                    break;
                case Rotation.Rotate90:
                    degRotation = 90;
                    break;
            }

            System.Windows.Media.Transform tr = new RotateTransform(degRotation);

            TransformedBitmap transformedBmp = new TransformedBitmap();
            transformedBmp.BeginInit();
            transformedBmp.Source = currentPhoto.Picture as BitmapSource;
            transformedBmp.Transform = tr;
            transformedBmp.EndInit();

            BitmapImage bi = GetBitmapImage<JpegBitmapEncoder>(transformedBmp);

            currentPhoto.Picture = bi;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(transformedBmp));
            using(MemoryStream mem = new MemoryStream())
            {
                encoder.Save(mem);
                currentPhoto.Thumbnail = CreateThumbnail(bi, mem);
            }
            

            dataService.EndTransaction();
        }

        void selectedService_SelectionChanged(object sender, EventArgs e)
        {
            if (SelectedPatient != null)
                SelectedPatient.Photos.ListChanged -= Photos_ListChanged;
            SelectedPatient = selectedService.SelectedPatient;
            if (SelectedPatient != null)
                SelectedPatient.Photos.ListChanged += Photos_ListChanged;
        }

        internal void PhotosChanged()
        {
            RaisePropertyChanged(() => Photos);
        }

        internal void SavePhoto(Photo photo)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = photo.Name; // Default file name
            dlg.DefaultExt = ".jpg"; // Default file extension
            dlg.Filter = "Obrazy (.jpg)|*.jpg"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;
                try
                {
                    using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(photo.Picture as BitmapSource));
                        encoder.Save(fs);
                    }
                }
                catch
                {
                }
            }            
        }
    }
}
