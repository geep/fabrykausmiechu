﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace Fabryka.Business
{
    public class NFZCompletedProcedure : XPObject
    {
        public NFZCompletedProcedure(Session session)
            : base(session)
        { }

        // Fields...
        private NFZVisit _ParentVisit;
        private int _Points;
        private string _Identifier;
        private NFZProcedure _Procedure;

        public NFZProcedure Procedure
        {
            get
            {
                return _Procedure;
            }
            set
            {
                SetPropertyValue("Procedure", ref _Procedure, value);
            }
        }


        public string Identifier
        {
            get
            {
                return _Identifier;
            }
            set
            {
                SetPropertyValue("Identifier", ref _Identifier, value);
            }
        }


        public int Points
        {
            get
            {
                return _Points;
            }
            set
            {
                SetPropertyValue("Points", ref _Points, value);
            }
        }

        [Association("NFZVisit-NFZCompletedProcedures")]
        public NFZVisit ParentVisit
        {
            get
            {
                return _ParentVisit;
            }
            set
            {
                SetPropertyValue("ParentVisit", ref _ParentVisit, value);
            }
        }
    }
}
