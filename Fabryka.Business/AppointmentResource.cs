﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using System.Drawing;
using DevExpress.Xpo.Metadata;

namespace Fabryka.Business
{
    public class AppointmentResource : XPObject
    {
        public AppointmentResource()
        {

        }
        public AppointmentResource(Session session)
            : base(session)
        {

        }
        public AppointmentResource(Session session, DevExpress.Xpo.Metadata.XPClassInfo classInfo)
            : base(session, classInfo)
        {

        }

        private bool _DontSendSMS;
        private string _Label;

        public string Label
        {
            get { return _Label; }
            set { SetPropertyValue("Label", ref _Label, value); }
        }

        protected Color _Color;

        [Persistent, ValueConverter(typeof(XPColorConverter))]
        public Color Color
        {
            get { return _Color; }
            set { SetPropertyValue("Color", ref _Color, value); }
        }

        [Association("Appointments-Resource")]
        public XPCollection<Appointment> Appointments
        {
            get { return GetCollection<Appointment>("Appointments"); }
        }


        public bool DontSendSMS
        {
            get
            {
                return _DontSendSMS;
            }
            set
            {
                SetPropertyValue("DontSendSMS", ref _DontSendSMS, value);
            }
        }
    }
}
