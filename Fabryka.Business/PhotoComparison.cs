﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace Fabryka.Business
{
    public class PhotoComparison : XPObject, IUpdateTypeObject
    {
        public PhotoComparison()
        {

        }
        public PhotoComparison(Session session)
            : base(session)
        {

        }
        public PhotoComparison(Session session, XPClassInfo classInfo)
            : base(session, classInfo)
        {

        }

        protected string _Name;

        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        protected Patient _Patient;

        [Association("Patient-PhotoComparisons")]
        public Patient Patient
        {
            get { return _Patient; }
            set { SetPropertyValue("Patient", ref _Patient, value); }
        }

        protected Photo _FirstPhoto;

        public Photo FirstPhoto
        {
            get
            {
                return _FirstPhoto;
            }
            set
            {
                SetPropertyValue("FirstPhoto", ref _FirstPhoto, value);
                RaisePropertyChangedEvent("FirstPhoto!");
            }
        }

        protected Photo _SecondPhoto;

        public Photo SecondPhoto
        {
            get
            {
                return _SecondPhoto;
            }
            set
            {
                SetPropertyValue("SecondPhoto", ref _SecondPhoto, value);
                RaisePropertyChangedEvent("SecondPhoto!");
            }
        }

        [NonPersistent]
        public UpdateType UpdateType
        {
            get { return UpdateType.PhotoComparisons; }
        }
    }
}
