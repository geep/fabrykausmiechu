﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using System.Drawing;
using DevExpress.Xpo.Metadata;
using DevExpress.Data.Filtering;

namespace Fabryka.Business
{
    public class AppointmentType : XPObject
    {
        public AppointmentType()
        {

        }
        public AppointmentType(Session session)
            : base(session)
        {

        }
        public AppointmentType(Session session, DevExpress.Xpo.Metadata.XPClassInfo classInfo)
            : base(session, classInfo)
        {

        }

        private string _Label;

        public string Label
        {
            get { return _Label; }
            set { SetPropertyValue("Label", ref _Label, value); }
        }

        protected Color _Color;

        [Persistent, ValueConverter(typeof(XPColorConverter))]
        public Color Color
        {
            get { return _Color; }
            set { SetPropertyValue("Color", ref _Color, value); }
        }

        private TimeSpan _Duration;

        public TimeSpan Duration
        {
            get
            {
                return _Duration;
            }
            set
            {
                SetPropertyValue("Duration", ref _Duration, value);
            }
        }

        private int _Position;

        public int Position
        {
            get
            {
                return _Position;
            }
            set
            {
                SetPropertyValue("Position", ref _Position, value);
            }
        }


        private bool _IsDefault;
        public bool IsDefault
        {
            get
            {
                return _IsDefault;
            }
            set
            {
                SetPropertyValue("IsDefault", ref _IsDefault, value);
            }
        }

        private bool _IsAllDay;
        public bool IsAllDay
        {
            get
            {
                return _IsAllDay;
            }
            set
            {
                SetPropertyValue("IsAllDay", ref _IsAllDay, value);
            }
        }

        private static Dictionary<bool, bool> _readDefault = new Dictionary<bool,bool>();
        private static Dictionary<bool, AppointmentType> _Default = new Dictionary<bool,AppointmentType>();

        public static AppointmentType GetDefault(Session session, bool allDay)
        {
            if (!_readDefault.ContainsKey(allDay))
            {
                _Default[allDay] = session.FindObject<AppointmentType>(
                    new GroupOperator(GroupOperatorType.And,
                        new BinaryOperator("IsDefault", true), new BinaryOperator("IsAllDay", allDay)));
                _readDefault[allDay] = true;
            }

            return _Default[allDay];
        }

    }
}
