﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fabryka.Business
{
    public enum UpdateType
    {
        None = 0,
        Patient = 1,
        NewPatient = 2,
        PatientDelete = 3,
        Appointments = 4,
        Visits = 5,
        Photos = 6,
        PhotoComparisons = 7,
        TreatmentPlan = 8
    }
}
