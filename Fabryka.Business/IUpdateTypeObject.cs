﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fabryka.Business
{
    public interface IUpdateTypeObject
    {
        UpdateType UpdateType { get; }
    }

    public interface IPropertyUpdateTypeObject : IUpdateTypeObject
    {
        UpdateType PropertyUpdateType(string propertyName);
    }
}
