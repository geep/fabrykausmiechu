﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace Fabryka.Business
{
    public class SMSLogEntry : XPObject
    {
        public SMSLogEntry(Session session)
            : base(session)
        { }

        // Fields...
        private int _NotifiedAppointmentID;
        private DateTime _CreatedOn = DateTime.Now;
        private string _Message;
        private string _CellphoneNumber;
        private SMSLogType _Type;

        public SMSLogType Type
        {
            get
            {
                return _Type;
            }
            set
            {
                SetPropertyValue("Type", ref _Type, value);
            }
        }

        public string CellphoneNumber
        {
            get
            {
                return _CellphoneNumber;
            }
            set
            {
                SetPropertyValue("CellphoneNumber", ref _CellphoneNumber, value);
            }
        }

        [Size(255)]
        public string Message
        {
            get
            {
                return _Message;
            }
            set
            {
                SetPropertyValue("Message", ref _Message, value);
            }
        }


        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }
            set
            {
                SetPropertyValue("CreatedOn", ref _CreatedOn, value);
            }
        }


        public int NotifiedAppointmentID
        {
            get
            {
                return _NotifiedAppointmentID;
            }
            set
            {
                SetPropertyValue("NotifiedAppointmentID", ref _NotifiedAppointmentID, value);
            }
        }
    }
}
