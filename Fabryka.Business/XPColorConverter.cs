﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Xpo.Metadata;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace Fabryka.Business
{
    public class XPColorConverter : ValueConverter
    {
        public override Type StorageType
        {
            get { return typeof(int); } // Persistent type is int.
        }
        public override object ConvertToStorageType(object value)
        {
            return ((Color)value).ToArgb(); // Converts from Memory (Color) to DB (int).
        }
        public override object ConvertFromStorageType(object value)
        {
            if (value == null)
                return Color.Empty;
            return Color.FromArgb((int)value); // Converts back from int (DB) to Color (in Memory).
        }
    }
}
