using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Xpo.Metadata;
using System.IO;
using System.Windows.Media.Imaging;

namespace Fabryka.Business
{
    #region BitmapImageSourceConverter
    /// <summary>
    /// From http://social.msdn.microsoft.com/forums/en-US/wpf/thread/8327dd31-2db1-4daa-a81c-aff60b63fee6/
    /// Converting an Image/BitmapImage object into Byte Array and vice versa
    /// </summary>
    public class BitmapImageSourceConverter : ValueConverter
    {
        #region ConvertToStorageType
        public override object ConvertToStorageType(object value)
        {
            BitmapImage image = (BitmapImage)value;
            if (image == null)
                return null;
            else
                return BufferFromImage(image);
        }

        public static Byte[] BufferFromImage(BitmapImage imageSource)
        {
            byte[] result;
            using (MemoryStream stream = new MemoryStream())
            {

                BitmapEncoder enc = new JpegBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(imageSource));
                enc.Save(stream);

                result = stream.ToArray();
            }

            return result;
        }
        #endregion

        #region ConvertFromStorageType
        public override object ConvertFromStorageType(object value)
        {
            Byte[] buffer = (Byte[])value;
            if (buffer == null)
                return null;
            else
                return ImageFromBuffer(buffer);
        }

        public static BitmapImage ImageFromBuffer(Byte[] bytes)
        {
            if (bytes == null) return null;

            BitmapImage image = new BitmapImage();
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.StreamSource = stream;
                image.EndInit();
            }
            return image;
        }

        #endregion

        #region StorageType
        public override Type StorageType { get { return typeof(byte[]); } }
        #endregion
    }
    #endregion
}
