﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace Fabryka.Business
{
    public class TemporaryPatient : XPObject, IDataErrorInfo
    {
        #region Constructors

        public TemporaryPatient()
        {

        }
        public TemporaryPatient(Session session)
            : base(session)
        {

        }
        public TemporaryPatient(Session session, DevExpress.Xpo.Metadata.XPClassInfo classInfo)
            : base(session, classInfo)
        {

        }
        #endregion

        private readonly static string _verifyPhonePattern = @"^(\+?[0-9\s]+)?$";
        private readonly static Regex _verifyPhoneExpr = new Regex(_verifyPhonePattern, RegexOptions.Compiled);

        protected string _FirstName = string.Empty;

        public string FirstName
        {
            get { return _FirstName; }
            set { SetPropertyValue("FirstName", ref _FirstName, value.NullTrim()); }
        }

        protected string _LastName = string.Empty;

        public string LastName
        {
            get { return _LastName; }
            set { SetPropertyValue("LastName", ref _LastName, value.NullTrim()); }
        }

        protected string _CellphoneNumber = string.Empty;

        public string CellphoneNumber
        {
            get { return _CellphoneNumber; }
            set { SetPropertyValue("CellphoneNumber", ref _CellphoneNumber, value.NullTrim()); }
        }

        [Association("Appointment-TemporaryPatient")]
        public XPCollection<Appointment> Appointment
        {
            get
            {
                return GetCollection<Appointment>("Appointment");
            }
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get 
            {
                string result = null;

                switch (columnName)
                {
                    case "CellphoneNumber":
                        if (!_verifyPhoneExpr.IsMatch(CellphoneNumber))
                            result = "Numer telefonu może zawierać wyłącznie cyfry, spacje i znak +";
                        break;
                    default:
                        break;
                }

                return result;
            }
        }
    }
}
