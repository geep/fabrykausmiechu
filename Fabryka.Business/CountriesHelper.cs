﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Fabryka.Business
{
    public class CountriesHelper
    {
        public static List<Country> Countries;

        static CountriesHelper()
        {
            var countries = new Dictionary<string, string>();

            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.SpecificCultures)) 
            {
                var regionInfo = new RegionInfo(ci.LCID);
                countries[regionInfo.ThreeLetterISORegionName] = regionInfo.DisplayName;
            }

            Countries = countries.OrderBy(pair => pair.Value)
                .Select(pair => new Country { ISOCode = pair.Key, DisplayName = pair.Value})
                .ToList();
        }
    }

    public class Country
    {
        public string ISOCode { get; set; }
        public string DisplayName { get; set; }
    }
}
