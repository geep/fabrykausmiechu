﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Xpo;

namespace Fabryka.Business
{
    public class Appointment : XPObject, IUpdateTypeObject
    {
        public Appointment()
        {

        }
        public Appointment(Session session)
            : base(session)
        {

        }
        public Appointment(Session session, DevExpress.Xpo.Metadata.XPClassInfo classInfo)
            : base(session, classInfo)
        {

        }

        private DateTime _StartTime;

        public DateTime StartTime
        {
            get { return _StartTime; }
            set { SetPropertyValue("StartTime", ref _StartTime, value); }
        }

        protected DateTime _EndTime;

        public DateTime EndTime
        {
            get { return _EndTime; }
            set { SetPropertyValue("EndTime", ref _EndTime, value); }
        }

        // TODO: Zrób relacje

        protected AppointmentType _Type;

        public AppointmentType Type
        {
            get 
            {
                if (_Type == null)
                    Type = AppointmentType.GetDefault(Session, AllDay);
                return _Type; 
            }
            set { SetPropertyValue("Type", ref _Type, value); }
        }

        protected AppointmentResource _Resource;

        [Association("Appointments-Resource")]
        public AppointmentResource Resource
        {
            get { return _Resource; }
            set { SetPropertyValue("Resource", ref _Resource, value); }
        }

        protected Doctor _Doctor; 

        [Association("Doctor-Appointments")]
        public Doctor Doctor
        {
            get { return _Doctor; }
            set { SetPropertyValue("Doctor", ref _Doctor, value); }
        }

        protected string _Description;

        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }

        protected Patient _Patient;


        [Association("Patient-Appointments")]
        public Patient Patient
        {
            get { return _Patient; }
            set 
            { 
                SetPropertyValue("Patient", ref _Patient, value);

                RaisePropertyChangedEvent("PatientFirstName");
                RaisePropertyChangedEvent("PatientLastName");
                RaisePropertyChangedEvent("PatientCellphoneNumber");
            }
        }

        private TemporaryPatient _NewPatient;

        [Aggregated, Association("Appointment-TemporaryPatient")]
        public TemporaryPatient NewPatient
        {
            get
            {
                return _NewPatient;
            }
            set
            {
                SetPropertyValue("NewPatient", ref _NewPatient, value);

                RaisePropertyChangedEvent("PatientFirstName");
                RaisePropertyChangedEvent("PatientLastName");
                RaisePropertyChangedEvent("PatientCellphoneNumber");
            }
        }

        private bool _IsNewPatient;

        public bool IsNewPatient
        {
            get
            {
                return _IsNewPatient;
            }
            set
            {
                SetPropertyValue("IsNewPatient", ref _IsNewPatient, value);
            }
        }

        [NonPersistent]
        public string PatientFirstName 
        {
            get
            {
                if (Patient == null && !IsNewPatient)
                    return "";

                
                if (IsNewPatient )
                {
                    if (NewPatient != null)
                        return NewPatient.FirstName;
                    else
                    {
                        //TODO: znajdź powód na null tego?!!?!
                        Fabryka.Logger.GlobalLogger.Log(String.Format("NULL NewPatient in Appointment {0}", Oid));
                        return "";
                    }
                }

                if (Patient == null)
                    return "";
                return Patient.FirstName;
            }

            set
            {
                NewPatient.FirstName = value;
                RaisePropertyChangedEvent("PatientFirstName");
            }
        }

        [NonPersistent]
        public string PatientLastName
        {
            get
            {
                if (Patient == null && !IsNewPatient)
                    return "";

                if (IsNewPatient && NewPatient != null)
                {
                    return NewPatient.LastName;
                }

                if (Patient == null)
                    return "";

                return Patient.LastName;
            }

            set
            {
                NewPatient.LastName = value;
                RaisePropertyChangedEvent("PatientLastName");
            }
        }

        [NonPersistent]
        public string PatientCellphoneNumber
        {
            get
            {
                if (Patient == null && !IsNewPatient)
                    return "";

                if (IsNewPatient && NewPatient != null)
                {
                    return NewPatient.CellphoneNumber;
                }

                if (Patient == null)
                    return "";

                if (Patient.ContactData == null)
                    return "";

                return Patient.ContactData.CellphoneNumber;
            }

            set
            {
                NewPatient.CellphoneNumber = value;
                RaisePropertyChangedEvent("PatientCellphoneNumber");
            }
        }

        private bool _IsReccurring;

        public bool IsReccurring
        {
            get
            {
                return _IsReccurring;
            }
            set
            {
                SetPropertyValue("IsReccurring", ref _IsReccurring, value);
            }
        }

        private bool _AllDay;
        public bool AllDay
        {
            get
            {
                return _AllDay;
            }
            set
            {
                SetPropertyValue("AllDay", ref _AllDay, value);
            }
        }

        protected AppointmentState _State;

        public AppointmentState State
        {
            get { return _State; }
            set { SetPropertyValue("State", ref _State, value); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // TODO: Napraw to.
            NewPatient = new TemporaryPatient(Session);
            if (Type == null)
                Type = AppointmentType.GetDefault(Session, AllDay);
        }

        public UpdateType UpdateType
        {
            get { return UpdateType.Appointments; }
        }
    }
}
