﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace Fabryka.Business
{
    public class PatientContactData : XPObject
    {
        public PatientContactData()
        {

        }
        public PatientContactData(Session session)
            : base(session)
        {

        }
        public PatientContactData(Session session, DevExpress.Xpo.Metadata.XPClassInfo classInfo)
            : base(session, classInfo)
        {

        } 

        private bool _DontSendSMS;
        protected string _CellphoneNumber; // 9. Telefon komorkowy

        public string CellphoneNumber
        {
            get { return _CellphoneNumber; }
            set { SetPropertyValue("CellphoneNumber", ref _CellphoneNumber, value.NullTrim()); }
        }

        protected string _SecondPhoneNumber; // 10. Drugi telefon kontaktowy

        public string SecondPhoneNumber
        {
            get { return _SecondPhoneNumber; }
            set { SetPropertyValue("SecondPhoneNumber", ref _SecondPhoneNumber, value.NullTrim()); }
        }

        protected string _EmailAddress; // 11. E-mail

        public string EmailAddress
        {
            get { return _EmailAddress; }
            set { SetPropertyValue("EmailAddress", ref _EmailAddress, value.NullTrim()); }
        }

        public bool DontSendSMS
        {
            get
            {
                return _DontSendSMS;
            }
            set
            {
                SetPropertyValue("DontSendSMS", ref _DontSendSMS, value);
            }
        }
    }
}
