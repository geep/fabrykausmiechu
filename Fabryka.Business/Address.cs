﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Xpo;

namespace Fabryka.Business
{
    public class Address : XPObject
    {
        public Address()
        {

        }
        public Address(Session session)
            : base(session)
        {

        }
        public Address(Session session, DevExpress.Xpo.Metadata.XPClassInfo classInfo)
            : base(session, classInfo)
        {

        } 

        private string _Street; // a. Ulica

        public string Street
        {
            get { return _Street; }
            set { SetPropertyValue("Street", ref _Street, value.NullTrim()); }
        }

        protected string _City; // b. Miasto

        public string City
        {
            get { return _City; }
            set { SetPropertyValue("City", ref _City, value.NullTrim()); }
        }

        protected string _PostalCode; // c. Kod pocztowy

        [Size(10)]
        public string PostalCode
        {
            get { return _PostalCode; }
            set { SetPropertyValue("PostalCode", ref _PostalCode, value.NullTrim()); }
        }

        protected string _Country; // d. Kraj

        public string Country
        {
            get { return _Country; }
            set 
            {
                if (value == "Polska")
                    value = "POL";

                SetPropertyValue("Country", ref _Country, value.NullTrim()); 
            }
        }
    }
}
