﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace Fabryka.Business
{
    public class Visit : XPObject, IUpdateTypeObject
    {
        public Visit()
        {

        }
        public Visit(Session session)
            : base(session)
        {

        }
        public Visit(Session session, DevExpress.Xpo.Metadata.XPClassInfo classInfo)
            : base(session, classInfo)
        {

        } 

        private string _LowerArc;
        private string _UpperArc;
        protected DateTime _Time = DateTime.Now; // 1. Data i godzina

        public DateTime Time
        {
            get { return _Time; }
            set { SetPropertyValue("Time", ref _Time, value); }
        }

        protected Patient _Patient;

        [Association("Patient-Visits")]
        public Patient Patient
        {
            get { return _Patient; }
            set { SetPropertyValue("Patient", ref _Patient, value); }
        }

        protected Doctor _Doctor; // 2. Lekarz

        [Association("Doctor-Visits")]
        public Doctor Doctor
        {
            get { return _Doctor; }
            set { SetPropertyValue("Doctor", ref _Doctor, value); }
        }

        protected Decimal _Receivable; // 5. Naleznosc

        public Decimal Receivable
        {
            get { return _Receivable; }
            set { SetPropertyValue("Receivable", ref _Receivable, value); }
        }

        protected Decimal _Payed; // 6. Wplata

        public Decimal Payed
        {
            get { return _Payed; }
            set { SetPropertyValue("Payed", ref _Payed, value); }
        }

        protected string _Description; // 7. Opis wizyty

        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }

        [Association("Visit-Photos")]
        public XPCollection<Photo> Photos
        {
            get { return GetCollection<Photo>("Photos"); }
        }


        public string UpperArc
        {
            get
            {
                return _UpperArc;
            }
            set
            {
                SetPropertyValue("UpperArc", ref _UpperArc, value);
            }
        }


        public string LowerArc
        {
            get
            {
                return _LowerArc;
            }
            set
            {
                SetPropertyValue("LowerArc", ref _LowerArc, value);
            }
        }

        [PersistentAlias("Patient.Visits.Sum(Payed)")]
        public decimal TotalPayed
        {
            get
            {
                return Convert.ToDecimal(EvaluateAlias("TotalPayed"));
            }
        }

        [PersistentAlias("Patient.Visits.Sum(Receivable)")]
        public decimal TotalReceivable
        {
            get
            {
                return Convert.ToDecimal(EvaluateAlias("TotalReceivable"));
            }
        }

        [NonPersistent]
        public UpdateType UpdateType
        {
            get { return UpdateType.Visits; }
        }
    }
}
