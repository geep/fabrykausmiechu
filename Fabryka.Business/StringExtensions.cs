﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fabryka.Business
{
    public static class StringExtensions
    {
        public static string NullTrim(this string value)
        {
            return value == null ? null : value.Trim();
        }
    }
}
