﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace Fabryka.Business
{
    public class SMSServiceStatus : XPObject
    {
        public SMSServiceStatus(Session session)
            : base(session)
        { }

        // Fields...
        private DateTime _CreatedOn = DateTime.Now;
        private DateTime _StartTime;
        private bool _ServiceStarted;

        public bool ServiceStarted
        {
            get
            {
                return _ServiceStarted;
            }
            set
            {
                SetPropertyValue("ServiceStarted", ref _ServiceStarted, value);
            }
        }

        public DateTime StartTime
        {
            get
            {
                return _StartTime;
            }
            set
            {
                SetPropertyValue("StartTime", ref _StartTime, value);
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }
            set
            {
                SetPropertyValue("CreatedOn", ref _CreatedOn, value);
            }
        }
    }
}
