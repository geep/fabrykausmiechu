﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace Fabryka.Business
{
    public class SMSStatusLogEntry : XPObject
    {
        public SMSStatusLogEntry(Session session)
            : base(session)
        { }

        // Fields...
        private DateTime _CreatedOn = DateTime.Now;
        private string _Message;
        private SMSStatusLogType _Type;

        public SMSStatusLogType Type
        {
            get
            {
                return _Type;
            }
            set
            {
                SetPropertyValue("Type", ref _Type, value);
            }
        }

        [Size(255)]
        public string Message
        {
            get
            {
                return _Message;
            }
            set
            {
                SetPropertyValue("Message", ref _Message, value);
            }
        }


        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }
            set
            {
                SetPropertyValue("CreatedOn", ref _CreatedOn, value);
            }
        }
    }
}
