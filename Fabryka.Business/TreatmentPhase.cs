﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace Fabryka.Business
{
    public class TreatmentPhase : XPObject, IUpdateTypeObject
    {
        public TreatmentPhase(Session session)
            : base(session)
        { }

        // Fields...
        private TreatmentPlan _Plan;
        private string _PhasePlan;
        private string _PhaseName;

        public string PhaseName
        {
            get
            {
                return _PhaseName;
            }
            set
            {
                SetPropertyValue("PhaseName", ref _PhaseName, value);
            }
        }

        [Size(SizeAttribute.Unlimited)]
        public string PhasePlan
        {
            get
            {
                return _PhasePlan;
            }
            set
            {
                SetPropertyValue("PhasePlan", ref _PhasePlan, value);
            }
        }


        [Association("TreatmentPlan-TreatmentPhases")]
        public TreatmentPlan Plan
        {
            get
            {
                return _Plan;
            }
            set
            {
                SetPropertyValue("Plan", ref _Plan, value);
            }
        }

        [NonPersistent]
        public UpdateType UpdateType
        {
            get
            {
                return UpdateType.TreatmentPlan;
            }
        }
    }
}
