﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace Fabryka.Business
{
    public class RTFTemplateNode : XPObject
    {
        public RTFTemplateNode(Session session)
            : base(session)
        { }

        // Fields...
        private bool _ShowTeethPicker;
        private string _Name;
        private RTFTemplateType _TemplateType;
        private bool _NeedsTeethSelected;
        private RTFTemplateNode _Parent;
        private string _Template;
        private bool _IsLeaf;


        public RTFTemplateType TemplateType
        {
            get
            {
                return _TemplateType;
            }
            set
            {
                SetPropertyValue("TemplateType", ref _TemplateType, value);
            }
        }

        [Size(50)]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                SetPropertyValue("Name", ref _Name, value);
            }
        }

        public bool IsLeaf
        {
            get
            {
                return _IsLeaf;
            }
            set
            {
                SetPropertyValue("IsLeaf", ref _IsLeaf, value);
            }
        }


        public bool NeedsTeethSelected
        {
            get
            {
                return _NeedsTeethSelected;
            }
            set
            {
                SetPropertyValue("NeedsTeethSelected", ref _NeedsTeethSelected, value);
            }
        }

        [Size(SizeAttribute.Unlimited)]
        public string Template
        {
            get
            {
                return _Template;
            }
            set
            {
                SetPropertyValue("Template", ref _Template, value);
            }
        }


        [Association("Parent-Children")]
        public RTFTemplateNode Parent
        {
            get
            {
                return _Parent;
            }
            set
            {
                SetPropertyValue("Parent", ref _Parent, value);
            }
        }

        [Association("Parent-Children")]
        public XPCollection<RTFTemplateNode> Children
        {
            get
            {
                return GetCollection<RTFTemplateNode>("Children");
            }
        }


        public bool ShowTeethPicker
        {
            get
            {
                return _ShowTeethPicker;
            }
            set
            {
                SetPropertyValue("ShowTeethPicker", ref _ShowTeethPicker, value);
            }
        }
    }


}
