﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace Fabryka.Business
{
    public class PatientFabrykaData : XPObject
    {
        public PatientFabrykaData()
        {

        }
        public PatientFabrykaData(Session session)
            : base(session)
        {

        }
        public PatientFabrykaData(Session session, DevExpress.Xpo.Metadata.XPClassInfo classInfo)
            : base(session, classInfo)
        {

        } 
        

        private bool _IsNFZPatient;
        protected int? _PatientNumber; // 3. Numer pacjenta

        public int? PatientNumber
        {
            get { return _PatientNumber; }
            set { SetPropertyValue("PatientNumber", ref _PatientNumber, value); }
        }

        private int? _MonthNumber;
        public int? MonthNumber
        {
            get
            {
                return _MonthNumber;
            }
            set
            {
                SetPropertyValue("MonthNumber", ref _MonthNumber, value);
            }
        }

        private int? _YearNumber;
        public int? YearNumber
        {
            get
            {
                return _YearNumber;
            }
            set
            {
                SetPropertyValue("YearNumber", ref _YearNumber, value);
            }
        }

        private string _ClinicNumberType;
        public string ClinicNumberType
        {
            get
            {
                return _ClinicNumberType;
            }
            set
            {
                SetPropertyValue("ClinicNumberType", ref _ClinicNumberType, value);
            }
        }

        protected int? _ClinicNumber; // 4. Numer kliniczny

        public int? ClinicNumber
        {
            get { return _ClinicNumber; }
            set { SetPropertyValue("ClinicNumber", ref _ClinicNumber, value); }
        }

        protected string _PESEL; // 12. PESEL

        [Size(11)]
        public string PESEL
        {
            get { return _PESEL; }
            set { SetPropertyValue("PESEL", ref _PESEL, value.NullTrim()); }
        }


        public bool IsNFZPatient
        {
            get
            {
                return _IsNFZPatient;
            }
            set
            {
                SetPropertyValue("IsNFZPatient", ref _IsNFZPatient, value);
            }
        }
    }
}
