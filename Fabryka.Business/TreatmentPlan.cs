﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace Fabryka.Business
{
    public class TreatmentPlan : XPObject
    {
        public TreatmentPlan()
        {

        }
        public TreatmentPlan(Session session)
            : base(session)
        {

        }
        public TreatmentPlan(Session session, DevExpress.Xpo.Metadata.XPClassInfo classInfo)
            : base(session, classInfo)
        {

        }

        protected string _UpperJaw; // b.i Faza retencji - Szczeka
        [Size(SizeAttribute.Unlimited)]
        public string UpperJaw
        {
            get { return _UpperJaw; }
            set { SetPropertyValue("UpperJaw", ref _UpperJaw, value); }
        }

        protected string _LowerJaw; // b.ii Faza retencji - Zuchwa
        [Size(SizeAttribute.Unlimited)]
        public string LowerJaw
        {
            get { return _LowerJaw; }
            set { SetPropertyValue("LowerJaw", ref _LowerJaw, value); }
        }

        [Association("TreatmentPlan-TreatmentPhases")]
        public XPCollection<TreatmentPhase> TreatmentPhases
        {
            get
            {
                return GetCollection<TreatmentPhase>("TreatmentPhases");
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (TreatmentPhases.Count == 0)
            {
                TreatmentPhases.Add(new TreatmentPhase(Session)
                {
                    PhaseName = "Pierwsza"
                });
            }
        }

        [NonPersistent]
        public UpdateType UpdateType
        {
            get
            {
                return UpdateType.TreatmentPlan;
            }
        }
    }
}
