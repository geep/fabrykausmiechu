﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using System.Drawing;
using System.Windows.Media;

namespace Fabryka.Business
{
    public class Photo : XPObject, IUpdateTypeObject
    {
        public Photo()
        {

        }
        public Photo(Session session)
            : base(session)
        {

        }
        public Photo(Session session, XPClassInfo classInfo)
            : base(session, classInfo)
        {

        }

        private bool _IsImportantPhoto;
        private DateTime _CreationTime;
        protected string _Name;

        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        protected string _Description;

        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }

        protected ImageSource _Thumbnail;

        [ValueConverter(typeof(BitmapImageSourceConverter))]
        public ImageSource Thumbnail
        {
            get { return _Thumbnail; }
            set { SetPropertyValue("Thumbnail", ref _Thumbnail, value); }
        }

        [Delayed(true)]
        [ValueConverter(typeof(BitmapImageSourceConverter))]
        public ImageSource Picture
        {
            get { return GetDelayedPropertyValue<ImageSource>("Picture"); }
            set
            {
                SetDelayedPropertyValue("Picture", value);
            }
        }

        protected Patient _Patient;

        [Association("Patient-Photos")]
        public Patient Patient
        {
            get { return _Patient; }
            set { SetPropertyValue("Patient", ref _Patient, value); }
        }

        protected Visit _Visit;

        [Association("Visit-Photos")]
        public Visit Visit
        {
            get { return _Visit; }
            set { SetPropertyValue("Visit", ref _Visit, value); }
        }

        [NonPersistent]
        public bool IsMainPhoto
        {
            get
            {
                if (Patient != null && Patient.MainPhoto == this)
                    return true;
                return false;
            }
        }


        public bool IsImportantPhoto
        {
            get
            {
                return _IsImportantPhoto;
            }
            set
            {
                SetPropertyValue("IsImportantPhoto", ref _IsImportantPhoto, value);
                if (Patient != null && !IsLoading)
                    Patient.NotifyImportantPhotosChanged();
            }
        }

        public DateTime CreationTime
        {
            get
            {
                return _CreationTime;
            }
            set
            {
                SetPropertyValue("CreationTime", ref _CreationTime, value);
            }
        }


        public override void AfterConstruction()
        {
            base.AfterConstruction();

            CreationTime = DateTime.Now;
        }

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);

            if (!this.IsLoading && propertyName == "Picture" && oldValue != newValue)
            {
                //Thumbnail = ((Image)newValue).GetThumbnailImage(Definitions.THUMBNAIL_SIZE, Definitions.THUMBNAIL_SIZE, null, new IntPtr());
            }
        }

        public void NotifyStateChanged()
        {
            RaisePropertyChangedEvent("IsMainPhoto");
            //RaisePropertyChangedEvent("IsImportantPicture");
        }

        [NonPersistent]
        public UpdateType UpdateType
        {
            get { return UpdateType.Photos; }
        }
    }
}
