﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Fabryka.Business
{
    public enum Gender
    {
        [Description("Nie ustawiona")]
        None = 0,
        [Description("Mężczyzna")]
        Male = 1,
        [Description("Kobieta")]
        Female = 2
    }

    public enum AppointmentState
    {
        [Description("Brak")]
        Unknown = 0,
        [Description("Odbyła się")]
        Occured = 1,
        [Description("Nie przyszedł/ła")]
        NotArrived = 2
    }

    public enum DatabaseType
    {
        Firebird = 0,
        Access = 1
    }

    public enum ApplicationMode
    {
        Server = 0,
        Client = 1
    }

    public enum RTFTemplateType
    {
        [Description("Wizyty")]
        Visits = 0,
        [Description("Diagnozy")]
        Diagnosis = 1,
        [Description("Plany leczenia")]
        TreatmentPlan = 2,
        [Description("Aparaty")]
        Braces = 3
    }

    public enum SMSLogType
    {
        Sent = 0,
        Recieved = 1
    }

    public enum SMSStatusLogType
    {
        Info = 0,
        SendingDaily = 1,
        Error = 2,
        Important = 3,
        Stopped = 4
    }
}
