﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace Fabryka.Business
{
    public class NFZProcedure : XPObject
    {
        public NFZProcedure(Session session)
            : base(session)
        { 
        }

        // Fields...
        private int _Points;
        private string _Identifier;
        private string _Description;

        public string Identifier
        {
            get
            {
                return _Identifier;
            }
            set
            {
                SetPropertyValue("Identifier", ref _Identifier, value);
            }
        }

        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                SetPropertyValue("Description", ref _Description, value);
            }
        }

        public int Points
        {
            get
            {
                return _Points;
            }
            set
            {
                SetPropertyValue("Points", ref _Points, value);
            }
        }
    }
}
