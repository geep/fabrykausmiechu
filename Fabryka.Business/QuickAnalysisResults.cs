﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace Fabryka.Business
{
    public class QuickAnalysisResults : XPObject
    {
        public QuickAnalysisResults(Session session)
            : base(session)
        { }

        // Fields...
        private bool _TonnIsSet;
        private bool _BoltonIsSet;
        private bool _KorkhausIsSet;
        private bool _LittleIsSet;
        private string _KorkhausComment;
        private decimal _KorkhausIwp;
        private string _LittleSize;
        private decimal _Little;
        private string _BoltonWhere;
        private decimal _BoltonOver;
        private decimal _BoltonAnterior;
        private decimal _BoltonOverall;
        private string _TonnWhere;
        private decimal _TonnOver;
        private decimal _Tonn;


        public bool TonnIsSet
        {
            get
            {
                return _TonnIsSet;
            }
            set
            {
                SetPropertyValue("TonnIsSet", ref _TonnIsSet, value);
            }
        }

        public decimal Tonn
        {
            get
            {
                return _Tonn;
            }
            set
            {
                SetPropertyValue("Tonn", ref _Tonn, value);
            }
        }


        public decimal TonnOver
        {
            get
            {
                return _TonnOver;
            }
            set
            {
                SetPropertyValue("TonnOver", ref _TonnOver, value);
            }
        }


        public string TonnWhere
        {
            get
            {
                return _TonnWhere;
            }
            set
            {
                SetPropertyValue("TonnWhere", ref _TonnWhere, value);
            }
        }


        public decimal BoltonOverall
        {
            get
            {
                return _BoltonOverall;
            }
            set
            {
                SetPropertyValue("BoltonOverall", ref _BoltonOverall, value);
            }
        }


        public decimal BoltonAnterior
        {
            get
            {
                return _BoltonAnterior;
            }
            set
            {
                SetPropertyValue("BoltonAnterior", ref _BoltonAnterior, value);
            }
        }


        public bool BoltonIsSet
        {
            get
            {
                return _BoltonIsSet;
            }
            set
            {
                SetPropertyValue("BoltonIsSet", ref _BoltonIsSet, value);
            }
        }


        public decimal BoltonOver
        {
            get
            {
                return _BoltonOver;
            }
            set
            {
                SetPropertyValue("BoltonOver", ref _BoltonOver, value);
            }
        }


        public string BoltonWhere
        {
            get
            {
                return _BoltonWhere;
            }
            set
            {
                SetPropertyValue("BoltonWhere", ref _BoltonWhere, value);
            }
        }


        public bool LittleIsSet
        {
            get
            {
                return _LittleIsSet;
            }
            set
            {
                SetPropertyValue("LittleIsSet", ref _LittleIsSet, value);
            }
        }

        public decimal Little
        {
            get
            {
                return _Little;
            }
            set
            {
                SetPropertyValue("Little", ref _Little, value);
            }
        }


        public string LittleSize
        {
            get
            {
                return _LittleSize;
            }
            set
            {
                SetPropertyValue("LittleSize", ref _LittleSize, value);
            }
        }



        public bool KorkhausIsSet
        {
            get
            {
                return _KorkhausIsSet;
            }
            set
            {
                SetPropertyValue("KorkhausIsSet", ref _KorkhausIsSet, value);
            }
        }

        public decimal KorkhausIwp
        {
            get
            {
                return _KorkhausIwp;
            }
            set
            {
                SetPropertyValue("KorkhausIwp", ref _KorkhausIwp, value);
            }
        }


        public string KorkhausComment
        {
            get
            {
                return _KorkhausComment;
            }
            set
            {
                SetPropertyValue("KorkhausComment", ref _KorkhausComment, value);
            }
        }

    }
}
