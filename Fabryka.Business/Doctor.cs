﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using System.Drawing;
using System.Windows.Media;

namespace Fabryka.Business
{
    public class Doctor: XPObject
    {
        public Doctor()
        {

        }
        public Doctor(Session session)
            : base(session)
        {

        }
        public Doctor(Session session, XPClassInfo classInfo)
            : base(session, classInfo)
        {

        }

        protected string _FirstName;

        public string FirstName
        {
            get { return _FirstName; }
            set { SetPropertyValue("FirstName", ref _FirstName, value.NullTrim()); }
        }

        protected string _LastName;

        public string LastName
        {
            get { return _LastName; }
            set { SetPropertyValue("LastName", ref _LastName, value.NullTrim()); }
        }

        [NonPersistent]
        public string FullName
        {
            get
            {
                return String.Format("{0} {1}", FirstName, LastName);
            }
        }

        protected string _CellphoneNumber;

        public string CellphoneNumber
        {
            get { return _CellphoneNumber; }
            set { SetPropertyValue("CellphoneNumber", ref _CellphoneNumber, value.NullTrim()); }
        }

        protected ImageSource _Thumbnail;

        [ValueConverter(typeof(BitmapImageSourceConverter))]
        public ImageSource Thumbnail
        {
            get { return _Thumbnail; }
            set { SetPropertyValue("Thumbnail", ref _Thumbnail, value); }
        }

        protected ImageSource _Picture;

        [Delayed(true)]
        [ValueConverter(typeof(BitmapImageSourceConverter))]
        public ImageSource Picture
        {
            get { return GetDelayedPropertyValue<ImageSource>("Picture"); }
            set
            {
                SetDelayedPropertyValue("Picture", value);
            }
        }

        [Association("Doctor-Patients")]
        public XPCollection<Patient> Patients
        {
            get { return GetCollection<Patient>("Patients"); }
        }

        [Association("Doctor-Visits")]
        public XPCollection<Visit> Visits
        {
            get { return GetCollection<Visit>("Visits"); }
        }

        [Association("Doctor-Appointments")]
        public XPCollection<Appointment> Appointments
        {
            get { return GetCollection<Appointment>("Appointments"); }
        }

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);

            if (!this.IsLoading && propertyName == "Picture" && oldValue != newValue)
            {
                //TODO: Zrób thumbnaile tu i w photo
                //Thumbnail = ((ImageSource)newValue)
            }
        }

    }
}
