﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace Fabryka.Business
{
    public class Treatment : XPObject
    {
        public Treatment()
        {

        }
        public Treatment(Session session)
            : base(session)
        {

        }
        public Treatment(Session session, DevExpress.Xpo.Metadata.XPClassInfo classInfo)
            : base(session, classInfo)
        {

        }

        protected DateTime _StartDate; // 7. Data rozpoczecia leczenia

        public DateTime StartDate
        {
            get { return _StartDate; }
            set { SetPropertyValue("StartDate", ref _StartDate, value); }
        }

        protected string _PatientProblems; // 17 - Problemy pacjenta

        [Size(SizeAttribute.Unlimited)]
        public string PatientProblems
        {
            get { return _PatientProblems; }
            set { SetPropertyValue("PatientProblems", ref _PatientProblems, value); }
        }

        protected string _DoctorProblems; // 18 - "Nasze problemy"

        [Size(SizeAttribute.Unlimited)]
        public string DoctorProblems
        {
            get { return _DoctorProblems; }
            set { SetPropertyValue("DoctorProblems", ref _DoctorProblems, value); }
        }

        protected string _Diagnosis; // 19 - Diagnoza

        [Size(SizeAttribute.Unlimited)]
        public string Diagnosis
        {
            get { return _Diagnosis; }
            set { SetPropertyValue("Diagnosis", ref _Diagnosis, value); }
        }

        protected string _Braces; // xx - Aparaty

        [Size(SizeAttribute.Unlimited)]
        public string Braces
        {
            get { return _Braces; }
            set { SetPropertyValue("Braces", ref _Braces, value); }
        }

        protected TreatmentPlan _Plan; // 21. Plan leczenia

        [Aggregated]
        public TreatmentPlan Plan
        {
            get { return _Plan; }
            set { SetPropertyValue("Plan", ref _Plan, value); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Plan == null)
                Plan = new TreatmentPlan(Session);
        }

    }
}
