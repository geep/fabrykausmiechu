﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace Fabryka.Business
{
    public class NFZVisit : XPObject
    {
    	public NFZVisit(Session session): base(session)
    	{ 
        }

        // Fields...
        private int _TotalPoints;
        private Visit _ParentVisit;

        public Visit ParentVisit
        {
            get
            {
                return _ParentVisit;
            }
            set
            {
                SetPropertyValue("ParentVisit", ref _ParentVisit, value);
            }
        }


        public int TotalPoints
        {
            get
            {
                return _TotalPoints;
            }
            set
            {
                SetPropertyValue("TotalPoints", ref _TotalPoints, value);
            }
        }


        [Association("NFZVisit-NFZCompletedProcedures")]
        public XPCollection<NFZCompletedProcedure> CompletedProcedures
        {
            get
            {
                return GetCollection<NFZCompletedProcedure>("CompletedProcedures");
            }
        }

        [PersistentAlias("[<NFZVisit>][^.ParentVisit.Patient=ParentVisit.Patient].Sum(TotalPoints)")]
        public int PointsSummary
        {
            get
            {
                return Convert.ToInt32(EvaluateAlias("PointsSummary"));
            }
        }


        public void AddProcedure(NFZProcedure procedure)
        {
            NFZCompletedProcedure cp = new NFZCompletedProcedure(Session)
            {
                Identifier = procedure.Identifier,
                Points = procedure.Points,
                Procedure = procedure
            };

            CompletedProcedures.Add(cp);

            TotalPoints += procedure.Points;
        }

        public void RemoveProcedure(NFZCompletedProcedure procedure)
        {
            if (!CompletedProcedures.Contains(procedure))
                return;

            CompletedProcedures.Remove(procedure);

            TotalPoints -= procedure.Points;

            procedure.Delete();
        }
        
    }
}
