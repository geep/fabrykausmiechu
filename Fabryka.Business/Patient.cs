﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;


namespace Fabryka.Business
{
    public class Patient : XPObject, IPropertyUpdateTypeObject
    {

        public Patient()
        {

        }
        public Patient(Session session)
            : base(session)
        {

        }
        public Patient(Session session, DevExpress.Xpo.Metadata.XPClassInfo classInfo)
            : base(session, classInfo)
        {

        }

        #region Patient Properties

        private QuickAnalysisResults _QuickResults;
        protected string _FirstName; // 1. Imie

        public string FirstName
        {
            get { return _FirstName; }
            set 
            {
                if (_FirstName != value)
                {
                    SetPropertyValue("FirstName", ref _FirstName, value.NullTrim());
                    RaisePropertyChangedEvent("FullName");
                }
            }
        }

        protected string _LastName; // 2. Nazwisko

        public string LastName
        {
            get { return _LastName; }
            set 
            {
                if (_LastName != value)
                {
                    SetPropertyValue("LastName", ref _LastName, value.NullTrim());
                    RaisePropertyChangedEvent("FullName");
                }
            }
        }

        [NonPersistent]
        public string FullName
        {
            get
            {
                return (String.Format("{0} {1}", (FirstName ?? String.Empty),(LastName ?? String.Empty))).NullTrim();
            }
        }

        protected DateTime _Birthdate; // 5. Data urodzenia

        public DateTime Birthdate
        {
            get { return _Birthdate; }
            set { SetPropertyValue("Birthdate", ref _Birthdate, value); }
        }

        protected DateTime _RegistrationDate = DateTime.Now; // 6. Data rejestracji

        public DateTime RegistrationDate
        {
            get { return _RegistrationDate; }
            set { SetPropertyValue("RegistrationDate", ref _RegistrationDate, value); }
        }

        private bool _IsArchived;

        public bool IsArchived
        {
            get
            {
                return _IsArchived;
            }
            set
            {
                SetPropertyValue("IsArchived", ref _IsArchived, value);
            }
        }

        protected Address _Address; // 8. Adres

        [Aggregated]
        public Address Address
        {
            get { return _Address; }
            set { SetPropertyValue("Address", ref _Address, value); }
        }

        protected PatientContactData _ContactData; // 9 - 11

        [Aggregated]
        public PatientContactData ContactData
        {
            get { return _ContactData; }
            set { SetPropertyValue("ContactData", ref _ContactData, value); }
        }

        protected PatientFabrykaData _FabrykaData; // 3, 4, 12

        [Aggregated]
        public PatientFabrykaData FabrykaData
        {
            get { return _FabrykaData; }
            set { SetPropertyValue("FabrykaData", ref _FabrykaData, value); }
        }

        protected Treatment _Treatment; // 7, 17, 18

        [Aggregated]
        public Treatment Treatment
        {
            get { return _Treatment; }
            set { SetPropertyValue("Treatment", ref _Treatment, value); }
        }

        protected Gender _Gender; // 13. Plec

        public Gender Gender
        {
            get { return _Gender; }
            set { SetPropertyValue("Gender", ref _Gender, value); }
        }

        protected Doctor _Doctor; // 15. Przypisany lekarz

        [Association("Doctor-Patients")]
        public Doctor Doctor
        {
            get { return _Doctor; }
            set { SetPropertyValue("Doctor", ref _Doctor, value); }
        }

        [Association("Patient-Visits")]
        public XPCollection<Visit> Visits
        {
            get { return GetCollection<Visit>("Visits"); }
        }

        [Association("Patient-Appointments")]
        public XPCollection<Appointment> Appointments
        {
            get { return GetCollection<Appointment>("Appointments"); }
        }

        [Association("Patient-Photos")]
        public XPCollection<Photo> Photos
        {
            get { return GetCollection<Photo>("Photos"); }
        }

        [Association("Patient-PhotoComparisons")]
        public XPCollection<PhotoComparison> PhotoComparisons
        {
            get { return GetCollection<PhotoComparison>("PhotoComparisons"); }
        }

        protected Photo _MainPhoto;  // 14. Zdjecie glowne

        public Photo MainPhoto
        {
            get { return _MainPhoto; }
            set 
            {
                var oldPhoto = _MainPhoto;
                SetPropertyValue("MainPhoto", ref _MainPhoto, value); 
                RaisePropertyChangedEvent("MainPhoto!");
                if (oldPhoto != null)
                    oldPhoto.NotifyStateChanged();
                if (_MainPhoto != null)
                    _MainPhoto.NotifyStateChanged();
            }
        }

        [Aggregated]
        public QuickAnalysisResults QuickResults
        {
            get
            {
                return _QuickResults;
            }
            set
            {
                SetPropertyValue("QuickResults", ref _QuickResults, value);
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            if (Address == null)
                Address = new Address(Session);
            if (FabrykaData == null)
                FabrykaData = new PatientFabrykaData(Session);
            if (ContactData == null)
                ContactData = new PatientContactData(Session);
            if (Treatment == null)
                Treatment = new Treatment(Session);
            if (QuickResults == null)
                QuickResults = new QuickAnalysisResults(Session);

        }

     /*   public XPCollection<Photo> ImportantPhotos // 16. Dodatkowe zdjecia
        {
            get { return GetCollection<Photo>("ImportantPhotos"); }
        } */

        [NonPersistent]
        public IList<Photo> ImportantPhotos
        {
            get
            {
                return (Photos.Where((p)=>p.IsImportantPhoto).ToList());
            }
        }
        #endregion
        [NonPersistent]
        public UpdateType UpdateType
        {
            get { return UpdateType.Patient; }
        }

        public UpdateType PropertyUpdateType(string propertyName)
        {
            if (propertyName == "FirstName" || propertyName == "LastName" || propertyName == "MainPhoto")
                return UpdateType.NewPatient;
            return UpdateType;
        }

        public void NotifyImportantPhotosChanged()
        {
            OnChanged("ImportantPhotos");
        }
    }
}
